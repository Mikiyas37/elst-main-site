import React, { useState } from 'react'
import { FaTelegram } from 'react-icons/fa'
import { Title } from 'rizzui'
import { LiaTelegram } from "react-icons/lia";
import { Input } from "@/components/ui/input"

type Props = {}

function Subscribe({ }: Props) {
    const [email, setEmail] = useState("")
    return (
        <div className='flex flex-col gap-5'>
            <Title className="text-[#ffffff] text-sm uppercase underline font-bold">
                Subscribe to our news letter
            </Title>
            <div className='flex justify-between gap-4'>
                <Input placeholder='Enter your email address'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    suffix={
                        <LiaTelegram onClick={() => setEmail("")}
                            size={25} className='text-white cursor-pointer border-[3px] w-fit rounded-full border-white p-1' />
                    }
                    className='placeholder:text-white/40 text-[#ffffff] font-black outline-none focus:outline-0 bg-transparent border-none border-b-4 border-b-black w-full' />

            </div>
        </div>
    )
}

export default Subscribe