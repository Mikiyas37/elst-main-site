"use client"
import React, { useEffect, useState } from "react";

import Container from "@/components/container";
import Link from "next/link";
import { Button } from "@/components/ui/button"

import Logo from "@/components/logo";
import cn from "@/utils/class-names";
import { usePathname } from "next/navigation";
import { navItems } from "@/constants/constants";
import { useDrawer } from "@/app/_shared/drawer-views/use-drawer";
import MobileNavbar from "./mobile-navbar";
import { HiMenuAlt3 } from "react-icons/hi";

const Header = () => {
  const pathName = usePathname()
  const [isScrollable, setIsScrollable] = useState(false);
  const { openDrawer } = useDrawer();


  useEffect(() => {
    const handleScroll = () => {
      setIsScrollable(window.scrollY > 0); // Update state when scrolled past the top
    };

    window.addEventListener('scroll', handleScroll);

    // Cleanup function to remove event listener on unmount
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);


  return (
    <section className="w-full mx-auto">
      <div className={cn("w-full px-2 md:px-4 max-w-5xl xl:max-w-6xl 2xl:max-w-7xl 3xl:max-w-8xl mx-auto",
        isScrollable ? "fixed z-50 top-4 left-1/2 right-1/2 -translate-x-1/2 rounded-2xl bg-[#ffffff] text-[hsla(240,100%,3%,1)]  shadow-md shadow-t-[5px] drop-shadow-md  md:px-6 py-2 w-full max-w-5xl xl:max-w-6xl 2xl:max-w-7xl 3xl:max-w-8xl mx-auto" : "mt-6"
      )}>
        <Container className="relative !p-2 flex items-center justify-between gap-3 ">
          <div className="flex items-center w-fit">
            {/* Logo */}
            <Link href={"/"}>
              <Logo />
            </Link>
          </div>

          <div className="hidden xl:flex items-center gap-3">

            {/* Nav Items */}
            <ul className="flex items-center gap-12">
              {navItems.map((item, index) => {
                const activeLink = pathName === item.href
                return (
                  <li key={index + "nav key"}
                    className={cn("", activeLink ? "font-extrabold text-primary text-[16px]" : "text-black text-[14px] font-medium opacity-50")}
                  >
                    <Link href={item.href}>
                      {item.name}
                    </Link>
                  </li>
                )
              })}
            </ul>

            <div className="ms-5">
              <Button
                type="button"
                onClick={() => {
                  const element = document.getElementById("contact");
                  if (element) {
                    element.scrollIntoView({ behavior: "smooth" });
                  } else {
                    // Handle case where element with id "contact" is not found (optional)
                    console.error("Element with id 'contact' not found!");
                  }
                }}

                className="hidden md:flex items-center justify-center bg-primary text-[14px] font-bold px-8 py-4 rounded-full text-white" >
                Contact Us
              </Button>
            </div>
          </div>

          {/* Mobile Nav */}
          <div className="flex xl:hidden">
            <button
              onClick={() =>
                openDrawer({
                  view: <MobileNavbar />,
                  placement: "right",
                  customSize: "320px",
                })
              }
              className=""
            >
              <HiMenuAlt3 size={25} />
            </button>
          </div>
        </Container>
      </div >
    </section>
  );
};

export default Header;
