import { useDrawer } from '@/app/_shared/drawer-views/use-drawer'
import { navItems } from '@/constants/constants'
import cn from '@/utils/class-names'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import React from 'react'
import { FaTimes } from 'react-icons/fa'

type Props = {}

function MobileNavbar({ }: Props) {
    const pathName = usePathname()
    const { closeDrawer } = useDrawer()


    return (
        <div className='flex h-full w-full flex-col bg-primary p-5'>
            <div className="flex items-end justify-end w-full">
                <button onClick={closeDrawer}>
                    <FaTimes className="text-white" size={20} />
                </button>
            </div>

            <ul className="flex flex-col items-center gap-6 mt-12 w-full">
                {navItems.map((item) => {
                    const activeLink = pathName === item.href
                    return (
                        <li key={item + "nav key"}
                            className={cn("text-[16px] font-bold border-b border-b-white/60", activeLink ? "text-white font-bold" : "text-white/60 font-normal")}
                        >
                            <Link href={item.href}>
                                {item.name}
                            </Link>
                        </li>
                    )
                })}
            </ul>


        </div>
    )
}

export default MobileNavbar