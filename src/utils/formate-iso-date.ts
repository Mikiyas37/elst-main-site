export function formatISODate(dateString: Date) {
    const date = new Date(dateString); // Create a Date object from the ISO string
  
    // Options for month formatting (adjust as needed)
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"];
  
    const monthIndex = date.getMonth(); // Get the month index (0-11)
    const monthName = monthNames[monthIndex]; // Get the month name
  
    const day = date.getDate(); // Get the day of the month
  
    // Format the date string (customize if needed)
    return `${monthName} ${day}, ${date.getFullYear()}`;
  }