import Link from 'next/link'
import React from 'react'

type Props = {
    onClick?: () => void
    text: string,
    href?: string
}

const CustomButton = ({ onClick, text, href }: Props) => {
    return (
        <Link href={href || "/"}
            className='px-10 py-1 rounded-full font-giloryb text-[20px]/[24.76px] text-[#A03A43] border-[3px] border-[#A03A43] hover:text-white hover:bg-[#A03A43]'
            onClick={onClick}
        >
            {text}
        </Link>
    )
}

export default CustomButton