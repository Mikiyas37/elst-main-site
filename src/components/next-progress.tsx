'use client';

import NextTopLoader from 'nextjs-toploader';

export default function NextProgress() {
  return <NextTopLoader
    color="#a03a43"
    showSpinner={true}
    easing="easing"
    speed={200}
    zIndex={1600}
  />;
}
