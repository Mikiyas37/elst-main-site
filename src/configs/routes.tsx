export const routes = {
    home: "/",
    products: "/products",
    aboutUs: "/about-us",
    insights: "/insights",
    careers: "/careers",
}