import { client } from "@sanity/lib/client"

async function getData(query: string) {
    const data = await client.fetch(query, {}, { cache: 'no-store' })

    return data
}

export const getCategory = async () => {
    const query = `*[_type == 'category'] | order(_createdAt desc)`
    const data = await getData(query)
    return data
}

export const getSubCategory = async () => {
    const query = `*[_type == 'subcategory'] | order(_createdAt desc)`
    const data = await getData(query)
    return data
}


export const getCategoryById = async (id: string) => {
    const query = `*[_id == '${id}'] | order(_createdAt desc)`

    const data = await getData(query);

    return data;
};

export const getsubCategoryById = async (id: string) => {
    const query = `*[_id == '${id}'] | order(_createdAt desc)`

    const data = await getData(query);

    return data;
};