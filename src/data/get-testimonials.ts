import { client } from "@sanity/lib/client"

async function getData() {
    const query = `*[_type == 'testimonials'] | order(_createdAt desc)`
    const data = await client.fetch(query, {}, { cache: 'no-store' })

    return data
}

export const getTestimonials = async () => {
    const data = await getData()

    return data
}

