import { client } from "@sanity/lib/client"

async function getData(query: string) {
    const data = await client.fetch(query, {}, { cache: 'no-store' })

    // && references(*[_type == 'product' && _id == '${id}']._id)
    return data
}

export const getBlogs = async () => {
    const query = `*[_type == 'blog'] | order(_createdAt desc)`

    const data = await getData(query)
    return data
}

export const getPodcasts = async (id: string) => {
    const query = `*[_type == 'blog'] && references(*[_type == 'category' && _id == '${id}']._id) ] | order(_createdAt desc)`

    const data = await getData(query)
    return data
}


export const getBlogsByCategory = async (id: string) => {
    const query = `*[_type == 'blog' && references(*[_type == 'category' && _id == '${id}']._id) ] | order(_createdAt desc)`

    const data = await getData(query)
    return data
}

export const getBlogsBySubCategory = async (id: string) => {
    const query = `*[_type == 'blog' && references(*[_type == 'subcategory' && _id == '${id}']._id)] | order(_createdAt desc)`

    const data = await getData(query)
    return data
}

export const getBlogDetails = async (id: string) => {
    const query = `*[_id == '${id}'] | order(_createdAt desc)`

    const data = await getData(query);

    return data;
};

