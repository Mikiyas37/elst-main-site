import { client } from "@sanity/lib/client"

import {
    useQuery,
} from '@tanstack/react-query'

const getProductDetails = async (id: string) => {
    const query = `*[_type == 'productDetails' && references(*[_type == 'product' && _id == '${id}']._id)] | order(_createdAt desc)`

    const data = await client.fetch(query, {}, { cache: 'no-store' })
    return data;
};

export const useProductDetails = (id: string) => {
    // return useQuery(['productDetails', id], () => getProductDetails(id), {
    //     staleTime: 1000 * 60 * 5, // Refetch data every 5 minutes
    // });

    const { isPending, error, data } = useQuery({
        queryKey: ['productDetails'],
        queryFn: () => getProductDetails(id)
    })

    return data

};
