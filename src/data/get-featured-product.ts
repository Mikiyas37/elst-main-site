"use client"
import { client } from "@sanity/lib/client"
import { useState } from "react"

async function getData() {
    const query = `*[_type == 'featuredProduct'] | order(_createdAt desc)`
    const data = await client.fetch(query, {}, { cache: 'no-store' })
    return data
}

export const getFeaturedProducts = async () => {
    // const [loading, setIsLoading] = useState(false)
    // const [data, setDate] = useState([])

    // try {
    //     setIsLoading(true)
    //     setDate(await getData())
    // } catch (error) {

    // } finally {
    //     setIsLoading(false)
    // }

    // return { loading, data }
    const data = await getData()
    return data
}
