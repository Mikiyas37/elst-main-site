import { client } from "@sanity/lib/client"

async function getData(id: string) {
  // Construct the query to filter by the referenced product's ID
  const query = `*[_type == 'productDetails' && references(*[_type == 'product' && _id == '${id}']._id)] | order(_createdAt desc)`

  // Use the `{ cache: 'no-store' }` option to bypass the cache
  const data = await client.fetch(query, {}, { cache: 'no-store' })
  return data;
}

export const getProductDetails = async (id: string) => {
  const data = await getData(id);

  return data;
};