import { client } from "@sanity/lib/client"

async function getData(query: string) {

    // Use the `{ cache: 'no-store' }` option to bypass the cache
    const data = await client.fetch(query, {}, { cache: 'no-store' })
    return data;
}

export const getJobs = async () => {

    const query = `*[_type == 'jobs'] | order(_createdAt desc)`
    const data = await getData(query);

    return data;
};

export const getJobDetails = async (id: string) => {

    const query = `*[_id == '${id}'] | order(_createdAt desc)`

    const data = await getData(query);

    return data;
};