import { routes } from "@/configs/routes"

export const navItems = [
  {
    name: "Home",
    href: routes.home
  },
  {
    name: "Products",
    href: routes.products
  },
  {
    name: "About Us",
    href: routes.aboutUs
  },
  {
    name: "Insights",
    href: routes.insights
  },
  {
    name: "Careers",
    href: routes.careers
  },
]

export const stasData = [
  {
    name: "Transactions",
    value: 5.5,
    description: "Billion+"
  },
  {
    name: "Users",
    value: 4,
    description: "Million+"
  },
  {
    name: "Registered Merchants",
    value: 16000,
    description: "+"
  },
  {
    name: "Customer Satisfaction",
    value: 99,
    description: "%"
  },
  {
    name: "Innovative Products",
    value: 28,
    description: "+"
  },
  {
    name: "Partners",
    value: 50,
    description: "+"
  },
]


export const productStatData = [
  {
    name: "Transactions",
    value: 5.5,
    description: "Billion+"
  },
  {
    name: "Users",
    value: 4,
    description: "Million+"
  },
  {
    name: "Merchants",
    value: 16000,
    description: "+"
  },
  {
    name: "Partners",
    value: 50,
    description: "+"
  },
]