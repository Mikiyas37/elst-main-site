import Hero from '@/app/_shared/careers'
import Images from '@/app/_shared/careers/images'
import Journey from '@/app/_shared/careers/journey'
import WorkWithUs from '@/app/_shared/careers/work-with-us'
import ContactUs from '@/app/_shared/contact-us'
import ScrollToUp from '@/app/_shared/scroll-to-up'
import React from 'react'

type Props = {}

function Careers({ }: Props) {
  return (
    <>
      <Hero />

      <Images />

      <WorkWithUs />

      <Journey />

      <ContactUs />

      <ScrollToUp className='fixed h-screen z-20 bottom-4 right-4' />
    </>
  )
}

export default Careers