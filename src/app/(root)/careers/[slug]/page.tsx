import Hero from '@/app/_shared/careers-details'
import Description from '@/app/_shared/careers-details/description'
import ContactUs from '@/app/_shared/contact-us'
import ScrollToUp from '@/app/_shared/scroll-to-up'
import React from 'react'

type Props = {}

function page({ }: Props) {
    return (
        <>
            <Hero />

            <Description />

            <ContactUs />

            <ScrollToUp className='fixed h-screen z-20 bottom-4 right-4' />
        </>
    )
}

export default page