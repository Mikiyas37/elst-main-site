import Hero from '@/app/_shared/insights-details'
import Relatednews from '@/app/_shared/insights-details/related-news'
import Subscription from '@/app/_shared/insights/subscription'
import ScrollToUp from '@/app/_shared/scroll-to-up'
import { getBlogDetails } from '@/data/get-blogs'
import React from 'react'

type Props = {
    params: {
        slug: string
    }
}

function page({ params }: Props) {
    return (
        <>
            <Hero />

            <Relatednews />

            <Subscription />

            <ScrollToUp className='fixed h-screen z-20 bottom-4 right-4' />
        </>
    )
}

export default page