import ContactUs from '@/app/_shared/contact-us'
import Hero from '@/app/_shared/insights'
import LatestInsights from '@/app/_shared/insights/latest-insights'
import Podcasts from '@/app/_shared/insights/podcasts'
import Subscription from '@/app/_shared/insights/subscription'
import TopCategories from '@/app/_shared/insights/top-categories'
import ScrollToUp from '@/app/_shared/scroll-to-up'
import { getBlogs, getBlogsByCategory, getPodcasts } from '@/data/get-blogs'
import { getCategory, getSubCategory } from '@/data/get-category'
import React from 'react'

type Props = {}

const Insights = async (props: Props) => {

  const blogs = await getBlogs()
  const categories = await getCategory()
  const getSubCatergory = await getSubCategory()
  const podcasts = await getBlogsByCategory("cde1c0d5-8a97-43f1-96ce-85a4d108f316")

  return (
    <>

      <Hero />

      <LatestInsights />

      <TopCategories data={blogs} getSubCatergory={getSubCatergory} />

      <Podcasts podcasts={podcasts}/>

      <Subscription />

      <ContactUs />

      <ScrollToUp className='fixed h-screen z-20 bottom-4 right-4' />

    </>
  )
}

export default Insights