import Hero from '@/app/_shared/about-us/about-hero'
import Achievements from '@/app/_shared/about-us/achievements'
import FAQ from '@/app/_shared/about-us/faq'
import Horizons from '@/app/_shared/about-us/horizons'
import Leadership from '@/app/_shared/about-us/leadership'
import MissionVisionValues from '@/app/_shared/about-us/mission-vision-values'
import OurCEO from '@/app/_shared/about-us/our-ceo'
import OurGoal from '@/app/_shared/about-us/our-goal'
import OurTeam from '@/app/_shared/about-us/our-team'
import Story from '@/app/_shared/about-us/story'
import ContactUs from '@/app/_shared/contact-us'
import ScrollToUp from '@/app/_shared/scroll-to-up'
import React from 'react'

type Props = {}

const AboutUs = (props: Props) => {
  return (
    <>
      <Hero />

      <MissionVisionValues />

      <Horizons />

      <Achievements />

      <OurGoal />

      <Story />

      <OurTeam />

      <OurCEO />

      <Leadership />

      <FAQ />

      <ContactUs />

      <ScrollToUp className={"fixed h-screen z-20 bottom-4 right-4 "} />

    </>
  )
}

export default AboutUs