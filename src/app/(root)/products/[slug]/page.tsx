// "use server"
import ContactUs from '@/app/_shared/contact-us'
import Chronicles from '@/app/_shared/products-detail/chronicles'
import Embark from '@/app/_shared/products-detail/embark'
import FinancialManagment from '@/app/_shared/products-detail/financial-managment'
import HappyCustomer from '@/app/_shared/products-detail/happy-customer'
import ProductDetailsHero from '@/app/_shared/products-detail/hero'
import HowItWorks from '@/app/_shared/products-detail/how-it-works'
import OurApp from '@/app/_shared/products-detail/latest-version-app'
import Memories from '@/app/_shared/products-detail/memories'
import PartnerShip from '@/app/_shared/products-detail/partnership'
import ProductDetailsPage from '@/app/_shared/products-detail/product-details-page'
import TopFeatures from '@/app/_shared/products-detail/top-features'
import ScrollToUp from '@/app/_shared/scroll-to-up'
import { getProductDetails } from '@/data/get-product-details'
import { useProductDetails } from '@/data/use-product-details'
import React, { useState } from 'react'

type Props = {
  params: { slug: string }
}

async function page({ params }: Props) {

  const data = await getProductDetails(params.slug)
  // console.log(data, "Product Details..");

  return (
    <div>
      {!data.length &&
        <div className='w-full h-96 flex items-center justify-center'>
          <p className='text-gray-500 font-bold md:text-xl text-center'>Product Detail Not Found!</p>
        </div>
      }
      {/* <ProductDetailsPage /> */}
      {data[0]?.introdcution &&
        <ProductDetailsHero data={data[0].introdcution} />}

      {data[0]?.topFeatures &&
        <TopFeatures data={data[0].topFeatures} />}

      {data[0]?.achievments &&
        <Embark data={data[0].achievments} />}

      {data[0]?.howItWorks &&
        <HowItWorks data={data[0]?.howItWorks} />}

      {data[0]?.web &&
        <FinancialManagment data={data[0]?.web} />}

      {data[0]?.memories?.videoMemory &&
        <Memories data={data[0]?.memories?.videoMemory} />}

      {data[0]?.memories?.photoMemory &&
        <Chronicles data={data[0]?.memories?.photoMemory} />}

      {data[0]?.testimonail &&
        <HappyCustomer data={data[0]?.testimonail} />}

      {data[0]?.partner &&
        <PartnerShip data={data[0]?.partner} />}

      {data[0]?.download &&
        <OurApp data={data[0]?.download} />} 

      <ContactUs />

      <ScrollToUp className={"fixed h-screen z-20 bottom-4 right-4 "} />


    </div>
  )
}

export default page