import React from 'react'
import Hero from '@/app/_shared/products'
import ScrollToUp from '@/app/_shared/scroll-to-up'
import FeaturedProducts from '@/app/_shared/products/featured-products'
import FinancialSolutions from '@/app/_shared/products/financial-solutions'
import CornerStone from '@/app/_shared/products/corner-stone'
import EducationEntertainment from '@/app/_shared/products/education-entertainment'
import OtherSector from '@/app/_shared/products/other-sector'
import Testimonials from '@/app/_shared/products/testimonials'
import ContactUs from '@/app/_shared/contact-us'
import { getProducts } from '@/data/get-product'
import { getFeaturedProducts } from '@/data/get-featured-product'

type Props = {}

const Products = (props: Props) => {

  return (
    <div className='relative w-full'>

      <Hero />

      <FeaturedProducts />

      <FinancialSolutions />

      <CornerStone />

      <EducationEntertainment />

      <OtherSector />

      <Testimonials />

      <ContactUs />

      <ScrollToUp className={"fixed h-screen z-20 bottom-4 right-4 "} />

    </div>
  )
}

export default Products