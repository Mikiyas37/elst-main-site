import type { Metadata } from "next";
import GlobalDrawer from "./_shared/drawer-views/container";

import { publicSans, openSans, landSans, gilroy } from "@/app/fonts";
import cn from "@/utils/class-names";
import "./globals.css";
import { getServerSession } from "next-auth";
import { authOptions } from "./api/auth/[...nextauth]/auth-options";
import AuthProvider from "./api/auth/[...nextauth]/auth-provider";
import GlobalModal from "./_shared/modal-views/container";
import { Toaster } from "sonner";
import LogoIcon from "@public/elst_logo.png"
import Image from "next/image";
import dynamic from "next/dynamic";
const NextProgress = dynamic(() => import("@/components/next-progress"), {
  ssr: false,
});


export const metadata: Metadata = {
  title: "EagleLion System Technology",
  description: "",
};


export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const session = await getServerSession(authOptions);

  return (
    <html lang="en">
      <body
        suppressHydrationWarning
        className={cn(
          publicSans.variable,
          openSans.variable,
          landSans.variable,
          gilroy.variable,
          "font-public"
        )}
      >
        <AuthProvider session={session}>
          <Toaster richColors position="top-right" closeButton />
          <GlobalDrawer />
          <GlobalModal />
          <NextProgress />
          {children}
        </AuthProvider>
      </body>
    </html>
  );
}
