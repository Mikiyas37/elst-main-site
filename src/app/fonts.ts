import { Public_Sans, Open_Sans } from "next/font/google";
import localFont from "next/font/local";

export const openSans = Open_Sans({
  subsets: ["latin"],
  variable: "--font-sans",
});

export const publicSans = Public_Sans({
  subsets: ["latin"],
  variable: "--font-public",
});

export const landSans = localFont({
  src: [
    {
      path: "./Landasans_demo01-Regular.ttf",
      weight: "800",
      style: "normal",
    },
  ],
  variable: "--font-landsans",
});

export const gilroy = localFont({
  src: [
    {
      path: "./fonts/gilroy/Gilroy-Regular.ttf",
    }
  ],
  weight: "400",
  style: "Regular",
  variable: "--font-gilroy"

})