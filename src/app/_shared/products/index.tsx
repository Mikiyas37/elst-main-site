import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'

import image1 from "@public/products/teletv.svg"
import image2 from "@public/products/cashgo.svg"
import image3 from "@public/products/chatbirr.svg"
import image4 from "@public/products/getrooms.svg"
import image5 from "@public/products/ethiodirect.svg"
import image6 from "@public/products/getfee.svg"
import image7 from "@public/products/dube.svg"

type Props = {}

const Hero = (props: Props) => {
  return (
    <Container >
      <div className='flex mt-24 flex-col items-center leading-7 w-full'>

        <Title className="font-extrabold text-2xl md:text-4xl 2xl:text-[40px]/[45px] text-center mx-auto max-w-3xl md:-mt-6">
          Explore Our Extensive Portfolio of Innovative such Products and Solutions
        </Title>

      </div>

      <div className='flex flex-col mt-8 w-full'>
        <div className='flex justify-between mx-auto md:max-w-5xl w-full'>
          <Image src={image1} alt="" className="h-16 w-20 p-3 rounded-lg object-contain shadow-lg" />
          <Image src={image2} alt="" className="h-[4.5rem] w-40 p-3 rounded-lg object-contain shadow-lg" />
        </div>
        <div className='flex justify-between mx-auto md:max-w-md w-full mt-4 md:-mt-3'>
          <Image src={image3} alt="" className="h-14 w-40 p-3 rounded-lg object-contain shadow-lg" />
          <Image src={image4} alt="" className="h-14 w-20 p-3 rounded-lg object-contain shadow-lg" />
        </div>
        <div className='flex justify-between mx-auto md:max-w-[52rem] w-full mt-4 md:-mt-1'>
          <Image src={image5} alt="" className="h-12 w-24 p-3 rounded-lg object-contain shadow-lg" />
          <Image src={image6} alt="" className="h-16 w-40 p-3 rounded-lg object-contain shadow-lg" />
        </div>
        <div className='flex justify-center mx-auto w-full mt-4 md:-mt-6 -mr-8'>
          <Image src={image7} alt="" className="h-24 w-40 p-3 rounded-lg object-contain shadow-lg" />
        </div>

      </div>

    </Container>
  )
}

export default Hero