import React from 'react'
import { Title, Text } from "@/components/ui/text"
import CountUp from 'react-countup';
import cn from '@/utils/class-names';
import { gsap } from "gsap"
import NumberTicker from '@/components/magicui/number-ticker';

type Props = {
    data: any,
    color?: string
}

function ProductStat({ data, color }: Props) {

    // gsap.timeline({
    //     scrollTrigger: {
    //         trigger: '.counter',
    //         snap: {
    //             duration: 2,
    //             delay: 1,
    //             ease: 'power1.inOut'
    //         }
    //     }
    // });

    return (
        <div className='flex flex-col items-center gap-1 w-full'>
            <div className='counter flex gap-2 font-extrabold text-xl md:text-3xl'>
                <p className={cn(color, 'text-[25px]/[36px] 2xl:[30px]/[40px] whitespace-pre-wrap tracking-tighter font-bold')}>
                    <NumberTicker value={data?.value} />
                </p>
                <Title className={cn(color, "text-[25px]/[36px] 2xl:[30px]/[40px] font-extrabold")}>
                    {data?.description}
                </Title>
            </div>
            <Text className={cn(color, 'text-sm font-light')}>
                {data?.name}
            </Text>
        </div>
    )
}

export default ProductStat