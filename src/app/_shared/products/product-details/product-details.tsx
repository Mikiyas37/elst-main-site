import React from 'react'
import { Title, Text } from "@/components/ui/text"
import ProductStat from './product-stat'
import startQuote from "@public/icon/Vector (19).svg"
import endQuote from "@public/icon/Vector (16).svg"
import Image from 'next/image'

import globe from "@public/icon/Vector (20).svg"
import Link from 'next/link'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../../configuredSanityClient'
import SectionLoader from '../../looader/section-loader'

type Props = {
    data?: any,
    loading?: boolean
}

function ProductDetails({ data, loading }: Props) {

    const imagerops = useNextSanityImage(configuredSanityClient, data?.happyCustomerImage);


    return (
        <div className='py-5 px-3 md:p-8 2xl:p-12 w-full overflow-hidden border-[1px] border-[#2F654C26] rounded-3xl'>
            <button className='px-3 rounded text-xs md:text-base bg-[#1A6B4814] text-[#2F654C] font-extrabold opacity-50'>
                {data?.category}
            </button>
            <div className='flex flex-col gap-3 mt-4'>
                <Title className='text-3xl font-extrabold'>
                    {data?.title}
                </Title>
                <Text className='w-full text-base 2xl:text-lg leading-7 font-light'>
                    {data?.description[0]?.children[0]?.text}
                </Text>
            </div>


            <div className='flex flex-col mt-12 gap-4'>
                <Title className={"text-lg md:text-xl font-bold"}>
                    Achievements Made numbers on it
                </Title>

                <div className='grid grid-cols-2 md:grid-cols-4 gap-8 justify-between w-full'>
                    {data?.achievments?.stats?.length && data?.achievments?.stats?.map((data: any, index: number) => (
                        <ProductStat data={data} key={index + "Stat Data"} />
                    ))}
                </div>
            </div>


            {/* {data?.happyCustomer && */}
            <div className='flex flex-col mt-12 gap-4'>
                <Title className={"text-xl font-bold"}>
                    One Of our  Happy Customers
                </Title>


                <Text className='relative px-6 md:px-8 flex gap-2 w-full font-light opacity-90 text-justify text-sm 2xl:text-base'>
                    {data?.happyCustomer}
                    <Image src={startQuote} alt={"start Quote"} className='absolute top-0 left-0' />
                    <Image src={endQuote} alt={"start Quote"} className='absolute bottom-0 right-0' />
                    {imagerops &&
                        <Image src={imagerops} alt='' className='absolute rounded-full w-12 h-12 right-6 -bottom-10 hidden md:block' />}
                </Text>
            </div>

            <div className='flex gap-2 items-center mt-16 pb-6'>
                <Image src={globe} alt="globe" className='w-4 h-4' />
                <Text className='font-extrabold font-gilroy text-base'>Link:</Text>
                <Link href={data?.link} target='_blank' className='text-sm font-bold text-[#2F654C] underline'>
                    {data?.link}
                </Link>
            </div>
            {/* } */}
        </div>
    )
}

export default ProductDetails