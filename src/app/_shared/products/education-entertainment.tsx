import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"

import schooCloud from "@public/products/smartschool.svg"
import teletv from "@public/products/teletv-02 3.png"
import dome from "@public/products/DOMe.png"

import Image from 'next/image'
import EducationCards from './cards/education-cards'
import FinestProductCards from './cards/finest-product-cards'
import image1 from "@public/innovations/Group 1171275114.png"
import image2 from "@public/innovations/Rectangle 1751.png"
import EntertainmentCards from './cards/entertainment-card'

type Props = {}

const education = [
    {
        image: <Image src={schooCloud} alt={"education"} className='w-36 h-32 object-contain' />,
        title: "School of Cloud",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        image: <Image src={schooCloud} alt={"education"} className='w-36 h-32 object-contain' />,
        title: "Smart School",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    // {
    //     image: <Image src={schooCloud} alt={"education"} className='w-36 h-32' />,
    //     title: "School of Cloud",
    //     description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
    //     link: "Nedajapp.com"
    // },
]

const entertainment = [
    {
        image: <Image src={teletv} alt={"education"} className='w-24 h-16 object-contain' />,
        title: "Tele Tv",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        image: <Image src={dome} alt={"education"} className='w-24 h-16 object-contain' />,
        title: "Dome",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
]


function EducationEntertainment({ }: Props) {
    return (
        <Container>
            <div className='mt-24 w-full'>
                <Title className='font-bold text-2xl md:text-3xl text-center md:text-start'>
                    Education and Entertainment platforms
                </Title>
                <Text className='font-normal leading-6 mt-2 max-w-6xl'>
                    We&apos;re not just about finance – we&apos;re also making waves in education and entertainment. Dive into our innovative platforms designed to inspire learning and enhance entertainment experiences.
                </Text>

                <div className='grid md:grid-cols-2 gap-4 xl:gap-6 2xl:gap-8 mt-12 w-full'>
                    {/* Education */}
                    <div className='w-full h-full relative'>
                        <Image src={image2} alt={""} fill />

                    </div>
                    <div className='flex flex-col gap-3 py-6 bg-[#F9F9F9] p-2 md:p-4 rounded-2xl border-[1px] border-[#A03A434D] shadow-md'>
                        <Title className='font-bold text-xl text-center md:text-start'>
                            Education and learning Tools
                        </Title>
                        <div className='grid grid-rows-2 gap-4 xl:gap-6'>
                            {education.map((data, index) => (
                                <EducationCards key={index} {...data} />
                            ))}
                        </div>
                    </div>

                </div>
            </div>
            <div className='mt-24 w-full'>
                <Title className='font-bold text-2xl xl:text-3xl md:text-center md:text-start'>
                    Entertainment platforms
                </Title>
                <Text className='font-light mt-2 max-w-6xl'>
                    We&apos;re not just about finance – we&apos;re also making waves in education and entertainment. Dive into our innovative platforms designed to inspire learning and enhance entertainment experiences.
                </Text>


                <div className='grid md:grid-cols-2 gap-4 xl:gap-6 2xl:gap-8 mt-12'>
                    <div className='flex flex-col gap-3 py-6 bg-[#F9F9F9] p-2 md:p-4 rounded-2xl border-[1px] border-[#A03A434D] shadow-md'>
                        <Title className='font-bold text-xl text-center md:text-start'>
                            Entertainment Platforms
                        </Title>
                        <div className='grid grid-rows-2 gap-4 xl:gap-6'>
                            {entertainment.map((data, index) => (
                                <EntertainmentCards key={index} {...data} />
                            ))}
                        </div>
                    </div>

                    {/* Entertainment */}
                    <div className='w-full h-full relative'>
                        <Image src={image1} alt={""} fill />
                    </div>
                </div>
            </div>
        </Container>
    )
}

export default EducationEntertainment