"use client"
import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import ProductListTabs from './product-tabs/productList-tabs'

type Props = {
}

function FeaturedProducts({ }: Props) {
    return (
        <Container>
            <div className='mt-24 w-full'>
                <Title className='font-extrabold text-2xl'>
                    Our Featured Products
                </Title>
                <Text className='font-normal opacity-70 text-sm'>
                    Explore our curated selection of top-notch featured products.
                </Text>

                <ProductListTabs className={"w-full"} />
            </div>
        </Container>
    )
}

export default FeaturedProducts