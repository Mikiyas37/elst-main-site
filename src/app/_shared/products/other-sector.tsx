import React from 'react'
import { Title, Text } from "@/components/ui/text"

import SimpleBar from '@/components/ui/simplebar';
import eagleView from "@public/products/eagle-view.png"
import dhs from "@public/products/dhs.png"
import getcare from "@public/products/getcare.png"
import chinet from "@public/products/chinet.png"

import Image from 'next/image'
import FinestProductCards from './cards/finest-product-cards';

type Props = {}

const finestProducts = [
    {
        image: <Image src={eagleView} alt={"eagleView"} className='w-48 h-28' />,
        title: "Eagle View",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        image: <Image src={dhs} alt={"dhs"} className='w-48 h-28' />,
        title: "Digital Health System",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        image: <Image src={getcare} alt={"getfee"} className='w-48 h-28' />,
        title: "getfee",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        image: <Image src={chinet} alt={"chinet"} className='w-48 h-28' />,
        title: "Chinet",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
]

function OtherSector({ }: Props) {
    return (
        <div className='mt-24 flex flex-col gap-10 w-full p-3'>
            <div className='flex flex-col gap-2'>
                <Title className='font-extrabold text-3xl text-center'>
                    We are also crafting finest products in other Sectors
                </Title>
                <Text className='font-light text-center max-w-6xl w-full mx-auto'>
                    Beyond banking and fintech, we&apos;re dedicated to excellence in other sectors too. Explore our range of top-quality products crafted to meet diverse needs and elevate experiences across various industries.
                </Text>
            </div>

            <SimpleBar className='w-full pb-6'>
                <div className='w-full flex gap-4 md:gap-6 2xl:gap-10'>
                    {finestProducts.map((data, index) => (
                        <FinestProductCards key={index} {...data} className={"min-w-[300px] md:min-w-[400px] min-h-[360px] w-full"} />
                    ))}
                </div>
            </SimpleBar>

        </div>
    )
}

export default OtherSector