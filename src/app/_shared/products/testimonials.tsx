import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import TestimonialsCard from './cards/testimonials-card'
import Image from 'next/image'
import CEO from "@public/ceo.png"
import image1 from "@public/testimonials/Ellipse 818 (1).png"
import image2 from "@public/testimonials/Ellipse 818 (2).png"
import image3 from "@public/testimonials/Ellipse 818 (3).png"
import image4 from "@public/testimonials/Ellipse 818 (4).png"
import image5 from "@public/testimonials/Ellipse 818 (5).png"
import image6 from "@public/testimonials/Rectangle1618.png"

type Props = {}

function Testimonials({ }: Props) {

  return (
    <Container className='mt-24 flex flex-col gap-10 w-full'>
      <div className='flex flex-col gap-1 w-full'>
        <Title className='font-extrabold text-3xl text-center'>
          Testimonials from our clients
        </Title>
        <Text className='font-medium opacity-70 text-center max-w-3xl 2xl:max-w-4xl w-full mx-auto'>
          Hear directly from those who have experienced the impact of our services firsthand, and discover how we&apos;ve helped them achieve their goals.
        </Text>
      </div>


      <div className='grid md:grid-cols-3 gap-6 2xl:gap-10 w-full'>
        <div className='flex flex-col gap-6 2xl:gap-10'>
          <TestimonialsCard
            image={<Image src={CEO} alt={"ceo"} className='w-full h-full' />}
            name={"Ato Kebede Taye"}
            position={"CEO Dashen Bank"}
            description={"“ From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. ”"}
          />
          <TestimonialsCard
            image={<Image src={image1} alt={"ceo"} className='w-full h-full' />}
            name={"Ato Kebede Taye"}
            position={"CEO Dashen Bank"}
            description={"“ From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. ”"}
          />

        </div>

        <div className='flex flex-col gap-6 2xl:gap-10 w-full'>
          <TestimonialsCard
            image={<Image src={image2} alt={"ceo"} className='w-full h-full' />}
            name={"Ato Kebede Taye"}
            position={"CEO Dashen Bank"}
            description={"“ From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. ”"}
          />
          <TestimonialsCard
            image={<Image src={image3} alt={"ceo"} className='w-full h-full' />}
            name={"Ato Kebede Taye"}
            position={"CEO Dashen Bank"}
            description={"“ From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. ”"}
          />
        </div>

        <div className='flex flex-col gap-6 2xl:gap-10 w-full'>
          <TestimonialsCard
            image={<Image src={image4} alt={"ceo"} className='w-full h-full' />}
            name={"Ato Kebede Taye"}
            position={"CEO Dashen Bank"}
            description={"“ From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. ”"}
          />
          <TestimonialsCard
            image={<Image src={image5} alt={"ceo"} className='w-full h-full' />}
            name={"Ato Kebede Taye"}
            position={"CEO Dashen Bank"}
            description={"“ From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances. ”"}
          />
        </div>

      </div>


    </Container>
  )
}

export default Testimonials