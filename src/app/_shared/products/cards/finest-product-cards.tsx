import React from 'react'
import globe from "@public/icon/Vector (20).svg"
import Image from 'next/image'
import Link from 'next/link'
import { Text, Title } from "@/components/ui/text"
import cn from '@/utils/class-names'

type Props = {
    image: any,
    title: string,
    description: string,
    link: string,
    className?: string
}

function FinestProductCards({ className, image, title, description, link }: Props) {
    return (
        <div className={cn(className, 'rounded-2xl p-4 grid grid-rows-2 justify-center h-full gap-3 bg-[#F9F9F9] border-[1px] border-[#A03A434D] shadow-md')}>
            <div className="flex flex-col flex-grow items-center justify-center rounded-xl bg-[#fff] border-[1px] border-[#8C65331A] shadow shadow-[#3333330D]">
                {image}
            </div>

            <div  className='flex flex-col gap-1'>
                <Title className="font-extrabold text-xl flex justify-between">
                    {title}
                    {/* <span className="text-xs font-light">Entertainment</span> */}
                </Title>
                <Text className="text-base font-normal">{description}</Text>

                <div className='flex gap-2 items-center mt-4'>
                    <Image src={globe} alt="globe" className='w-4 h-4' />
                    <Text className='font-bold text-base'>Link:</Text>
                    <Link href="#" className='text-sm text-primary underline'>
                        {link}
                    </Link>
                </div>

            </div>
        </div>
    )
}

export default FinestProductCards