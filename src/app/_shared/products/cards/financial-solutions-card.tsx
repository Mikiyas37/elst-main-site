"use client"
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import globe from "@public/icon/Vector (20).svg"
import Image from 'next/image'
import Link from 'next/link'
import { routes } from '@/configs/routes'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../../configuredSanityClient'

type Props = {
    data: any,
}

function FinancialSolutionsCard({ data }: Props) {


    const imagesProps = useNextSanityImage(configuredSanityClient, data?.image);
    const logoProps = useNextSanityImage(configuredSanityClient, data?.logo);
    // console.log(imagesProps, "imagesProps")
    // console.log(data, "data")


    return (
        <Link href={routes.products + `/${data?._id}`} className='group w-full min-h-80 h-fit p-4 md:p-8 hover:bg-white hover:rounded-3xl hover:shadow-md'>
            <div className='grid grid-cols-5 p-3 items-end justify-between border-[0.03px] border-[#8C65331A] bg-white group-hover:bg-[#ffffff] rounded-2xl'>
                <div className='col-span-2 flex min-h-40 h-full w-full items-center justify-center'>
                    {logoProps && <Image src={logoProps} alt={"logo"} className='w-24 h-14' />}
                </div>
                <div className='col-span-3 md:col-span-2'>
                    {imagesProps &&
                        <Image src={imagesProps} alt={"smartphone"} className='w-64 h-44 object-contain shrink-0' />}
                </div>
            </div>

            <div className='flex flex-col gap-3 mt-4'>
                <div className='flex justify-between items-end'>
                    <Title className='text-xl xl:text-2xl font-extrabold'>
                        {data?.name}
                    </Title>
                    <p className='text-gray-600 uppercase text-xs font-bold opacity-70'>{data?.category}</p>
                </div>
                <Text className='max-w-lg w-full text-extrabold leading-6 line-clamp-3 text-sm 2xl:text-base'>
                    {data?.description[0]?.children[0]?.text}
                </Text>
            </div>

            <div className='flex gap-2 items-center mt-8'>
                <Image src={globe} alt="globe" className='w-4 h-4' />
                <Text className='font-bold text-base'>Link:</Text>
                <Link href="#" className='text-sm text-[#2F654C] underline'>
                    {data?.link}
                </Link>
            </div>
        </Link>
    )
}

export default FinancialSolutionsCard