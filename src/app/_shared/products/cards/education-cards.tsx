import Reacr from "react"
import globe from "@public/icon/Vector (20).svg"
import Image from 'next/image'
import Link from 'next/link'
import { Text, Title } from "@/components/ui/text"

type Props = {
    image: any,
    title: string,
    description: string,
    link: string
}

function EducationCards({ image, title, description, link }: Props) {
    return (
        <div className='p-4 grid md:grid-cols-2 gap-3 justify-between'>
            <div className="flex flex-col min-h-28 h-full items-center justify-center rounded-xl bg-[#ffffff] border-[1px] border-[#8C65331A] shadow shadow-[#3333330D]">
                {image}
            </div>

            <div>
                <p className="text-[10px] font-medium">Education and Learning</p>
                <Title className="font-extrabold text-lg xl:text-xl">{title}</Title>
                <Text className="text-xs 2xl:text-sm font-light">{description}</Text>

                <div className='flex gap-2 items-center mt-4'>
                    <Image src={globe} alt="globe" className='w-4 h-4' />
                    <Text className='font-bold text-sm'>Link:</Text>
                    <Link href="#" className='text-xs text-primary underline'>
                        {link}
                    </Link>
                </div>

            </div>
        </div>
    )
}

export default EducationCards