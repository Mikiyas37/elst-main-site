import React from 'react'
import { Title, Text } from "@/components/ui/text"

type Props = {
  image: any
  name: string
  position: string
  description: string
}

function TestimonialsCard({ image, name, description, position }: Props) {
  return (
    <div className='w-full h-full rounded-2xl border border-[#8C65331A] bg-[#fff] shadow-lg pb-12 md:pb-20 p-3 md:p-6 2xl:p-8'>
      <div className='flex gap-4 items-center'>
        <div className='w-14 h-14 rounded-full'>
          {image}
        </div>

        <div className='flex flex-col gap-1'>
          <Title className='text-base font-bold'>{name}</Title>
          <Title className='text-sm font-medium'>{position}</Title>
        </div>
      </div>

      <Text className='mt-4 text-sm font-light'>{description}</Text>
    </div>
  )
}

export default TestimonialsCard