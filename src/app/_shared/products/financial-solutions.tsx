"use client"
import React, { useEffect, useState } from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import FinancialSolutionsCard from './cards/financial-solutions-card'

import cashgo from "@public/products/cashgo.svg"
import telebirr from "@public/products/chatbirr.svg"
import getfee from "@public/products/getfee.svg"
import ethiodirect from "@public/products/ethiodirect.svg"

import smartphone from "@public/products/smartphone.png"
import Image from 'next/image'
import { getProducts } from '@/data/get-product'
import CardLoader from '../looader/card-loader'

type Props = {
}

const solutions = [
    {
        icon: <Image src={cashgo} alt={"smartphone"} />,
        image: <Image src={smartphone} alt={"smartphone"} className='w-30 h-48' />,
        title: "CashGo",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        icon: <Image src={telebirr} alt={"smartphone"} />,
        image: <Image src={smartphone} alt={"smartphone"} className='w-30 h-48' />,
        title: "ChatBirr",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        icon: <Image src={getfee} alt={"smartphone"} />,
        image: <Image src={smartphone} alt={"smartphone"} className='w-30 h-48' />,
        title: "Get Fee",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
    {
        icon: <Image src={ethiodirect} alt={"smartphone"} />,
        image: <Image src={smartphone} alt={"smartphone"} className='w-30 h-48' />,
        title: "EthioDirect",
        description: "Nedaj is a cutting-edge mobile application designed to streamline the process of fuel payment for car owners. ",
        link: "Nedajapp.com"
    },
]

function FinancialSolutions({ }: Props) {

    const [data, setData] = useState([])
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        getProducts()
            .then(data => {
                setData(data)
                setLoading(false)
            })
            .catch(error => {
                setError(error)
            });
    }, [])


    return (
        <Container className='flex flex-col mt-24 gap-10 w-full'>
            <Title className=' font-extrabold text-2xl xl:text-3xl 2xl:text-4xl text-center max-w-5xl mx-auto w-full'>
                Explore our wide range of
                <span className='text-primary px-3 text-3xl xl:text-4xl 2xl:text-5xl'>Financial solutions</span>
                that simplify
                <span className='text-primary px-3 text-3xl xl:text-4xl 2xl:text-5xl'>Banking</span>
                and fintech systems
            </Title>


            {loading ?
                <div className='grid md:grid-cols-2 gap-6'>
                    <CardLoader />
                    <CardLoader />
                    <CardLoader />
                </div>
                :
                <div className='grid md:grid-cols-2 gap-6 w-full'>
                    {data?.length && data?.map((data: any, index: number) => (
                        <FinancialSolutionsCard key={index} data={data} />
                    ))}
                </div>
            }

        </Container>
    )
}

export default FinancialSolutions