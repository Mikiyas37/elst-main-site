import Container from '@/components/container'
import Image from 'next/image'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import bgPattern from "@public/pattern/BackgroundPattern 1.png"
import images from "@public/innovations/Rectangle742.png"

type Props = {}

function CornerStone({ }: Props) {
    return (
        <div className='mt-24 bg-[#F2F2F2] w-full items-center justify-center relative py-12'>
            <Image src={bgPattern} alt='bgpattern' fill />

            <Container>
                <div className='grid md:grid-cols-5 gap-4 md:gap-8 md:h-[50vh] w-full'>

                    <div className='col-span-3 p-2 flex flex-col gap-3 md:gap-6 justify-center'>
                        <Title className='font-bold md:leading-6 text-2xl md:text-3xl max-w-xl w-full '>
                            Quality is our cornerstone – we never compromise
                            when it comes to developing our products.
                        </Title>
                        <Text className='font-normal w-full text-justify'>
                            Excellence is ingrained in everything we do. We are committed to delivering
                            top-notch products that meet the highest standards of quality.
                            Our meticulous attention to detail and unwavering dedication ensure
                            that every product we develop is reliable, efficient, and innovative.
                            Trust us to provide solutions that not only meet but exceed your expectations,
                            because quality is at the heart of our mission.
                        </Text>
                    </div>

                    <div className='col-span-2 relative w-full'>
                        <Image src={images} alt='bgpattern' fill />
                    </div>

                </div>
            </Container>
        </div>
    )
}

export default CornerStone