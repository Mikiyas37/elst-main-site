import { useNextSanityImage } from 'next-sanity-image';
import { configuredSanityClient } from '../../configuredSanityClient';
import Image from 'next/image';
import cn from '@/utils/class-names';

export function SanityImage({ imageUrl, className }: { imageUrl: any, className: string }) {

    const imageProps = useNextSanityImage(configuredSanityClient, imageUrl);

    return <Image src={imageProps!} alt="image" className={cn(className, "shrink-0")} />;
}