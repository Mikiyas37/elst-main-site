"use client"
import { Tab, TabList, TabPanel, TabPanels, Tabs } from 'rizzui'
import React, { useEffect, useState } from 'react'
import cn from '@/utils/class-names'

// import SimpleBar from 'simplebar-react';
// import 'simplebar-react/dist/simplebar.min.css';
import SimpleBar from "@/components/ui/simplebar"
import ProductDetails from '../product-details/product-details';

import nedaj from "@public/products/nedaj.svg"
import dube from "@public/products/dube.svg"
import guzogo from "@public/products/guzogo.svg"
import getrooms from "@public/products/getrooms.svg"

import smartphone from "@public/products/smartphone.png"
import { Title } from "@/components/ui/text"
import { SanityImage } from './sanity-image'
import { getFeaturedProducts } from '@/data/get-featured-product'
import ImageLoader from '../../looader/image-loader'
import SectionLoader from '../../looader/section-loader'

type Props = {
    className: string,
}

const tabs = [
    {
        title: "Nedaj",
        icon: nedaj,
        image: smartphone,
        component: <ProductDetails />
    },
    {
        title: "Dube",
        icon: dube,
        image: smartphone,
        component: <ProductDetails />
    },
    {
        title: "Guzogo",
        icon: guzogo,
        image: smartphone,
        component: <ProductDetails />
    },
    {
        title: "Getrooms",
        icon: getrooms,
        image: smartphone,
        component: <ProductDetails />
    },
]


function ProductListTabs({ className }: Props) {
    const [data, setData] = useState([])
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        getFeaturedProducts()
            .then(data => {
                setData(data)
                setLoading(false)
            })
            .catch(error => {
                setError(error)
            });
    }, [])

    return (
        <section className={cn("w-full h-full flex", className)}>

            <Tabs className={cn("flex flex-col md:flex-row mt-6 gap-1 w-full")}>
                <TabList className={"md:max-w-[330px] w-full"}>
                    <SimpleBar>
                        <div className={cn("flex md:flex-col gap-4 md:gap-8")}>
                            {
                                loading ?
                                    <div className='flex md:flex-col gap-6'>
                                        <ImageLoader className={"h-10 md:h-[180px]"} />
                                        <ImageLoader className={"h-10 md:h-[180px]"} />
                                        <ImageLoader className={"h-10 md:h-[180px]"} />
                                    </div>
                                    : data?.length ? data?.map((data: any, index: number) => (

                                        <Tab
                                            key={index}
                                            className={({ selected }: { selected: boolean }) =>
                                                cn(
                                                    "border-[1.5px] p-2 md:p-6 flex w-full md:h-[180px] rounded-xl items-center justify-around",
                                                    selected ? "border-t-[#A03A4399] outline-none border-r-[#A03A434D] border-b-[#A03A4399] border-l-primary" : "border-[#8C65331A]"
                                                )
                                            }
                                        >

                                            <Title className='flex md:hidden'>{data?.title}</Title>

                                            <div className='hidden md:flex items-center rounded-full justify-center p-3 bg-white borde-[#F9F9F9]'>
                                                <SanityImage imageUrl={data?.logo} className={"w-16 h-16 shrink-0"} />
                                            </div>

                                            <SanityImage imageUrl={data?.image} className={"hidden md:block w-48 h-36 shrink-0"} />

                                        </Tab>
                                    )) : null}
                        </div>
                    </SimpleBar>
                </TabList>

                <TabPanels className="flex-grow w-full h-full">
                    {loading ?
                        <SectionLoader className='w-full h-full'/>
                        : data?.length ? data?.map((data: any, index: number) => (
                            <TabPanel key={index} className="md:px-5 w-full rounded-xl  h-full" >
                                <ProductDetails data={data} loading={loading} />
                            </TabPanel>
                        )) : null}
                </TabPanels>
            </Tabs>
        </section>

    )
}

export default ProductListTabs