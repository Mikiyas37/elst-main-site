import Container from '@/components/container'
import React from 'react'
import MissionCard from './cards/mission-card'
import VisionCard from './cards/vision-card'
import ValuesCard from './cards/values-card'
import { gsap } from "gsap"
import Image from 'next/image'
import lion from "@public/about-us/lion.png"
import eagle from "@public/about-us/eagle.png"
import eaglelion from "@public/about-us/eaglelion.svg"
import { Title, Text } from "@/components/ui/text"


type Props = {}

function MissionVisionValues({ }: Props) {


    return (
        <Container>
            <div className='flex flex-col gap-6 relative'>

                <section className='h-full md:h-[80vh] sticky top-32 flex flex-col md:flex-row justify-between rounded-3xl border-[1px] border-[#8C65331A] bg-[#EEF0EF]'>
                    <div className='flex-1 flex flex-col justify-between gap-4 h-full p-5 md:p-10'>
                        <Image src={eaglelion} alt="lion" className='w-16 h-16 object-cover' />

                        <div className='flex-1 h-full flex flex-col gap-1 mt-6 md:mt-12 max-w-2xl'>
                            <p className='text-sm font-medium opacity-60 uppercase'>Our Mission</p>
                            <Title className='mb-2 font-extrabold text-2xl xl:text-3xl max-w-xl'>
                                Making a Difference: Our Commitment to Positive Impact
                            </Title>
                            <Text className='font-normal opacity-85 text-base w-[90%]'>
                                At EagleLion, we are dedicated to making a difference in the world.
                                With a steadfast commitment to positive impact,
                                we strive to create meaningful change through our innovative solutions
                                and unwavering dedication. Every project we undertake is fueled
                                by our passion for driving progress and leaving a lasting, positive legacy.
                                Join us on our journey as we work tirelessly to make the world a better place,
                                one initiative at a time.
                            </Text>
                        </div>
                    </div>
                    <div className='w-full md:w-[37%] relative h-full'>
                        <Image src={lion} alt="lion" className='w-full h-full object-cover rounded-r-3xl' />
                    </div>
                </section>


                <section className='h-full md:h-[80vh] sticky top-40 mt-5 flex flex-col md:flex-row justify-between rounded-3xl border-[1px] border-[#8C65331A] bg-[#F0E6E7]'>
                    <div className='flex-1 flex flex-col justify-between gap-4 h-full p-5 md:p-10'>
                        <Image src={eaglelion} alt="lion" className='w-16 h-16 object-cover' />

                        <div className='flex-1 h-full flex flex-col gap-1 mt-6 md:mt-12 max-w-2xl'>
                            <p className='text-sm font-medium opacity-60 uppercase'>Our Vision</p>
                            <Title className='mb-2 font-extrabold text-2xl xl:text-3xl max-w-xl'>
                                Aspiring to Greatness: Our Vision for Excellence
                            </Title>
                            <Text className='font-normal opacity-85 text-base w-[90%]'>
                                At EagleLion, we aspire to greatness in everything we do.
                                Our vision for excellence drives us to push boundaries,
                                exceed expectations, and achieve remarkable results.
                                With a relentless pursuit of quality and innovation,
                                we are committed to setting new standards of excellence in our industry.
                                Join us as we strive to make our vision a reality and
                                leave a lasting legacy of greatness.
                            </Text>
                        </div>
                    </div>
                    <div className='w-full md:w-[37%] relative h-full'>
                        <Image src={eagle} alt="lion" className='w-full h-full object-cover rounded-r-3xl' />
                    </div>
                </section>


                <section className='h-full md:h-[80vh] sticky top-48 flex flex-col md:flex-row justify-between rounded-3xl border-[1px] border-[#8C65331A] bg-[#EEF0EF]' >
                    <div className='flex-1 flex flex-col justify-between gap-4 h-full p-5 md:p-10'>
                        <Image src={eaglelion} alt="lion" className='w-16 h-16 object-cover' />

                        <div className='flex-1 h-full flex flex-col gap-1 mt-6 md:mt-12 max-w-2xl'>
                            <p className='text-sm font-medium opacity-60 uppercase'>Our Values</p>
                            <Title className='mb-2 font-extrabold text-2xl xl:text-3xl max-w-xl'>
                                Living Our Values: The Heart of EagleLion
                            </Title>
                            <Text className='font-normal opacity-85 text-base w-[90%]'>
                                At EagleLion, our values are more than just words on paper—they
                                are the heartbeat of our organization.
                                Every decision we make, every action we take,
                                is guided by our unwavering commitment to integrity,
                                innovation, and excellence. Our values serve as the foundation of our culture,
                                shaping how we work together, how we serve our clients, and how we contribute to our communities.
                                Through living our values each day, we strive to create a positive impact and build a legacy of trust,
                                respect, and success.
                            </Text>
                        </div>
                    </div>
                    <div className='w-full md:w-[37%] relative h-full'>
                        <Image src={lion} alt="lion" className='w-full h-full object-cover rounded-r-3xl' />
                    </div>
                </section >


                <section className='h-full md:h-[50vh]' >
                </section>
            </div>

            {/* <MissionCard />
            <VisionCard className={""} />
            <ValuesCard className={""} /> */}



        </Container>
    )
}

export default MissionVisionValues