import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Container from '@/components/container'
import HorizonsCard from './cards/horizons-card'
import fintech from "@public/about-us/Frame 5817.svg"
import bank from "@public/about-us/Group 1171275053 (1).svg"
import entertainemnt from "@public/about-us/Group 1171275056.svg"
import media from "@public/about-us/Group 1171275057.svg"
import booking from "@public/about-us/Group 1171275055.svg"
import hotel from "@public/about-us/Group 1171275060.svg"
import other from "@public/about-us/Group 1171275056 (1).svg"
import service from "@public/about-us/Group (1).svg"
import learning from "@public/about-us/Group (2).svg"
import education from "@public/about-us/Group 1171275054 (1).svg"

type Props = {}

function Horizons({ }: Props) {

  const horizons = []


  return (
    <Container>

      <div className='mt-24 md:min-h-[70vh] h-full'>
        <div className='flex flex-col gap-4  max-w-4xl'>
          <Title className='text-2xl md:text-3xl font-extrabold max-w-lg w-full'>
            Exploring Boundless Horizons:
            Innovating Across Diverse Sectors and Industries
          </Title>

          <Text className='font-bold opacity-40 text-sm'>
            From finance and Banking systems to Education,
            Entertainment and  beyond, our innovative approach knows no bounds
          </Text>
        </div>


        <div className='grid md:grid-cols-3 gap-5 mt-12'>
          <div className='flex flex-col gap-4'>
            <HorizonsCard
              title={"Bank and Fintech Services"}
              desc={"From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances."}
              icon1={bank}
              icon1className={"h-10 w-10"}
              icon2={fintech}
              iconclassName={"flex justify-between"}
            />
            <HorizonsCard
              title={"Entertainment and Media Services"}
              desc={"From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances."}
              icon1={other}
              icon1className={"w-60"}
              icon2={service}
              iconclassName={"flex justify-between"}
            />
          </div>
          <div className='flex flex-col gap-4'>
            <HorizonsCard
              title={"Hotel resevation and Booking Services"}
              desc={"From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances."}
              icon1={entertainemnt}
              icon1className={"w-40"}
              icon2={media}
              icon2className={"w-20"}
              iconclassName={"flex justify-between"}
            />
            <HorizonsCard
              title={"Other Services"}
              desc={"From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances."}
              icon1={education}
              icon2={learning}
              iconclassName={"flex justify-between items-end"}
            />
          </div>
          <div className='flex flex-col gap-3'>
            <HorizonsCard
              title={"Education and Learning Services"}
              desc={"From modern digital banking solutions to advanced financial technology, we're here to revolutionize the way you manage your finances."}
              icon1={hotel}
              icon2={booking}
              iconclassName={"grid grid-cols-2 place-items-end place-content-end item-end justify-end"}
            />
          </div>
        </div>
      </div>
    </Container>
  )
}

export default Horizons