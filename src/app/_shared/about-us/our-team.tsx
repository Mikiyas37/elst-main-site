"use client"
import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"


import image1 from "@public/about-us/Frame 5912.png"
import image2 from "@public/about-us/Frame 5913.png"
import Image from 'next/image'


type Props = {}


function OurTeam({ }: Props) {
    return (
        <Container>
            <div className='my-24 flex flex-col gap-3 w-full h-full'>

                <div className='grid md:grid-cols-3 gap-3'>

                    <div className='flex flex-col gap-3 md:col-span-2'>
                        <p className='text-sm font-medium opacity-70 uppercase'>Our Team</p>
                        <Title className='text-2xl xl:text-3xl font-extrabold max-w-3xl w-full'>
                            Collaborating for Impact: Our Team&apos;s Collective Efforts Drive Meaningful Change
                        </Title>
                        <div className=' w-full '>
                            <Image src={image1} alt={""} className='w-full h-full md:h-[400px] object-contain' />
                        </div>
                    </div>


                    <div className='flex flex-col gap-3 md:col-span-1 w-full'>
                        <Text className='font-light opacity-90'>
                            Together, we leverage our diverse talents, perspectives, and expertise to tackle complex challenges
                            and create innovative solutions that make a lasting impact on the world.
                        </Text>
                        <div className='w-full'>
                            <Image src={image2} alt={""} className='w-full h-full md:h-[400px] object-contain' />
                        </div>
                    </div>


                </div>


            </div>
        </Container>
    )
}

export default OurTeam