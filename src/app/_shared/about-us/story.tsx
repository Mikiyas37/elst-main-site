"use client"
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import story from "@public/about-us/story.png"
import ReactPlayer from 'react-player'

type Props = {}

function Story({ }: Props) {
    return (
        <Container>
            <div className='my-24 flex flex-col gap-3 w-full h-full'>

                <div className='flex flex-col gap-1 pb-8'>
                    <Title className='text-center text-xl xl:text-2xl w-full font-extrabold'>
                        Our Story
                    </Title>
                    <Text className='font-light opacity-90 text-center text-sm max-w-3xl mx-auto'>
                        From humble beginnings to innovative breakthroughs, our story is one of resilience, vision, and relentless pursuit of excellence.
                        Discover the journey that shaped Eagle Lion System Technologies.
                    </Text>
                </div>
                <div className='w-full h-[40vh] md:h-[550px] relative mt-4'>
                    {/* <Image src={story} alt="" fill /> */}
                    <div className="video-parent-div  h-full w-full rounded-3xl">
                        {<>
                            <ReactPlayer
                                width={"100%"}
                                controls={true}
                                className={"rounded-3xl h-full aspect-video"}
                                url={"https://youtu.be/4uDuVJ3kK9s?si=7bJkEtnh1E4-hDmW"}
                                light={<Image src={story} alt="" fill />}
                            />
                        </>
                            //  <>
                            //     <div className="flex items-center justify-center h-full w-full">
                            //         <FaSpinner size={20} className={"animate-spin"} />
                            //     </div>
                            // </>
                        }
                    </div>
                </div>
            </div>
        </Container>
    )
}

export default Story