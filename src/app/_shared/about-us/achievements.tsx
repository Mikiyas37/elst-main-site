"use client"
import Container from '@/components/container'
import { productStatData } from '@/constants/constants'
import React from 'react'
import ProductStat from '../products/product-details/product-stat'
import { Title, Text } from "@/components/ui/text"
import SimpleBar from "@/components/ui/simplebar"
import AchievementCards from './cards/achievements'

import image1 from "@public/about-us/Frame 5914.png"
import image2 from "@public/about-us/Frame 5931.png"
import image3 from "@public/about-us/Frame 5932.png"
import image4 from "@public/about-us/Frame 5933.png"

type Props = {}

const achievements = [
    {
        image: image1,
        title: "Dubeale App",
        description: "Dubeale app has facilitated over 1 billion birr in loans, empowering countless users to achieve their financial goals with ease. "
    },
    {
        image: image2,
        title: "Dubeale App",
        description: "Dubeale app has facilitated over 1 billion birr in loans, empowering countless users to achieve their financial goals with ease. "
    },
    {
        image: image3,
        title: "Dubeale App",
        description: "Dubeale app has facilitated over 1 billion birr in loans, empowering countless users to achieve their financial goals with ease. "
    },
    {
        image: image4,
        title: "Dubeale App",
        description: "Dubeale app has facilitated over 1 billion birr in loans, empowering countless users to achieve their financial goals with ease. "
    },
    {
        image: image2,
        title: "Dubeale App",
        description: "Dubeale app has facilitated over 1 billion birr in loans, empowering countless users to achieve their financial goals with ease. "
    },
]
function Achievements({ }: Props) {
    return (
        <Container >
            <div className='mt-24 flex flex-col gap-3 w-full'>

                <div className='grid md:grid-cols-3 items-end'>

                    <div className='col-span-2'>
                        <p className='text-sm font-medium opacity-70 uppercase'>Our Achievements</p>
                        <div className='grid grid-cols-2 gap-4 md:grid-cols-4 justify-between p-4'>
                            {productStatData.map((data, index) => (
                                <ProductStat
                                    key={index}
                                    data={data}
                                    color={"text-primary"}
                                />
                            ))}
                        </div>
                    </div>
                    <Text className='font-semibold opacity-90 text-sm col-span-1'>
                        Explore our proudest moments and milestones at Eagle Lion System Technologies.
                        From industry recognition to impactful projects,
                        our achievements reflect our dedication to excellence and innovation.
                    </Text>
                </div>

                <SimpleBar className='w-full py-4'>
                    <div className='w-full flex gap-4 md:gap-6 2xl:gap-10'>
                        {achievements.map((data, index) => (
                            <AchievementCards
                                key={index}
                                {...data}
                                className='min-h-[300px] min-w-[350px]'
                            />
                        ))}
                    </div>
                </SimpleBar>
            </div>
        </Container>
    )
}

export default Achievements