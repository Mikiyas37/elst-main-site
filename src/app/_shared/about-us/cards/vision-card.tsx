import React from 'react'
import eagle from "@public/about-us/eagle.png"
import eaglelion from "@public/about-us/eaglelion.svg"
import Image from 'next/image'
import { Title, Text } from "@/components/ui/text"
import cn from '@/utils/class-names'


type Props = {
    className: string
}

function VisionCard({ className }: Props) {
    return (
        <div className={cn(className, 'flex justify-between rounded-3xl border-[1px] border-[#8C65331A] bg-[#F0E6E7]')}>
            <div className='flex-1 flex flex-col justify-between h-full p-10'>
                <Image src={eaglelion} alt="lion" className='w-16 h-16 object-cover' />

                <div className='flex-1 h-full flex flex-col gap-2 justify-between mt-12 max-w-2xl'>
                    <p className='text-base font-light uppercase'>Our Vision</p>
                    <Title className='font-bold text-2xl max-w-xl'>
                        Aspiring to Greatness: Our Vision for Excellence
                    </Title>
                    <Text className='font-light text-base'>
                        At EagleLion, we aspire to greatness in everything we do.
                        Our vision for excellence drives us to push boundaries,
                        exceed expectations, and achieve remarkable results.
                        With a relentless pursuit of quality and innovation,
                        we are committed to setting new standards of excellence in our industry.
                        Join us as we strive to make our vision a reality and
                        leave a lasting legacy of greatness.
                    </Text>
                </div>
            </div>
            <div className='w-[30%] relative h-full bg-red-50'>
                <Image src={eagle} alt="lion" className='w-full h-full object-cover rounded-r-3xl' />
            </div>
        </div>
    )
}

export default VisionCard