import React from 'react'
import lion from "@public/about-us/lion.png"
import eaglelion from "@public/about-us/eaglelion.svg"
import Image from 'next/image'
import { Title, Text } from "@/components/ui/text"
import cn from '@/utils/class-names'


type Props = {
    className: string
}


function ValuesCard({ className }: Props) {
    return (
        <div className={cn(className, 'flex justify-between rounded-3xl border-[1px] border-[#8C65331A] bg-[#EEF0EF]')} >
            <div className='flex-1 flex flex-col justify-between h-full p-10'>
                <Image src={eaglelion} alt="lion" className='w-16 h-16 object-cover' />

                <div className='flex-1 h-full flex flex-col gap-2 justify-between mt-12 max-w-2xl'>
                    <p className='text-base font-light uppercase'>Our Values</p>
                    <Title className='font-bold text-2xl max-w-xl'>
                        Living Our Values: The Heart of EagleLion
                    </Title>
                    <Text className='font-light text-base'>
                        At EagleLion, our values are more than just words on paper—they
                        are the heartbeat of our organization.
                        Every decision we make, every action we take,
                        is guided by our unwavering commitment to integrity,
                        innovation, and excellence. Our values serve as the foundation of our culture,
                        shaping how we work together, how we serve our clients, and how we contribute to our communities.
                        Through living our values each day, we strive to create a positive impact and build a legacy of trust,
                        respect, and success.
                    </Text>
                </div>
            </div>
            <div className='w-[30%] relative h-full bg-red-50'>
                <Image src={lion} alt="lion" className='w-full h-full object-cover rounded-r-3xl' />
            </div>
        </div >
    )
}

export default ValuesCard