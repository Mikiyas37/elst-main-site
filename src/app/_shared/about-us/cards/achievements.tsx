import Image from 'next/image'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import cn from '@/utils/class-names'

type Props = {
    className: string
    title: string,
    description: string,
    image: any
}

function AchievementCards({ className, title, description, image }: Props) {
    return (
        <div className={cn(className, 'rounded-xl')}>
            <Image src={image} alt={"achievements"} className='object-cover w-full h-[75%]' />

            <div className='flex flex-col gap-2'>
                <Title className='font-bold text-base'>
                    {title}
                </Title>
                <Text className='font-light text-xs'>
                    {description}
                </Text>

            </div>
        </div>
    )
}

export default AchievementCards