import React from 'react'
import { Title, Text } from "@/components/ui/text"
import cn from '@/utils/class-names'
import Image from 'next/image'

type Props = {
    title: string,
    desc: string,
    icon1?: string,
    icon1className?: string,
    icon2?: string,
    iconclassName?: string,
    icon2className?: string,
}

function HorizonsCard({ title, desc, icon1, icon1className, icon2, iconclassName, icon2className }: Props) {
    return (
        <div className='rounded-2xl max-w-[350px] w-full h-fit p-6 bg-[#fff] shadow-md border-[.3px] border-[#333333]/20 flex flex-col gap-2'>
            <Title className='font-extrabold text-[17px]/[18px] 2xl:text-[18px]/[21px]'>{title}</Title>
            <Text className={cn('font-bold opacity-75 text-xs')}>{desc}</Text>
            <div className={cn(iconclassName, "mt-3 ")}>
                {icon1 ? <Image src={icon1} alt="icon1" className={cn(icon1className, "object-contain")} /> : null}
                {icon2 ? <Image src={icon2} alt="icon2" className={cn(icon2className, "object-contain")} /> : null}
            </div>
        </div>
    )
}

export default HorizonsCard