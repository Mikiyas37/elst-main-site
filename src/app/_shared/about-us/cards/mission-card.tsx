import React from 'react'
import lion from "@public/about-us/lion.png"
import eaglelion from "@public/about-us/eaglelion.svg"
import Image from 'next/image'
import { Title, Text } from "@/components/ui/text"


type Props = {}

function MissionCard({ }: Props) {
    return (
        <div className='flex justify-between rounded-3xl border-[1px] border-[#8C65331A] bg-[#EEF0EF]'>
            <div className='flex-1 flex flex-col justify-between h-full p-10'>
                <Image src={eaglelion} alt="lion" className='w-16 h-16 object-cover' />

                <div className='flex-1 h-full flex flex-col gap-2 justify-between mt-12 max-w-2xl'>
                    <p className='text-base font-light uppercase'>Our Mission</p>
                    <Title className='font-bold text-2xl max-w-xl'>
                        Making a Difference: Our Commitment to Positive Impact
                    </Title>
                    <Text className='font-light text-base'>
                        At EagleLion, we are dedicated to making a difference in the world.
                        With a steadfast commitment to positive impact,
                        we strive to create meaningful change through our innovative solutions
                        and unwavering dedication. Every project we undertake is fueled
                        by our passion for driving progress and leaving a lasting, positive legacy.
                        Join us on our journey as we work tirelessly to make the world a better place,
                        one initiative at a time.
                    </Text>
                </div>
            </div>
            <div className='w-[30%] relative h-full bg-red-50'>
                <Image src={lion} alt="lion" className='w-full h-full object-cover rounded-r-3xl' />
            </div>
        </div>
    )
}

export default MissionCard