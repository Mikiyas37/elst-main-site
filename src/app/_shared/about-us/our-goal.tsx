import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import { ImArrowDownRight2 } from "react-icons/im";

type Props = {}

function OurGoal({ }: Props) {
    return (
        <Container >
            <div className='flex flex-col md:flex-row items-start gap-4 md:gap-8 max-w-7xl mx-auto w-full p-2 md:p-6'>

                <div className='flex items-center gap-2'>
                    <Title className='font-extrabold text-3xl whitespace-nowrap'>
                        Our Goal
                    </Title>
                    <ImArrowDownRight2 size={20} className='text-black font-extrabold' />
                </div>

                <div className="flex flex-col gap-3 col-span-3">
                    <Title className='md:text-xl font-extrabold'>
                        Empowering Progress: Our Commitment to Driving Positive Change and Building
                        a Brighter Future Through Innovation, Collaboration, and Purposeful Action.
                    </Title>

                    <Text className='text-base font-light opacity-90 text-justify'>
                        At Eagle Lion System Technologies, our goal is clear:
                        to drive positive change through innovation, collaboration,
                        and purposeful action. From pioneering groundbreaking technologies to
                        fostering meaningful partnerships, we are dedicated to making a meaningful
                        impact on the world around us. With a clear vision and a shared sense of purpose,
                        we are determined to leave a lasting legacy of progress and prosperity for generations to come.
                        Our journey is fueled by a deep-rooted belief in the transformative power of technology
                        and the potential of human ingenuity to overcome any obstacle in our path.
                    </Text>


                    <Text className='text-base font-light opacity-90 text-justify'>
                        At Eagle Lion System Technologies, our goal is clear:
                        to drive positive change through innovation, collaboration,
                        and purposeful action. From pioneering groundbreaking technologies to
                        fostering meaningful partnerships, we are dedicated to making a meaningful i
                        mpact on the world around us. With a clear vision and a shared sense of purpose,
                        we are determined to leave a lasting legacy of progress and prosperity for generations to come.
                        Our journey is fueled by a deep-rooted belief in the transformative power of technology
                        and the potential of human ingenuity to overcome any obstacle in our path.
                    </Text>
                </div>
            </div>
        </Container>
    )
}

export default OurGoal