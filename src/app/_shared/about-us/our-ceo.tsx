import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import CEO from "@public/about-us/Rectangle 1742 (1).png"

import { ImQuotesLeft, ImQuotesRight } from 'react-icons/im'

type Props = {}

function OurCEO({ }: Props) {
    return (
        <Container>
            <div className='my-24 grid md:grid-cols-2 max-w-7xl 3xl:max-w-8xl mx-auto w-full h-full rounded-3xl bg-[#EEF0EF]'>
                <div className='flex flex-col gap-4 p-6 md:p-12 md:pt-16'>
                    <Title className='font-extrabold text-2xl md:text-3xl'>
                        Message from our CEO
                    </Title>

                    <div className='relative py-4'>
                        {/* <Image src={startQuote} alt={"start Quote"} /> */}
                        <ImQuotesLeft size={25} className='text-primary/50 absolute top-0 left-0' />
                        <Text className='text-sm  font-light opacity-90 ps-6 md:ps-12'>
                            As the CEO of Eagle Lion System Technologies,
                            I am proud to lead a team that is passionately
                            committed to our vision of driving positive change and
                            our mission of empowering progress through innovative solutions.
                            Together, we are dedicated to pushing boundaries, fostering collaboration,
                            and creating meaningful impact in everything we do. With a relentless
                            focus on excellence and a shared commitment to integrity, we strive to
                            be at the forefront of innovation, shaping the future and making a
                            difference in the world. Thank you for joining us on this journey as
                            we continue to explore boundless horizons and make a lasting difference together.
                        </Text>
                        <ImQuotesRight size={25} className='text-primary/50 absolute bottom-0 right-0' />
                        {/* <Image src={endQuote} alt={"start Quote"} /> */}
                    </div>


                    <div className=''>
                        <Title className='font-bold text-base'>
                            Bersufekad Getachew Amare
                        </Title>

                        <Text className='text-sm font-light'>
                            CEO and Founder
                        </Text>
                    </div>

                </div>

                <div className='w-full h-full flex items-end justify-end'>
                    <Image src={CEO} alt={""} className='content-end w-full h-full object-contain' />
                </div>
            </div>
        </Container>
    )
}

export default OurCEO