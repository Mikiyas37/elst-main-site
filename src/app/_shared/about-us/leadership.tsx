import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Container from '@/components/container'
import image1 from "@public/about-us/Frame 5919.png"
import image2 from "@public/about-us/Frame 5919 (1).png"
import Image from 'next/image'
import { FaLinkedin } from "react-icons/fa";
import bgPattern from "@public/pattern/BackgroundPattern3.png"

type Props = {}

const leadership = [
    {
        image: image1,
        name: "Solomon Kebede",
        title: "Chief Technology Officer(CTO)"
    },
    {
        image: image2,
        name: "Tigist Alemu",
        title: "Chief Technology Officer(CTO)"
    },
    {
        image: image1,
        name: "Solomon Kebede",
        title: "Chief Technology Officer(CTO)"
    },
    {
        image: image2,
        name: "Tigist Alemu",
        title: "Chief Technology Officer(CTO)"
    },
    {
        image: image1,
        name: "Solomon Kebede",
        title: "Chief Technology Officer(CTO)"
    },
    {
        image: image2,
        name: "Tigist Alemu",
        title: "Chief Technology Officer(CTO)"
    },
    {
        image: image1,
        name: "Solomon Kebede",
        title: "Chief Technology Officer(CTO)"
    },
    {
        image: image2,
        name: "Tigist Alemu",
        title: "Chief Technology Officer(CTO)"
    },
]

function Leadership({ }: Props) {
    return (
        <Container >
            <div className={"my-24 flex flex-col gap-8"}>

                <div className='max-w-5xl w-full mx-auto flex flex-col gap-1'>

                    <Title className='font-extrabold text-2xl xl:text-3xl text-center'>
                        Meet our Leadership team
                    </Title>

                    <Text className='font-light opacity-90 text-sm mx-auto max-w-6xl text-center'>
                        Get to know the driving force behind Eagle Lion System Technologies.
                        Our leadership team brings together diverse expertise, unwavering dedication,
                        and a shared vision to guide our company towards success.
                    </Text>
                </div>


                <div className='grid grid-cols-2 md:grid-cols-4 gap-5 md:gap-12 max-w-6xl mx-auto'>
                    {leadership.map(({ image, name, title }, index) => (
                        <div key={index}
                            className='flex flex-col gap-3'>
                            <div className='relative md:h-[180px] md:w-[250px] rounded-3xl'>
                                <Image src={image} alt={name} className={"w-full h-full object-contain"} />
                                {/* <Image src={bgPattern} alt={name} fill className='object-left -z-10'/>
                                <Image src={bgPattern} alt={name} fill className='object-right -z-10'/> */}
                            </div>

                            <div className='flex flex-col gap-2'>
                                <Title className='font-bold text-base'>
                                    {name}
                                </Title>
                                <Text className='flex justify-between font-light text-xs md:text-sm'>
                                    {title}
                                    <FaLinkedin size={25} className='text-primary' />
                                </Text>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </Container>
    )
}

export default Leadership