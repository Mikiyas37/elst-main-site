import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"

type Props = {}

function Hero({ }: Props) {
    return (
        <Container className={"my-24"}>
            <div className='flex flex-col gap-2 max-w-5xl mx-auto'>
                <Title className='text-center text-3xl xl:text-[36px]/[45px] w-full font-extrabold'>
                    Our Story: Building Bridges, Creating Opportunities.
                </Title>
                <Text className='font-light max-w-3xl mx-auto text-center text-base'>
                    Welcome to EagleLion, where innovation meets excellence. Discover our story and commitment to delivering top-quality solutions that exceed expectations.
                </Text>
            </div>

        </Container>
    )
}

export default Hero