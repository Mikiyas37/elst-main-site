"use client"
import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import cn from "@/utils/class-names";
import { PiCaretDownBold } from "react-icons/pi";
import { Collapse } from "@/components/ui/collapse"

type Props = {}

const faqData = [
    {
        id: 1,
        question: "What services do you offer?",
        answer: "We offer a wide range of software development services, including web development, mobile app development, custom software development, UI/UX design, and quality assurance testing."
    },
    {
        id: 2,
        question: "How long does it take to develop a software project?",
        answer: "We offer a wide range of software development services, including web development, mobile app development, custom software development, UI/UX design, and quality assurance testing."
    },
    {
        id: 3,
        question: "What technologies do you specialize in?",
        answer: "We offer a wide range of software development services, including web development, mobile app development, custom software development, UI/UX design, and quality assurance testing."
    },
    {
        id: 4,
        question: "Do you provide ongoing support and maintenance after the project is completed?",
        answer: "We offer a wide range of software development services, including web development, mobile app development, custom software development, UI/UX design, and quality assurance testing."
    },
    {
        id: 5,
        question: "How do you ensure the quality of your software products?",
        answer: "We offer a wide range of software development services, including web development, mobile app development, custom software development, UI/UX design, and quality assurance testing."
    },
]

function FAQ({ }: Props) {
    return (
        <Container>
            <div className='flex flex-col gap-12 my-24'>

                <div className='flex flex-col gap-1'>
                    <Title className='text-center font-extrabold text-2xl'>
                        Frequently Ask Question
                    </Title>
                    <Text className='text-sm font-light opacity-90 text-center max-w-4xl mx-auto'>
                        Find answers to common queries about our services.
                        Browse our FAQ section to get clarity on key topics,
                        understand our processes better, and make informed
                        decisions about partnering with us
                    </Text>
                </div>

                <Container
                    className="max-w-6xl mx-auto flex flex-col items-start gap-6 w-full"
                    clean
                >
                    {faqData.map((faq) => (
                        <Collapse
                            key={faq.id}
                            className="w-full boder-[0.5px] border-[#8C65331A] bg-[#F9F9F9] shadow-lg shadow-black/5"
                            duration={500}
                            header={({ open, toggle }) => (
                                <div
                                    onClick={toggle}
                                    className={cn(
                                        "w-full  flex items-center text-white p-4 rounded-md cursor-pointer justify-between border border-white/20 bg-white/[0.08]"
                                    )}
                                >
                                    <span className="flex text-[#333333] font-extrabold text-base items-center">{faq.question}</span>

                                    <PiCaretDownBold
                                        strokeWidth={3}
                                        className={cn(
                                            "h-3.5 w-3.5 -rotate-90 text-primary transition-transform duration-200",
                                            open && "rotate-40 "
                                        )}
                                    />
                                </div>
                            )}
                        >
                            <div className="text-[#333333] font-light text-xs p-4 rounded-md max-w-3xl w-full">
                                <Text className='font-light opacity-90'>{faq.answer}</Text>
                            </div>
                        </Collapse>
                    ))}
                </Container>
            </div>
        </Container>
    )
}

export default FAQ