"use client"
import React, { useEffect, useState } from 'react'
import { Title, Text } from "@/components/ui/text"
import Container from '@/components/container'
import Image from 'next/image'
import image from "@public/insights/Frame6034.png"
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'
import { getCategoryById } from '@/data/get-category'
import { getBlogDetails, getBlogsByCategory } from '@/data/get-blogs'
import { SanityImage } from '../products/product-tabs/sanity-image'
import Link from 'next/link'
import { routes } from '@/configs/routes'
import { usePathname } from 'next/navigation'
import SectionLoader from '../looader/section-loader'
import ImageLoader from '../looader/image-loader'

type Props = {}

function Relatednews({ }: Props) {

    const [data, setData] = useState<any>([])
    const [category, setCategory] = useState<any>([])
    const [blogs, setBlogs] = useState<any>([])
    const pathName = usePathname()
    const slug = pathName.split("/").pop()

    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        getBlogDetails(slug!)
            .then(data => {
                setData(data)
                setLoading(false)
            })
            .catch(error => {
                setError(error)
            });
    }, [slug])


    useEffect(() => {
        setCategory([]); // Clear previous blogs (optional)
        getCategoryById(data[0]?.category?._ref)
            .then(data => {
                setCategory(data)
            });

        setBlogs([])
        getBlogsByCategory(data[0]?.category?._ref)
            .then(data => {
                setBlogs(data)
            });

    }, [data])


    return (
        <Container>
            <div className='my-24 grid md:grid-cols-4 gap-4'>
                <div className='md:col-span-3 flex flex-col gap-8 p-1.5 md:p-4'>

                    {loading ?
                        <>
                            <SectionLoader />
                        </>
                        : data[0]?.details?.map((detail: any, index: number) => (
                            <div key={index} className='flex flex-col gap-3'>
                                <Title className='text-xl font-extrabold'>
                                    {detail?.subtitle}
                                </Title>
                                <Text className='text-base font-light opacity-90'>
                                    {detail?.description[0]?.children[0]?.text}
                                </Text>
                            </div>
                        ))}


                </div>

                {/* Related news */}
                <div className='md:col-span-1 flex flex-col gap-6 w-full'>
                    <Text className='text-lg font-bold'>
                        Related {category[0]?.title}
                    </Text>
                    {loading ?
                        <div className='grid grid-cols-2 md:grid-cols-1 gap-6 w-full'>
                            {Array(3).fill(0).map((_, index) => (
                                <ImageLoader key={index}
                                    className='min-h-[150px]' />
                            ))}
                        </div>
                        :
                        <div className='grid grid-cols-2 md:grid-cols-1 gap-6 w-full'>
                            {blogs.length ? blogs?.map((blog: any, index: number) => (
                                <Link key={index} href={routes.insights + `/${blog?._id}`} >
                                    <SanityImage imageUrl={blog.image} key={index} className={"w-full h-full rounded-lg object-cover"} />
                                </Link>
                            )) : null}
                        </div>

                    }
                </div>
            </div>

        </Container>
    )
}

export default Relatednews