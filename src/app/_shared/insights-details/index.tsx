"use client"
import React, { useEffect, useState } from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import { Button } from "@/components/ui/button"
import Image from 'next/image'
import logo from "@public/insights/Group 1171275164 (1).svg"
import calender from "@public/insights/Group.svg"
import timer from "@public/insights/Vector (21).svg"
import frame from "@public/insights/Frame.png"
import { getCategoryById } from '@/data/get-category'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'
import { formatISODate } from '@/utils/formate-iso-date'
import { getBlogDetails } from '@/data/get-blogs'

import { usePathname } from 'next/navigation'

type Props = {
}

function Hero({ }: Props) {

  const [category, setCategory] = useState<any>([])
  const pathName = usePathname()
  const slug = pathName.split("/").pop()
  const [data, setData] = useState<any>([])

  const [error, setError] = useState(null)
  const [loading, setLoading] = useState(false)
  const imageProps = useNextSanityImage(configuredSanityClient, data[0]?.image);

  useEffect(() => {
    setLoading(true)
    getBlogDetails(slug!)
      .then(data => {
        setData(data)
        setLoading(false)
      })
      .catch(error => {
        setError(error)
      });
  }, [slug])


  useEffect(() => {
    setCategory([]); // Clear previous blogs (optional)
    getCategoryById(data[0]?.category?._ref).then(data => setCategory(data));
  }, [data])


  return (
    <Container>
      {loading ?
        <>
          Loading...
        </>
        :
        <div className='mb-24 mt-12 mx-auto max-w-4xl flex flex-col gap-5 items-center justify-center'>
          {category[0]?.title &&
            <Button className='bg-[#A03A431A] uppercase max-w-[8rem] w-full text-primary rounded-full border-[0.3px] text-xs font-bold'>
              {category[0]?.title}
            </Button>}
          <Title className='font-extrabold text-2xl md:text-3xl 2xl:text-5xl text-center'>
            {data[0]?.title}
          </Title>

          <div className='flex flex-col gap-1 md:flex-row justify-between w-full max-w-xl mx-auto'>
            <Text className='font-bold flex gap-2 items-center'>
              <Image src={logo} alt={""} />
              Eaglelion system technologies
            </Text>

            <Text className='ms-3 md:ms-0 flex items-center gap-2'>
              <Image src={calender} alt={"calender"} />
              {data[0]?._updatedAt && formatISODate(data[0]?._updatedAt)}
            </Text>

            <Text className='ms-3 md:ms-0 flex gap-2 items-center'>
              <Image src={timer} alt={"timer"} />
              5 min read
            </Text>
          </div>
        </div>
      }
      {imageProps &&
        <div className='relative w-full md:h-[550px]'>
          <Image src={imageProps} alt={""} fill />
        </div>}
    </Container>
  )
}

export default Hero