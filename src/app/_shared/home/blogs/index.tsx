"use client";
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import BlogTabs from './blog-tabs'

type Props = {
    getCatergory: any
}

function Blogs({ getCatergory }: Props) {
    return (
        <Container className='mt-24 flex flex-col gap-5 h-full'>
            <Title className="text-3xl md:text-3xl xl:text-4xl text-[#424143] font-bold">
                Browse our latest blog articles
            </Title>

            <BlogTabs getCatergory={getCatergory} />
        </Container>
    )
}

export default Blogs