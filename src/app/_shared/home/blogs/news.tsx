"use client"
import React, { useState } from 'react'
import Slider from "react-slick";
// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { IoIosArrowRoundBack, IoIosArrowRoundForward } from "react-icons/io";

import image1 from "@public/blogs/blog1.png"
import image2 from "@public/blogs/blog2.png"
import image3 from "@public/blogs/blog3.png"
import Blog from './blog';
import useMedia from 'react-use/lib/useMedia';
import { client } from '@sanity/lib/client';
import { getBlogs } from '@/data/get-blogs';
import { Text } from "@/components/ui/text"
import CardLoader from '../../looader/card-loader';

type Props = {
  blogs: any,
  loading: boolean
}

// const blogs = [
//   {
//     title: "AI: Shaping the Future of Tech",
//     description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront, reshaping industries and revolutionizing the way we ....",
//     date: "April 12,2024",
//     image: image1
//   },
//   {
//     title: "AI: Shaping the Future of Tech",
//     description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront, reshaping industries and revolutionizing the way we ....",
//     date: "April 12,2024",
//     image: image2
//   },
//   {
//     title: "AI: Shaping the Future of Tech",
//     description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront, reshaping industries and revolutionizing the way we ....",
//     date: "April 12,2024",
//     image: image3
//   },
//   {
//     title: "AI: Shaping the Future of Tech",
//     description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront, reshaping industries and revolutionizing the way we ....",
//     date: "April 12,2024",
//     image: image1
//   },
//   {
//     title: "AI: Shaping the Future of Tech",
//     description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront, reshaping industries and revolutionizing the way we ....",
//     date: "April 12,2024",
//     image: image3
//   },
// ]



function News({ loading, blogs }: Props) {

  const isMobile = useMedia('(max-width: 640px)', false);

  const NextArrow = (props: any) => {
    const { onClick } = props;
    return (
      <div
        className="p-3 bg-[#EEEEEE] border-[3px] group hover:border-primary hover:bg-white cursor-pointer duration-200 rounded-full text-2xl flex items-center justify-center z-20 absolute -bottom-20 left-[30%] md:left-[47%]"
        onClick={onClick}
      >
        <IoIosArrowRoundBack className='hover:text-primary hover:font-bold' />
      </div>
    );
  };
  const PrevArrow = (props: any) => {
    const { onClick } = props;
    return (
      <div
        className="p-3 bg-[#EEEEEE] border-[3px] hover:border-primary hover:bg-white cursor-pointer duration-200 rounded-full text-2xl flex items-center justify-center z-20 absolute -bottom-20 left-[60%] md:left-[53%]"
        onClick={onClick}
      >
        <IoIosArrowRoundForward className='hover:text-primary hover:font-bold' />
      </div>
    );
  };
  var settings = {
    className: "flex gap-8",
    dots: false,
    infinite: true,
    autoplay: false,
    slidesToShow: isMobile ? 1 : 3,
    slidesToScroll: isMobile ? 1 : 3,
    // slidesToShow: 1,
    // slidesToScroll: 1,
    arrows: true,
    nextArrow: <PrevArrow />,
    prevArrow: <NextArrow />,
  };

  return (
    <div className="slider-container">
      {loading ?
        <div className='grid md:grid-cols-3 gap-4 w-full'>
          {Array(isMobile ? 1 : 3).fill(0).map((_, index) => (
            <CardLoader key={index}/>
          ))}
        </div>
        : blogs.length === 1 ? (
          <Blog blog={blogs[0]} key={"Blog"} /> // Use blog ID or unique key
        ) : (
          blogs.length <= 3 && isMobile ?
            <Slider {...settings} className='w-full h-full gap-6'>
              {blogs?.map((blog: any, index: number) => (
                <Blog blog={blog} key={index + "Blog"} />
              ))}
            </Slider>
            : blogs.length <= 3 && !isMobile ?
              <div className='grid md:grid-cols-3 w-full'>
                {

                  blogs?.map((blog: any, index: number) => (
                    <Blog blog={blog} key={index + "Blog"} />
                  ))
                }
              </div>
              :
              <Slider {...settings} className='w-full h-full gap-6'>
                {blogs?.map((blog: any, index: number) => (
                  <Blog blog={blog} key={index + "Blog"} />
                ))}
              </Slider>

        )}
    </div>
  );
}

export default News
