// "use client";
import { Tab, TabList, TabPanel, TabPanels, Tabs, Text } from 'rizzui'
import React, { useEffect, useState } from 'react'
import cn from '@/utils/class-names'
import News from './news'
import Podcasts from './podcasts'
import Articles from './articles'
import Insights from './insights'

import SimpleBar from 'simplebar-react';
import 'simplebar-react/dist/simplebar.min.css';
import { useRouter } from 'next/navigation';
import { getBlogsByCategory } from '@/data/get-blogs';

type Props = {
    getCatergory: any
}

// const tabs = [
//     {
//         title: "Articles",
//         component: <News />
//     },
//     {
//         title: "Podcasts",
//         component: <News />
//     },
//     // {
//     //     title: "Articles and Blogs",
//     //     component: <News />
//     // },
//     // {
//     //     title: "Insights",
//     //     component: <News />
//     // },
// ]



function BlogTabs({ getCatergory }: Props) {
    const [blogs, setBlogs] = useState([])
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)

    function handleTabClick(categoryId: string) {
        setBlogs([]); // Clear previous blogs (optional)
        setLoading(true)
        getBlogsByCategory(categoryId)
            .then(data => {
                setBlogs(data)
                setLoading(false)
            })
            .catch(error => {
                setError(error)
            });
    }

    useEffect(() => {
        handleTabClick(getCatergory[0]?._id)
    }, [getCatergory])

    return (
        <section className='w-full h-full'>
            <Tabs className={"mt-6 flex flex-col gap-8"} defaultIndex={0}>
                <SimpleBar>
                    <TabList className={"max-w-fit flex gap-8 justify-between md:px-6 mt-3 w-full"}>
                        {getCatergory?.length && getCatergory?.map((data: any, index: number) => (
                            <Tab key={index}
                                onClick={() => handleTabClick(data?._id)}
                                className={({ selected }: { selected: boolean }) =>
                                    cn(
                                        "text-black pb-2 whitespace-nowrap font-gilroy-extrabold text-[18px]/[22.5px] border-b-4",
                                        selected ? " border-[#A03A43] outline-none font-bold text-[#A03A43]" : "border-transparent"
                                    )
                                }
                            >
                                <Text className='font-bold text-xl'>{data?.title}</Text>

                            </Tab>
                        ))}
                    </TabList>
                </SimpleBar>
                <TabPanels>
                    {getCatergory?.length && getCatergory?.map((_: any, index: number) => (
                        <TabPanel key={index} className={"2xl:px-8"}>
                            <News loading={loading} blogs={blogs} />
                        </TabPanel>
                    ))}
                </TabPanels>
            </Tabs>
        </section>
    )
}

export default BlogTabs