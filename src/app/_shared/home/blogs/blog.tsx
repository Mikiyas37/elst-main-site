
import React from 'react'
import Image, { StaticImageData } from 'next/image'
import { useNextSanityImage } from 'next-sanity-image';
import { Title, Text } from "@/components/ui/text"
import { configuredSanityClient } from '../../configuredSanityClient';
import Link from 'next/link';
import { routes } from '@/configs/routes';
import { formatISODate } from '@/utils/formate-iso-date';

type Props = {
    blog: any
}

function Blog({ blog }: Props) {


    const imageProps = useNextSanityImage(configuredSanityClient, blog?.image);


    return (
        <Link href={routes.insights + `/${blog?._id}`} className='flex flex-col gap-3 p-3 w-fit hover:bg-[#ffffff] hover:shadow-md hover:rounded-lg'>
            <Image
                src={imageProps!}
                alt={blog?.title}
                width={"200"}
                height={"200"}
                className='max-w-[24rem] 2xl:max-w-[26rem] w-full aspect-video object-cover rounded-xl'
            />

            <div className='flex flex-col gap-1 pr-3'>
                <Text className='text-[14px]/[17.15px] text-[#333333] font-light'>{blog?._updatedAt && formatISODate(blog?._updatedAt)}</Text>
                <Title className='text-[24px]/[30px] font-semibold line-clamp-1'>{blog?.title}</Title>
                <Text className='font-normal opacity-80 text-sm leading-4 max-w-[24rem] 2xl:max-w-[26rem] w-full text-justify line-clamp-3'>
                    {blog?.description && blog?.description[0]?.children[0]?.text}
                </Text>
            </div>
        </Link>
    )
}

export default Blog