"use client"
import React, { useState } from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import WhyChooseUs from './why-choose-us'
import image from "../../../../../public/choose-us/Frame6140.png"
import Image from 'next/image'

type Props = {}

const whyChoose = [
    {
        id: 1,
        title: "01 - Commitment",
        description: "Our commitment to our clients is unwavering. We go above and beyond to ensure your success and satisfaction with every project."
    },
    {
        id: 2,
        title: "02 - Excellence",
        description: "Our commitment to our clients is unwavering. We go above and beyond to ensure your success and satisfaction with every project."
    },
    {
        id: 3,
        title: "03 - Quality",
        description: "Our commitment to our clients is unwavering. We go above and beyond to ensure your success and satisfaction with every project."
    },
    {
        id: 4,
        title: "04 - Innovation",
        description: "Our commitment to our clients is unwavering. We go above and beyond to ensure your success and satisfaction with every project."
    },
]



function ChooseUs({ }: Props) {
    const [openId, setOpenId] = useState(1); // Track currently opened WhyChooseUs (if any)

    const handleOpen = (id: any) => {
        setOpenId(id === openId ? null : id); // Toggle open state based on clicked id
    };

    return (
        <Container className={"mt-24 grid grid-cols-1 gap-8 mx-auto items-center md:grid-cols-2 h-full md:h-[70vh]"}>
            <div className='flex items-center justify-center p-2'>
                <div className='flex flex-col gap-2'>
                    <Title className='font-bold text-2xl'>
                        Why you choose us?
                    </Title>
                    <Text className='font-light text-center max-w-xl'>
                        Embark on a journey of financial innovation with us, where tradition meets cutting-edge technology. As leaders in the field.
                    </Text>
                    <div className='flex flex-col gap-4 mt-5 max-w-lg'>
                        {whyChoose.map((whyChoose, index) => (
                            <WhyChooseUs key={index} whyChoose={whyChoose}
                                open={whyChoose.id === openId} // Pass open state based on id
                                setOpen={handleOpen}
                            />
                        ))}
                    </div>
                </div>
            </div>

            <div className='max-w-lg overflow-hidden relative h-[28rem]'
            // style={{ backgroundImage: `url('../../../../../public/choose-us/Frame6140.png')`, backgroundSize: "cover", backgroundRepeat: "no-repeat" }}
            >
                <Image src={image} alt="" className='w-max h-full object-cover z-10 -rotate-[15] rounded-xl' />
                <Image src={image} alt="" className='absolute top-0 w-max h-full object-cover -rotate-6 rounded-xl' />
            </div>

        </Container>
    )
}

export default ChooseUs