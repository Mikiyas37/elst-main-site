import React, { useState } from 'react'
import { Title, Text } from "@/components/ui/text"
type Props = {
    whyChoose: {
        id: any,
        title: string,
        description: string
    },
    open: boolean,
    setOpen: (id: any) => void
}

function WhyChooseUs({
    whyChoose: { id, title, description }, open, setOpen
}: Props) {

    const [isOpen, setIsOpen] = useState(false); // Local state for WhyChooseUs

    function handleClick() {
        setIsOpen(!isOpen); // Toggle open state on click within WhyChooseUs

        // Close other WhyChooseUs components if current one is opened
        if (isOpen) {
            setOpen?.(id); // Pass id to parent to close siblings (if parent provides setOpen)
        }
    };


    return (
        <button onClick={handleClick}
            className='flex flex-col p-5 gap-1 rounded-lg cursor-pointer border-[0.03px] border-[#A03A434D] bg-[#ffffff] shadow-xl shadow-[#3333330D]'>
            <Title className='font-bold text-primary'>
                {title}
            </Title>
            {open &&
                <Text className='font-light text-sm'>
                    {description}
                </Text>
            }
        </button>
    )
}

export default WhyChooseUs