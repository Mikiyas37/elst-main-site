"use client"
import { useNextSanityImage } from 'next-sanity-image'
import Image from 'next/image'
import React, { useState } from 'react'
import { configuredSanityClient } from '../../configuredSanityClient'

type Props = {
    logo: any
}

const CompanyLogo = ({ logo }: Props) => {

    const imageProps = useNextSanityImage(configuredSanityClient, logo?.image);


    // const [currentImage, setCurrentImage] = useState(logo.image2);

    // const handleMouseEnter = () => {
    //     setCurrentImage(logo.image2);
    // };

    // const handleMouseLeave = () => {
    //     setCurrentImage(logo.image1);
    // };

    return (
        imageProps &&
        <Image
            className='w-32 h-16 object-contain'
            // grayscale hover:grayscale-0
            src={imageProps}
            alt={"LOGO"}
        // onMouseEnter={handleMouseEnter}
        // onMouseLeave={handleMouseLeave}
        />
    )
}

export default CompanyLogo