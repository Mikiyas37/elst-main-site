"use client";
import React from "react";
import Container from "@/components/container";
import { Title, Text } from "@/components/ui/text";

import "@/css/hero-image-scroll.css";
import CustomButton from "@/components/custom-button";
import Products from "./products";
import { ProductsOrbits } from "./orbiting-circles";
import { routes } from "@/configs/routes";
import Image from "next/image";
import cfs from "@public/products/cfs.svg"
import chatBirr from "@public/products/chatbirr.svg"
import chinet from "@public/products/chinet.svg"
import { Icons } from "../../../../../components/icons";
import OrbitingItems from "../../../../../components/animata/list/orbiting-items";

const Hero = () => {
  return (
    <div className="relative overflow-hidden w-full h-[70vh] md:h-screen mx-auto">
      {/* <Products /> */}
      <ProductsOrbits />
      {/* <div className="mt-48">

        <OrbitingItems
          items={[
            <Icons.gitHub key="github" className="h-6 w-6" />,
            <Icons.twitter key="twitter" className="h-6 w-6" />,
            <Icons.react key="yarn" className="h-6 w-6" />,
            <Icons.tailwind key="tailwind" className="h-6 w-6" />,
            <Icons.framerMotion key="framer" className="h-6 w-6" />,
            <Icons.apple key="apple" className="h-6 w-6" />,
          ]}
          radius={50}
        />
      </div> */}

      {/* <div className="flex flex-col gap-4 items-center justify-center w-full h-full -mt-6 md:-mt-28 z-10">
        <Title className="text-[#333333] font-extrabold text-center text-2xl md:text-4xl 2xl:text-[40px]/[45px]">
          Leading the Charge in Financial Innovation.
          <br />
          Your Partner in Banking Excellence.
        </Title>

        <Text className="font-bold opacity-50 max-w-xl 2xl:max-w-2xl text-center text-[#333333]/50 text-[16px]/[22px] px-3 md:px-0">
          Embark on a journey of financial innovation with us, where tradition meets cutting-edge technology. As leaders in the field, we pave the way for banking excellence, offering tailored solutions to meet your every need.
        </Text>

        <CustomButton
          href={routes.products}
          text={"Explore"}
        />
      </div> */}


    </div>
  );
};

export default Hero;
