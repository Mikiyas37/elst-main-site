import { Tab, TabList, TabPanel, TabPanels, Tabs, Text } from 'rizzui'
import React from 'react'
import cn from '@/utils/class-names'
import Creativity from './creativity'
import Teamwork from './teamwork'
import Impact from './impact'
import SimpleBar from "@/components/ui/simplebar"

type Props = {}

const tabs = [
    {
        title: "Creativity",
        component: <Creativity />
    },
    {
        title: "Teamwork",
        component: <Creativity />
    },
    {
        title: "Quality",
        component: <Creativity />
    },
    {
        title: "Innovation",
        component: <Creativity />
    },
    {
        title: "Impact",
        component: <Creativity />
    },
]
function CreativeSolutions({ }: Props) {

    return (
        <section className='bg-[#F2F2F2] rounded-3xl w-full h-fit md:pb-12 px-3'>
            <Tabs className={"mt-6"} defaultIndex={0}>
                <SimpleBar className='w-full h-full pb-4 md:pb-0'>
                    <TabList className={"max-w-fit flex gap-4 md:gap-8 2xl:gap-12 justify-between md:px-6 mt-3 w-full"}>
                        {tabs.map((tab, index) => (
                            <Tab key={index}
                                className={({ selected }: { selected: boolean }) =>
                                    cn(
                                        "text-black",
                                        selected ? "text-[#A03A43] font-extrabold outline-none text-[18px]/[20px] 2xl:text-[24px]/[25px]" : "text-[#8C6533] font-bold text-[14px] 2xl:text-[20px]/[25px]"
                                    )
                                }
                            >
                                <Text>{tab.title}</Text>

                            </Tab>
                        ))}
                    </TabList>
                </SimpleBar>

                <TabPanels className={""}>
                    {tabs.map(({ component }, index) => (
                        <TabPanel key={index}>
                            {component}
                        </TabPanel>
                    ))}
                </TabPanels>

            </Tabs>
        </section>
    )
}

export default CreativeSolutions