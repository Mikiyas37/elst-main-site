import React from 'react'
import { Title, Text } from '@/components/ui/text';
import Image from 'next/image';
import image1 from "@public/creations/Group 1171275056 (1).png"
import image2 from "@public/creations/Group 1171275052 (1).png"
import image3 from "@public/creations/Component 9 (1).png"
import ImageSlider from './image-slider';
import bgPattern from "@public/pattern/BackgroundPattern 1.png"
import art from "@public/creations/art.png"


function Creativity() {
  return (
    <div className='grid grid-cols-1 md:grid-cols-2 gap-16 justify-between p-2 md:p-4 h-fit relative'>
      <Image src={bgPattern} alt='bgpattern' fill />
      <div className='flex flex-col mt-8 h-full justify-center'>
        <Title as="h1" className='font-extrabold text-xl md:text-[30px]/[40px] text-[#333333] 2xl:text-[30px]/[40px] w-full'>
          Redefining Possibilities with <br />
          Creative Solutions
        </Title>

        <div className='flex justify-center items-center w-full h-full mx-auto '>
          <div className='relative md:min-h-[230px] max-w-md w-full p-8 flex flex-col gap-1 rounded-2xl bg-white shadow-lg'>
            <Title className='font-bold text-xl'>
              Creativity
            </Title>
            <Text className='text-sm 2xl:text-base '>
              Creativity is at the heart of everything we do. Our innovative solutions are designed to transform businesses and drive success.
            </Text>

            <Image src={image1} alt='' className='object-contain hidden md:block w-full h-40 absolute -bottom-12 2xl:-bottom-20 -left-36 2xl:-left-44' />
            <Image src={image2} alt='' className='object-contain hidden md:block w-full h-28 absolute top-[45%] left-[40%]' />
            <Image src={image3} alt='' className='object-contain hidden md:block w-32 h-32 absolute -bottom-16 2xl:-bottom-20 left-1/3 right-1/3 2xl:left-1/2 2xl:right-1/2 mx-auto' />

          </div>

        </div>
      </div>

      <div className='relative flex justify-center items-center overflow-hidden max-h-[450px] w-full'>
        {/* <ImageSlider /> */}
        <Image src={art} alt="" className='w-full h-full object-contain' />
      </div>
    </div>
  )
}

export default Creativity;