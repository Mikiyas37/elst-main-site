import React from 'react'
import Slider from "react-slick";
// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Image from 'next/image';

import image1 from "@public/creations/Rectangle1785.png"
import image2 from "@public/creations/Rectangle1784.png"
import image3 from "@public/creations/Rectangle1786.png"

type Props = {}

function ImageSlider({ }: Props) {

    // var settings = {
    //     className: "flex gap-6",
    //     dots: false,
    //     infinite: true,
    //     autoplay: false,
    //     slidesToShow: isMobile ? 1 : 3,
    //     slidesToScroll: isMobile ? 1 : 3,
    //     arrows: true,
    //     nextArrow: <NextArrow />,
    //     prevArrow: <PrevArrow />,
    // };

    const settings = {
        className: "center slider variable-width",
        variableWidth: true,
        centerMode: true,
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 1,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 600,
    };

    return (
        <div className="slider-container w-full h-full">
            <Slider {...settings} className="flex gap-3 h-full w-full relative">
                <div className='relative w-full h-72 md:h-96'>
                    <Image src={image1} alt='' className='object-cover w-full h-full ' />
                </div>
                <div className='relative w-full h-72 md:h-96'>
                    <Image src={image2} alt='' className='object-cover w-full h-full' />
                </div>
                <div className='relative w-full h-72 md:h-96'>
                    {/* <Image src={image3} alt='' className='object-cover w-full h-full' /> */}
                </div>
            </Slider>
        </div>
    )
}

export default ImageSlider