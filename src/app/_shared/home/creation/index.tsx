"use client";
import Container from '@/components/container'
import React from 'react'
import { Text } from '@/components/ui/text'

import { BiLike } from "react-icons/bi";
import TCI from './tci';
import CreativeSolutions from './tabs';

type Props = {}

function Creations({ }: Props) {
    return (
        <Container className='flex flex-col gap-12 mt-24 max-w-8xl  mx-auto'>

            <div className='grid grid-cols-1 md:grid-cols-5 gap-8'>
                <Text className='md:col-span-3 font-semibold text-[#333333] text-2xl md:text-[30px]/[40px] 2xl:text-[36px]/[48px] px-3'>
                    Creating Impact Through Innovation: <br />
                    Our Process for Bringing <br />
                    Your Ideas to Life
                </Text>
                <div className='md:col-span-2 flex flex-col justify-between gap-3 px-3'>
                    <Text className='flex items-center text-[#333333] font-light text-[14px]/[25px] gap-3'>
                        <BiLike size={20} />
                        From concept to creation
                    </Text>
                    <Text className='font-medium max-w-2xl text-[16px]/[23px] text-[#333333] w-full text-justify'>
                        From the initial spark of an idea to the final product, we turn visions into reality with precision and passion. Experience seamless innovation and unparalleled craftsmanship with our expert team, dedicated to bringing your concepts to life.
                    </Text>
                </div>
            </div>

            {/* Teamwork, Creativity and Impact */}
            {/* <TCI /> */}
            <CreativeSolutions />


        </Container>
    )
}

export default Creations