import React from 'react'
import { Title, Text } from "@/components/ui/text"
import CountUp from 'react-countup';
import { gsap } from "gsap";
import NumberTicker from '@/components/magicui/number-ticker';

type Props = {
    name: string,
    value: number,
    description: string
}

function Statistics({ name, value, description }: Props) {

    gsap.timeline({
        scrollTrigger: {
            trigger: '.counter',
            snap: {
                duration: 2,
                delay: 1,
                ease: 'power1.inOut'
            }
        }
    });


    return (
        <div className='flex flex-col gap-1 overflow-hidden w-full'>
            <div className='counter flex gap-2 justify-center font-bold '>
                {/* <CountUp
                        decimal="."
                        // decimals={2}
                        delay={5}
                        duration={3}
                        end={value}
                    /> */}
                <p className='text-[25px]/[36px] 2xl:[30px]/[40px] whitespace-pre-wrap tracking-tighter font-extrabold text-[#FFFFFF]'>
                    <NumberTicker value={value} />
                </p>
                <Title className='text-[25px]/[36px] 2xl:[30px]/[40px] font-extrabold text-[#FFFFFF]'>
                    {description}
                </Title>
            </div>
            <Text className='text-[10px] md:text-xs 2xl:text-sm font-semibold whitespace-nowrap text-center text-[#FFFFFF] uppercase'>
                {name}
            </Text>
        </div>
    )
}

export default Statistics