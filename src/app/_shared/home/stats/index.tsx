"use client";
import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import { stasData } from '@/constants/constants'
import Statistics from './statistics'
import Image from 'next/image'
import component from "@public/stats/Component_5.png"
import person from "@public/stats/Rectangle 1697.png"
import bgImage from "@public/stats/Group1171275296.png"


type Props = {}


function Stats({ }: Props) {

    return (
        <Container className='mt-24'>
            <div
                className='relative rounded-2xl flex flex-col gap-3 text-white py-5 p-6 md:px-8 md:py-6 bg-gradient-to-br to-[#A03A43] from-[#737373]'
            >
                <Title className='font-extrabold text-[24px]/[28px] xl:text-[32px]/[38.73px] text-[#F6F6F6]'>
                    Software Solutions Tailored for Your Business Success...
                </Title>
                <Text className='font-medium text-[16px] md:leading-[27px] text-[#F6F6F6] mb-6'>
                    From the initial spark of an idea to the final product, we turn visions into reality with precision and passion. Experience seamless innovation and unparalleled craftsmanship with our expert team, dedicated to bringing your concepts to life.
                </Text>

                {/* <Image src={bgImage} alt="Footer pattern" fill /> */}
                <div className='grid grid-cols-2 md:grid-cols-6 gap-6 justify-between items-start w-full my-4 px-3 md:px-6'>
                    {stasData.map((data, index) => (
                        <Statistics {...data} key={index + "Stat Data"} />
                    ))}
                </div>
            </div>


            {/* <div className='grid md:grid-cols-3 gap-8'>
                <div style={{
                    boxShadow: "0px 4px 20px 0px #3333330D"
                }}
                    className='relative border-[0.1px] border-[#A03A434D] pb-16 p-6 max-h-44 h-full flex flex-col gap-2 rounded-[15px] bg-[#ffffff] shadow-lg shadow-[#3333330D]'>
                    <Title className='font-bold text-xl'>
                        Over 28 Products
                    </Title>
                    <Text className='font-light'>
                        Discover the innovation behind our 28+ groundbreaking products designed to transform your business.
                    </Text>

                    <Image src={component} alt="" className='absolute right-0 top-0 object-contain w-16 h-16 scale-150' />
                </div>

                <div style={{
                    boxShadow: "0px 4px 20px 0px #3333330D"
                }}
                    className='relative border-[0.1px] border-[#A03A434D] pb-16 p-6 max-h-40 h-full flex flex-col gap-2 rounded-[15px] bg-[#ffffff] shadow-lg shadow-[#3333330D]'>
                    <Title className='font-bold text-lg md:text-2xl 2xl:text-3xl'>
                        Creating Waves of Change: Empowering Impactful Solutions
                    </Title>
                </div>

                <div className='relative rounded-2xl flex flex-col gap-5 text-white p-5 md:p-6 bg-gradient-to-br to-[#A03A43] from-[#737373]'>

                    <Title className='font-bold text-xl 2xl:text-2xl'>
                        “Leading the Charge in Financial Innovation. Your Partner in Banking Excellence. Leading the Charge in Financial Innovation. ”
                    </Title>

                    <div className='flex gap-2 mt-2'>
                        <Image src={person} alt="" />
                        <div className='flex flex-col'>
                            <Title className='font-bold text-base'>
                                Ato Yonas Kebede
                            </Title>
                            <Text className='font-light text-xs'>
                                CEO and Director of Dashen Bank
                            </Text>

                        </div>
                    </div>


                </div>

            </div> */}
            {/* <Image src={bgImage} alt="Footer pattern" fill /> */}

        </Container>
    )
}

export default Stats