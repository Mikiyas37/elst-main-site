"use client";
import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import InnovationTabs from './tabs'
import bgPattern from "@public/pattern/BackgroundPattern 2.png"
import Image from 'next/image'

type Props = {}

function Innovations({ }: Props) {
    return (
        <Container className='mt-24 flex flex-col gap-12'>

            <div className='flex flex-col items-center gap-5'>
                <Title className='text-2xl 2xl:text-3xl font-extrabold opacity-100 text-[#424143] text-center'>
                    Empowering Visionaries, Driving Innovation, <br />
                    and Transforming Futures
                </Title>
                <Text className="font-normal 2xl:text-[16px]/[25px] text-[#333333] opacity-70 text-center max-w-4xl">
                    Pioneering solutions that redefine industry standards and spark change. Creating opportunities and driving growth for a brighter tomorrow.
                </Text>
            </div>

            <div className='bg-[#F2F2F2] rounded-2xl w-full h-full pb-6 relative'>
                <Image src={bgPattern} alt="" fill />

                <InnovationTabs />
            </div>

        </Container>
    )
}

export default Innovations