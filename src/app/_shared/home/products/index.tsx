"use client";
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'

import image1 from "@public/discover/mobile.svg"
import image2 from "@public/discover/e 2.png"
import image3 from "@public/discover/Untitled-3-02 2 (1).png"
import image4 from "@public/discover/Untitled-3-13 1 (1).png"
import image5 from "@public/discover/teletv-02 2 (2).png"
import image6 from "@public/discover/Untitled-3-08 3.png"
import image7 from "@public/discover/Untitled-3-07 1 (2).png"
import image8 from "@public/discover/Untitled-3-09 1.png"
import image9 from "@public/discover/image-removebg-preview (12) 1 (1).png"
import image10 from "@public/discover/Untitled-3-12 1.png"
import image11 from "@public/discover/Untitled-3-04 1 (1).png"
import image12 from "@public/discover/Untitled-3-05 2 (3).png"
import image13 from "@public/discover/Untitled-3-01 3 (1).png"
import image14 from "@public/discover/Untitled-3-10 2 (2).png"
import image16 from "@public/discover/chatbirr-01 2 (1).png"
import image17 from "@public/discover/Untitled-3-11 1.png"
import image18 from "@public/discover/Untitled-3-13 1.png"
import image19 from "@public/discover/Untitled-3-13 1 (2).png"

import CustomButton from '@/components/custom-button'

import { routes } from '@/configs/routes'

type Props = {}

const Products = (props: Props) => {


  return (
    <Container className='mt-24'>
      <div className='flex flex-col gap-2 items-center leading-7'>
        <p className='font-semibold text-[14px]/[25px] px-5 py-1 round text-[#333333] bg-[#0000000D] text-opacity-[50%] rounded-full'>
          Our Products
        </p>
        <Title className="font-bold text-[#333333] text-2xl md:text-[36px]/[45px] text-center mx-auto">
          Discover our revolutionary innovations and finest products
        </Title>
        <Text className='font-medium text-[16px]/[25px] text-[#333333] opacity-[60%] max-w-xl text-center'>
          Explore our groundbreaking innovations and premium products, crafted to set new benchmarks for excellence and quality in the industry.
        </Text>
      </div>

      <div className='mt-12 grid grid-cols-1 lg:grid-cols-8 gap-6 px-6 2xl:px-16 3xl:px-24 mx-auto'>
        <div className='col-span-3 grid grid-cols-3 items-start gap-2 mt-16'>
          <div className='flex flex-col gap-2'>
            <Image src={image2} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image12} alt="" className="h-36  w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image11} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
          </div>

          <div className='flex flex-col gap-2'>
            <Image src={image3} alt="" className="h-36  w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image8} alt="" className="h-24  w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image18} alt="" className="h-36  w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
          </div>

          <div className='flex flex-col gap-2'>
            <Image src={image14} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image12} alt="" className="h-36 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image4} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
          </div>
        </div>



        <div className='col-span-2'>
          <Image src={image1} alt="" className="h-72 md:h-fit w-full" />
        </div>


        <div className='col-span-3 grid grid-cols-3 items-start gap-2 md:mt-16'>
          <div className='flex flex-col gap-2'>
            <Image src={image6} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image17} alt="" className="h-36 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image9} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
          </div>

          <div className='flex flex-col gap-2'>
            <Image src={image13} alt="" className="h-36 w-full rounded-lg object-contain shadow-lg p-3 md:p-5 " />
            <Image src={image5} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image19} alt="" className="h-36 w-full rounded-lg object-contain shadow-lg p-3 md:p-5 " />
          </div>

          <div className='flex flex-col gap-2'>
            <Image src={image10} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5" />
            <Image src={image16} alt="" className="h-36 w-full rounded-lg object-contain shadow-lg p-3 md:p-5 " />
            <Image src={image7} alt="" className="h-24 w-full rounded-lg object-contain shadow-lg p-3 md:p-5 " />
          </div>
        </div>



      </div>

      <div className='w-full flex justify-center mt-6'>
        <CustomButton
          href={routes.products}
          text='Browse All'
        />
      </div>
    </Container>
  )
}

export default Products