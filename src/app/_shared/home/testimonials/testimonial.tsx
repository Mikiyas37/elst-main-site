import React from 'react'
import Image, { StaticImageData } from 'next/image'
import { BiSolidQuoteRight } from "react-icons/bi";

import { Title, Text } from "@/components/ui/text"
import { getTestimonials } from '@/data/get-testimonials';

type Props = {
    title: string,
    description: string,
    fullName: string,
    image: StaticImageData,
    role: string
}

function Testimonial({ title, description, fullName, image, role }: Props) {

    const testimonials = getTestimonials()

    return (
        <div className='grid grid-cols-1 md:grid-cols-3 gap-8 justify-center w-full'>
            <div className='w-full md:col-span-1 relative px-2'>
                <Image src={image} alt={title} className='max-w-lg w-full aspect-video object-cover rounded-xl' />
                <div className='absolute right-0 top-0 flex items-center justify-center rounded-full p-4 bg-primary'>
                    <BiSolidQuoteRight className='text-[#fff]' size={20} />
                </div>
            </div>

            <div className='flex flex-col gap-2 md:col-span-2 px-2'>
                <Title className='font-bold text-[22px]/[24.76px] text-[#333333]'>{title}</Title>
                <Text className='text-xs md:text-[16px]/[24px] leading-4 md:leading-5 font-light max-w-2xl w-full text-justify'>{description}</Text>

                <div className='mt-4'>
                    <Text className='font-extrabold text-[16px]/[20px] text-[#333333]'>{fullName}</Text>
                    <Text className='font-medium text-[14px]/[30px] text-[#333333] '>{role}</Text>
                </div>
            </div>
        </div>
    )
}

export default Testimonial