import React from 'react'
import { Text } from "@/components/ui/text"
import cn from '@/utils/class-names'

type Props = {}

function JobCardLoader({ }: Props) {
    return (
        <div className="flex flex-col animate-pulse flex-wrap  gap-8">
            <div className='flex items-center justify-between p-6'>
                <div className={cn("grid w-8 h-8 place-items-center rounded-lg bg-gray-300")} />

                <button className="mb-4 h-6 w-28 rounded-full bg-gray-300" />
            </div>
            <div className="w-full mt-6">
                <Text
                    className="mb-4 h-6 w-36 rounded-full bg-gray-300"
                >
                    &nbsp;
                </Text>
                <Text
                    className="mb-1 h-2 w-full rounded-full bg-gray-300"
                >
                    &nbsp;
                </Text>
                <Text
                    className="mb-1 h-2 w-full rounded-full bg-gray-300"
                >
                    &nbsp;
                </Text>
                <Text
                    className="mb-1 h-2 w-full rounded-full bg-gray-300"
                >
                    &nbsp;
                </Text>
                <div className='flex justify-end me-8 my-3'>
                    <span
                        className="mb-6 h-5 w-16  rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </span>
                </div>

            </div>
        </div>
    )
}

export default JobCardLoader