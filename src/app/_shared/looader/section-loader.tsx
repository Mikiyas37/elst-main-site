import cn from '@/utils/class-names'
import React from 'react'
import { Text } from "@/components/ui/text"

type Props = {
    className?: string
}

function SectionLoader({ className }: Props) {
    return (
        <div className="flex flex-col animate-pulse flex-wrap  gap-8">
            <div className={cn(className, "w-full min-h-96 h-full rounded-3xl bg-gray-50")} >
                <div className="w-full">
                    <Text
                        className="mb-4 h-3 w-16 rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-2 h-2 w-48 rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>


                    <div className="w-full mt-12">
                        <Text
                            className="mb-4 h-3 w-16 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-2 h-2 w-48 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-72 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                    </div>
                    
                    <div className="w-full mt-12">
                        <Text
                            className="mb-4 h-3 w-16 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-2 h-2 w-48 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-72 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                    </div>

                    <div className="w-full mt-12">
                        <Text
                            className="mb-4 h-3 w-16 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-2 h-2 w-48 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-72 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <Text
                            className="mb-1 h-2 w-full rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SectionLoader