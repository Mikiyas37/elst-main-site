import React from 'react'
import { Text } from "@/components/ui/text"

type Props = {}

function OtherJobsCardLoader({ }: Props) {
    return (
        <div
            className='flex flex-col animate-pulse flex-wrap  gap-8'>
            <div className='flex gap-4'>
                <button className="mb-4 h-6 w-20 rounded-full bg-gray-300" />

                <div className="w-full">
                    <Text
                        className="mb-4 h-8 w-36 rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <Text
                        className="mb-1 h-2 w-full rounded-full bg-gray-300"
                    >
                        &nbsp;
                    </Text>
                    <div className='flex justify-end me-8 mt-8 mb-1'>
                        <span
                            className="mb-6 h-5 w-16  rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </span>
                    </div>

                </div>
            </div>


            <div className='flex justify-end'>
                <button className='w-fit h-fit px-4 py-1 bg-[#A03A43]/10 rounded-full text-[14px] uppercase text-primary font-bold'>
                </button>
            </div>
        </div>
    )
}

export default OtherJobsCardLoader