import React from 'react'
import { Text } from "@/components/ui/text"

type Props = {
    className?: string
}

function Loader({ }: Props) {
    return (
        <div className="flex flex-col animate-pulse flex-wrap gap-8">
            <div className='flex justify-between items-center w-full'>
                <div className='flex gap-3 items-start'>
                    <button className="h-8 w-8 rounded-full bg-gray-300" />

                    <div>
                        <Text
                            className="mb-1 h-5 w-48 rounded-full bg-gray-300"
                        >
                            &nbsp;
                        </Text>
                        <div className='flex flex-col md:flex-row md:items-center md:gap-3'>
                            <Text
                                className="mb-2 h-3 w-24 rounded-full bg-gray-300"
                            >
                                &nbsp;
                            </Text>
                        </div>
                    </div>

                </div>

                <button className="h-3 w-10 rounded-full bg-gray-300" />

            </div>

        </div>
    )
}

export default Loader