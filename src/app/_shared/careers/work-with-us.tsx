import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import WorkWithUsCard from './card/work-with-us'

type Props = {}

const whyWorkWithUs = [
    {
        title: "Opportunities for Growth and Development",
        description: "At our company, we believe in investing in our employees' professional development. From mentorship programs to training opportunities, we provide the resources and support you need to advance your career and achieve your goals."
    },
    {
        title: "Innovative and Collaborative Environment",
        description: "Join a team of passionate professionals who are dedicated to pushing the boundaries of innovation. Our collaborative work environment fosters creativity and encourages new ideas, allowing you to contribute to groundbreaking projects and make a real impact."
    },
    {
        title: "Opportunity to Make an Impact",
        description: "Be part of a team that is making a difference. Whether it's developing life-saving medical devices, creating sustainable solutions for environmental challenges, or revolutionizing industries with disruptive technologies, our work has a tangible impact on the world around us."
    },
    {
        title: "Supportive Leadership",
        description: "Our leadership team is committed to supporting and empowering employees. With open communication channels, regular feedback sessions, and a culture of transparency, you'll have the support and guidance you need to thrive and succeed in your role."
    },
]

function WorkWithUs({ }: Props) {
    return (
        <Container>
            <div className='my-24'>
                <Title className='text-3xl md:text-4xl font-extrabold'>
                    Why work with us?
                </Title>
                <Text className='font-light opacity-90 text-sm'>
                    What make us the ideal choice for your career growth.
                </Text>

                <div className='grid md:grid-cols-3 gap-8 gap-y-10 2xl:gap-12 justify-between mt-12'>
                    {whyWorkWithUs.map((data, index) => (
                        <WorkWithUsCard
                            key={index}
                            {...data} />
                    ))}
                </div>
            </div>


        </Container>
    )
}

export default WorkWithUs