import Container from '@/components/container'
import React from 'react'
import image1 from "@public/careers/Rectangle 1592 (1).png"
import image2 from "@public/careers/Rectangle 1597.png"
import image3 from "@public/careers/Rectangle 1593 (1).png"
import image4 from "@public/careers/Rectangle 1601.png"
import image5 from "@public/careers/Rectangle 1602.png"
import image6 from "@public/careers/Rectangle 1603.png"
import image7 from "@public/careers/Rectangle 1604.png"
import image8 from "@public/careers/Rectangle 1605.png"
import Image from 'next/image'

type Props = {}

function Images({ }: Props) {
    return (
        <Container>
            <div className='grid md:grid-cols-5 gap-5 items-center'>
                <div className='flex justify-center items-center col-span-3 md:col-span-1 w-full'>
                    <Image src={image1} alt={"image"} />
                </div>
                <div className='col-span-3 flex flex-col gap-5'>
                    <div className=' grid grid-cols-3 items-center gap-5'>
                        <Image src={image2} alt={"image"} />
                        <Image src={image3} alt={"image"} />
                        <Image src={image4} alt={"image"} />
                    </div>

                    <div className='grid grid-cols-3 items-center gap-5 md:-mt-12'>
                        <Image src={image6} alt={"image"} />
                        <Image src={image7} alt={"image"} />
                        <Image src={image8} alt={"image"} />
                    </div>
                </div>
                <div className='flex justify-center items-center col-span-3 md:col-span-1 w-full'>
                    <Image src={image5} alt={"image"} />
                </div>
            </div>

        </Container>
    )
}

export default Images