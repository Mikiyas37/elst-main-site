import React from 'react'
import { Title, Text } from "@/components/ui/text"


type Props = {
    title: string,
    description: string
}

function WorkWithUsCard({ title, description }: Props) {
    return (
        <div className='flex flex-col gap-2'>
            <Title className='ps-2 font-extrabold text-xl border-l-[3px] border-l-primary'>
                {title}
            </Title>

            <Text className='text-sm font-light opacity-80'>
                {description}
            </Text>
        </div>
    )
}

export default WorkWithUsCard