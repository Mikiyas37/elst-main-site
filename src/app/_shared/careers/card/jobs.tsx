import React from 'react'
import Image from 'next/image'
import { Title, Text } from "@/components/ui/text"
import image from "@public/careers/Group 1171275054.svg"
import Link from 'next/link'
import { routes } from '@/configs/routes'

type Props = {
    data: any
}

function Jobs({ data }: Props) {
    return (
        <Link href={routes.careers + `/${data?._id}`}
            className='cursor-pointer flex flex-col gap-3 p-5 rounded-3xl shadow-xl shadow-[#0000000D] border border-[#A03A4333]'>
            <div className='flex justify-between items-center'>
                <Image src={image} alt={""} className='w-12 h-12 overflow-hidden scale-150' />
                <button className='w-fit h-fit px-6 py-1.5 bg-[#A03A43]/10 rounded-full text-xs uppercase text-primary font-bold'>
                    {data?.type}
                </button>
            </div>

            <div className='flex flex-col gap-3'>
                <Title className='font-extrabold text-xl'>
                    {data?.title}
                </Title>

                <Text className='line-clamp-3 font-light opacity-70 text-sm'>
                    {data?.description && data?.description[0]?.children[0]?.text}
                </Text>
            </div>

            <div>
                <p className='text-end underline hover:no-underline text-base font-semibold text-primary'>
                    View & Apply
                </p>
            </div>
        </Link>
    )
}

export default Jobs