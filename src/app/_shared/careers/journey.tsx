"use client"
import React, { useEffect, useState } from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Jobs from './card/jobs'
import { getJobs } from '@/data/get-jobs'
import JobCardLoader from '../looader/job-card-loader'

type Props = {}


function Journey({ }: Props) {
  const [jobs, setJobs] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    setLoading(true)
    getJobs()
      .then(data => {
        setJobs(data)
        setLoading(false)
      })
      .catch(error => {
        setError(error)
      });
  }, [])

  return (
    <Container>
      <div className='my-24'>
        <Title className='text-center font-extrabold text-3xl md:text-4xl'>
          Join Our Journey: Be Part of Something Extraordinary
        </Title>

        <Text className='text-center font-light opacity-90 text-sm'>
          Embark on a rewarding career journey with us and contribute to something truly exceptional.
        </Text>


        {/* {Array(8).fill(0).map((_, index) => ( */}

        {loading ?
          <div className='mt-12 grid md:grid-cols-3 gap-6 2xl:gap-10'>

            {Array(5).fill(0).map((_, index) => (
              <JobCardLoader key={index} />
            ))}
          </div>
          :
          <div className='mt-12 grid md:grid-cols-3 gap-6 2xl:gap-10'>
            {jobs.length ? jobs?.map((data: any, index: number) => (
              <Jobs
                data={data}
                key={index}
              />
            ))
              : null
            }
          </div>
        }
      </div>
    </Container>
  )
}

export default Journey