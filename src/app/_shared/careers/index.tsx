import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Container from '@/components/container'

type Props = {}

function Hero({ }: Props) {
    return (
        <Container>
            <div className='my-24 max-w-4xl mx-auto flex flex-col gap-1'>
                <Title className='text-3xl xl:text-4xl font-extrabold text-center'>
                    Careers: Where Passion and Purpose Align
                </Title>
                <Text className='text-base font-light opacity-90 text-center mx-auto max-w-3xl'>
                    Explore exciting career opportunities with us
                    and join a dynamic team dedicated to innovation and excellence.
                    Grow your career in a supportive and collaborative environment.
                </Text>
            </div>
        </Container>
    )
}

export default Hero