import { routes } from '@/configs/routes'
import Link from 'next/link'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import image from "@public/careers/Group 1171275054.svg"


type Props = {
    data: any
}

function OtherJobs({ data }: Props) {
    return (
        <Link href={routes.careers + `/${data?._id}`}
            className='cursor-pointer flex flex-col gap-3 p-3 rounded-xl shadow-xl shadow-[#0000000D] border border-[#A03A4333]'>
            <div className='flex gap-4 justify-start items-start'>
                <Image src={image} alt={""} className='w-28 h-28 overflow-hidden scale-[200%]' />

                <div className='flex flex-col gap-1'>
                    <Title className='font-extrabold text-lg'>
                        {data?.title}
                    </Title>

                    <Text className='text-xs font-light opacity-70 line-clamp-2'>
                        {data?.description && data?.description[0]?.children[0]?.text}
                    </Text>
                </div>
            </div>


            <div className='flex justify-end'>
                <button className='w-fit h-fit px-4 py-1 bg-[#A03A43]/10 rounded-full text-xs uppercase text-primary font-bold'>
                    {data?.type}
                </button>
            </div>
        </Link>
    )
}

export default OtherJobs