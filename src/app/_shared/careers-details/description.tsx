"use client"
import React, { useEffect, useState } from 'react'
import { Title, Text } from "@/components/ui/text"
import Container from '@/components/container'
import { Button } from "@/components/ui/button"
import OtherJobs from './cards/other-jobs'
import { getJobDetails, getJobs } from '@/data/get-jobs'
import { usePathname } from 'next/navigation'
import SectionLoader from '../looader/section-loader'
import SimpleBar from "@/components/ui/simplebar"
import OtherJobsCardLoader from '../looader/other-jobs-card-loader'

type Props = {}

function Description({ }: Props) {
    const pathName = usePathname()
    const slug = pathName.split("/").pop()

    const [jobDetails, setJobDetails] = useState<any>([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)

    useEffect(() => {
        setLoading(true)
        getJobDetails(slug!)
            .then(data => {
                setJobDetails(data)
                setLoading(false)
            })
            .catch(error => {
                setError(error)
            });
    }, [slug])

    const [jobs, setJobs] = useState([])

    useEffect(() => {
        setLoading(true)
        getJobs()
            .then(data => {
                setJobs(data)
                setLoading(false)
            })
            .catch(error => {
                setError(error)
            });
    }, [])

    return (
        <Container>
            <div className='mb-24 mx-auto max-w-7xl grid md:grid-cols-3 gap-6 2xl:gap-12'>
                {loading ?
                    <div className='w-full md:col-span-2'>
                        <SectionLoader className='w-full h-full' />
                    </div>
                    :
                    <div className='md:col-span-2 w-full'>
                        {jobDetails[0]?.details &&
                            <div>
                                <Title className='text-2xl font-extrabold'>
                                    Description
                                </Title>
                                <Text className='text-base font-light opacity-90 mt-4'>
                                    {jobDetails[0]?.details && jobDetails[0]?.details[0]?.children[0]?.text}
                                </Text>

                                {/* Tasks */}
                                {jobDetails[0]?.tasks &&
                                    <div className='mt-10'>
                                        <Title className='text-xl font-extrabold'>
                                            Tasks
                                        </Title>

                                        <div className='mt-3 flex flex-col gap-2'>
                                            {jobDetails[0]?.tasks && jobDetails[0]?.tasks.length &&
                                                jobDetails[0]?.tasks?.map((task: string, index: number) => (
                                                    <div key={index}
                                                        className='ms-2 md:ms-0 flex items-center gap-3 md:gap-2'>
                                                        <div className='w-2 h-2 rounded-full bg-black' />
                                                        <Text className='text-sm md:text-base font-light opacity-70'>
                                                            {task}
                                                        </Text>
                                                    </div>
                                                ))}

                                        </div>
                                    </div>
                                }

                                {/* Requirements */}
                                {jobDetails[0]?.requirements &&
                                    <div className='mt-10'>
                                        <Title className='text-xl font-extrabold'>
                                            Requirements
                                        </Title>

                                        <div className='mt-3 flex flex-col gap-2'>
                                            {jobDetails[0]?.requirements && jobDetails[0]?.requirements.length &&
                                                jobDetails[0]?.requirements?.map((requirements: string, index: number) => (
                                                    <div key={index}
                                                        className='ms-2 md:ms-0 flex items-center gap-3 md:gap-2'>
                                                        <div className='w-2 h-2 rounded-full bg-black' />
                                                        <Text className='text-sm md:text-base font-light opacity-70'>
                                                            {requirements}
                                                        </Text>
                                                    </div>
                                                ))}


                                        </div>
                                    </div>
                                }


                                {/* Contact Info */}
                                {jobDetails[0]?.contact &&
                                    <div className='mt-10 border-b pb-10'>
                                        <Title className='text-xl font-extrabold'>
                                            Contact Info
                                        </Title>

                                        <Text className='mt-3 text-base font-normal'>
                                            {jobDetails[0]?.contact && jobDetails[0]?.contact[0]?.children?.map((text: any, index: number) => (
                                                <span key={index} className='text-sm md:text-base font-bold opacity-70'>
                                                    {text?.text}
                                                </span>
                                            ))}

                                        </Text>
                                    </div>
                                }


                                {/* Apply */}
                                <div className='mt-12'>
                                    <Button className="px-8 2xl:px-12 rounded-full text-white bg-primary text-sm font-bold">
                                        Apply Now
                                    </Button>
                                </div>
                            </div>
                        }
                    </div>
                }

                {/*  */}
                <div className='flex flex-col gap-6'>
                    <Title className='text-xl font-extrabold'>
                        Other Jobs
                    </Title>

                    <SimpleBar className='max-h-[900px] h-full'>
                        <div className='flex flex-col gap-5'>
                            {loading ?
                                <div className='flex flex-col gap-5'>
                                    <OtherJobsCardLoader />
                                    <OtherJobsCardLoader />
                                </div>
                                : jobs.length ? jobs?.map((data: any, index: number) => (
                                    <OtherJobs key={index}
                                        data={data} />
                                ))
                                    : null}
                        </div>
                    </SimpleBar>
                </div>
            </div>
        </Container>
    )
}

export default Description