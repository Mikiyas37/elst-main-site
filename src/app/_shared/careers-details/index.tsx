"use client"
import Container from '@/components/container'
import Image from 'next/image'
import React, { useEffect, useState } from 'react'
import image from "@public/careers/Group 1171275054.svg"
import { Title, Text } from "@/components/ui/text"
import { usePathname } from 'next/navigation'
import { getJobDetails } from '@/data/get-jobs'
import { formatISODate } from '@/utils/formate-iso-date'
import Loader from '../looader/loader'

type Props = {}

function Hero({ }: Props) {

    const pathName = usePathname()
    const slug = pathName.split("/").pop()

    const [jobDetails, setJobDetails] = useState<any>([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)

    useEffect(() => {
        setLoading(true)
        getJobDetails(slug!)
            .then(data => {
                setJobDetails(data)
                setLoading(false)
            })
            .catch(error => {
                setError(error)
            });
    }, [slug])

    return (
        <Container>
            <div className='my-16 mx-auto max-w-7xl border-b pb-8'>
                {loading ?
                    <Loader />
                    :
                    <div className='flex justify-between items-center'>
                        <div className='flex gap-3 items-start'>
                            <Image src={image} alt={""} className='w-12 h-12 overflow-hidden scale-150' />

                            <div>
                                <Title className='font-extrabold text-xl md:text-2xl'>
                                    {jobDetails[0]?.title}
                                </Title>
                                <div className='flex flex-col md:flex-row md:items-center md:gap-3'>
                                    <Text className='text-base font-bold opacity-70'>
                                        Posted on:
                                    </Text>
                                    <Text className='text-sm font-bold opacity-50'>
                                        {jobDetails[0]?._updatedAt && formatISODate(jobDetails[0]?._updatedAt)}
                                    </Text>
                                </div>
                            </div>

                        </div>

                        <button className='w-fit h-fit px-6 py-1.5 bg-[#A03A43]/10 rounded-full text-xs md:text-sm uppercase text-primary font-bold'>
                            {jobDetails[0]?.type}
                        </button>
                    </div>
                }
            </div>
        </Container>
    )
}

export default Hero