"use client"
import React from 'react'
import Container from '@/components/container'

import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import map from "@public/contact-us/Rectangle1725.png"
import ContacUsForm from './contac-us-form'
import Simplebar from "@/components/ui/simplebar"

type Props = {}

function ContactUs({ }: Props) {
    return (
        <Container>
            <div id='contact' className='mt-24 pb-8 flex flex-col gap-8 p-1.5 w-full '>

                <div>
                    <Title className='text-[30px]/[40px] font-bold '>
                        Get In Touch: Let&apos;s build something great together.
                    </Title>
                    <Text className='text-[16px]/[25px] text-[#333333] opacity-[60%] mt-1'>
                        For general enquires you can touch with our front desk supporting team
                    </Text>
                </div>

                <div className='grid grid-cols-1 gap-5 md:grid-cols-5 w-full'>
                    <ContacUsForm className={"md:col-span-2 w-full"} />


                    {/* <Image src={map} alt="" className='w-full h-max object-contain col-span-3' /> */}
                    <Simplebar className='w-full md:col-span-3'>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.5336509432773!2d38.7924566735283!3d9.014986189211193!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x164b852b6c634421%3A0x245e403a62391711!2sEagleLion%20System%20Technology!5e0!3m2!1sen!2set!4v1719324797133!5m2!1sen!2set"
                            width="700" height="450" loading="lazy" ></iframe>
                    </Simplebar>

                </div>
            </div>
        </Container>
    )
}

export default ContactUs