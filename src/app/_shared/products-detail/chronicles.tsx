"use client"
import Container from '@/components/container'
import React from 'react'
import { Title } from "@/components/ui/text"
import Image from 'next/image'

import image1 from "@public/product-details/gallery/Rectangle 1592.png"
import image2 from "@public/product-details/gallery/Rectangle 1593.png"
import image3 from "@public/product-details/gallery/Rectangle 1594.png"
import image4 from "@public/product-details/gallery/Rectangle 1595.png"
import image5 from "@public/product-details/gallery/Rectangle 1596.png"
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'

type Props = {
    data: any
}

function Chronicles({ data }: Props) {

    const image1 = useNextSanityImage(configuredSanityClient, data?.images[0]?.image1);
    const image2 = useNextSanityImage(configuredSanityClient, data?.images[0]?.image2);
    const image3 = useNextSanityImage(configuredSanityClient, data?.images[0]?.image3);
    const image4 = useNextSanityImage(configuredSanityClient, data?.images[0]?.image4);

    const image5 = useNextSanityImage(configuredSanityClient, data?.images[1]?.image5);
    const image6 = useNextSanityImage(configuredSanityClient, data?.images[1]?.image6);
    const image7 = useNextSanityImage(configuredSanityClient, data?.images[1]?.image7);
    const image8 = useNextSanityImage(configuredSanityClient, data?.images[1]?.image8);


    return (
        <Container className='mt-24 '>
            <Title className='font-bold text-2xl xl:text-3xl max-w-4xl w-full'>
                {data?.title}
            </Title>


            <div className='grid grid-cols-3 gap-3 mt-8'>
                {image1 &&
                    <div className='flex flex-col justify-between gap-3 h-[30vh] md:h-[75vh]'>
                        <Image src={image1} alt={""} className='w-full object-cover h-full' />
                    </div>}
                {image2 && image3 &&
                    <div className='flex flex-col h-[30vh] md:h-[75vh] gap-3'>
                        {image2 &&
                            <Image src={image2} alt={""} className='w-full object-cover h-[49%]' />}
                        {image3 &&
                            <Image src={image3} alt={""} className='w-full object-cover h-[49%]' />}
                    </div>}
                {image4 &&
                    <div className='flex flex-col justify-between gap-3 h-[30vh] md:h-[75vh]'>
                        <Image src={image4} alt={""} className='w-full object-cover h-full' />
                    </div>}
            </div>

            <div className='grid grid-cols-3 gap-3 mt-8'>
                {image5 &&
                    <div className='flex flex-col justify-between gap-3 h-[30vh] md:h-[75vh]'>
                        <Image src={image5} alt={""} className='w-full object-cover h-full' />
                    </div>}
                {image6 && image7 &&
                    <div className='flex flex-col h-[30vh] md:h-[75vh] gap-3'>

                        {image6 &&
                            <Image src={image6} alt={""} className='w-full object-cover h-[49%]' />}
                        {image7 &&
                            <Image src={image7} alt={""} className='w-full object-cover h-[49%]' />}

                    </div>}

                {image8 &&
                    <div className='flex flex-col justify-between gap-3 h-[30vh] md:h-[75vh]'>
                        <Image src={image8} alt={""} className='w-full object-cover h-full' />
                    </div>}


            </div>

        </Container>
    )
}

export default Chronicles