"use client"
import React from 'react'
import Container from '@/components/container'
import Image from 'next/image'
import frame from "@public/mobile-frame.png"
import { Title, Text } from "@/components/ui/text"
import { Tab, TabList, TabPanel, TabPanels, Tabs } from 'rizzui'
import SimpleBar from "@/components/ui/simplebar"
import cn from '@/utils/class-names'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'
import { SanityImage } from '../products/product-tabs/sanity-image'


type Props = {
    data: any
}

function TopFeatures({ data }: Props) {

    const topFeatures = [
        {
            title: "Loan Accessibility",
            description: "Easily apply for loans directly through the app, streamlining the borrowing process and eliminating traditional paperwork hassles.",
            image: frame
        },
        {
            title: "Seamless Transactions",
            description: "Easily apply for loans directly through the app, streamlining the borrowing process and eliminating traditional paperwork hassles.",
            image: frame
        },
        {
            title: "Merchant Marketplace",
            description: "Easily apply for loans directly through the app, streamlining the borrowing process and eliminating traditional paperwork hassles.",
            image: frame
        },
    ]

    return (
        <Container className='mt-24 h-fit p-8'>
            <div className='flex flex-col gap-3 items-center mx-auto max-w-6xl'>
                <Title className='text-2xl xl:text-3xl font-bold text-center'>
                    {data?.title}
                </Title>
                <Text className='text-base 2xl:text-xl font-light text-center max-w-4xl w-full '>
                    {data?.subtitle}
                </Text>
            </div>

            {/* <div className='grid md:grid-cols-5 items-center w-full h-full py-6'>
                <div className='col-span-2 px-2 md:px-6 py-3 h-full'>
                    <div className='flex flex-col gap-3 h-full w-full'>
                        {topFeatures.map((feature, index) => (
                            <div key={index}
                                className='flex flex-col gap-1 justify-center p-4 md:p-6 rounded-3xl w-full h-full border-[1px] border-[#1A6B482E]'>

                                <p className='w-10 h-10 text-white rounded-full flex items-center justify-center bg-[#2F654C]'>
                                    0{index + 1}
                                </p>

                                <Title className='font-bold text-lg'>
                                    {feature.title}
                                </Title>
                                <Text className='font-light text-base'>
                                    {feature.description}
                                </Text>
                            </div>
                        ))}
                    </div>
                </div>

                <div className='col-span-3 row-auto bg-[#2F654C] rounded-2xl md:h-[500px] w-full relative flex items-center justify-between'>
                    <Image src={frame} alt="" fill />
                </div>

            </div> */}

            <Tabs className={cn("flex flex-col md:flex-row mt-6 gap-6 md:px-6 w-full")}>
                <TabList className={"md:max-w-[400px] w-full"}>
                    <SimpleBar>
                        <div className={cn("flex flex-col gap-4 md:gap-8")}>
                            {data.features.length && data?.features?.map((data: any, index: number) => (

                                <Tab
                                    key={index}
                                    className={({ selected }: { selected: boolean }) =>
                                        cn(
                                            "outline-none p-3 md:p-7 h-full rounded-xl bg-white transition-all duration-100",
                                            selected ? "bg-primary text-white outline-none" : "border shadow-sm text-black"
                                        )
                                    }
                                >
                                    <div className='flex flex-col w-full'>

                                        <p className='w-10 h-10 font-extrabold text-primary rounded-full flex items-center justify-center bg-[#fff] border border-[#8C65331A] shadow-sm'>
                                            0{index + 1}
                                        </p>

                                        <Title className='text-start font-extrabold text-xl'>
                                            {data.featureName}
                                        </Title>
                                        <Text className='font-light text-xs text-start'>
                                            {data.featureDescription[0]?.children[0]?.text}
                                        </Text>
                                    </div>

                                </Tab>
                            )
                            )}
                        </div>
                    </SimpleBar>
                </TabList>

                <TabPanels className="flex-grow flex-1 w-full h-full">
                    {data.features.length && data?.features?.map((data: any, index: number) => (

                        <TabPanel key={index} className="w-full flex items-center justify-center rounded-xl h-full" >
                            {/* <ProductDetails data={data} /> */}
                            <div className='col-span-3 row-auto rounded-2xl md:h-[577px] w-full relative flex items-center justify-center'>

                                <SanityImage imageUrl={data?.image} className={"w-full h-full object-contain"} />

                            </div>
                        </TabPanel>
                    )
                    )}
                </TabPanels>
            </Tabs>
        </Container >
    )
}

export default TopFeatures