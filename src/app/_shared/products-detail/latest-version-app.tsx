"use client"
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import smartphone from "@public/products/smartphone.png"
import playstore from "@public/icon/palystore.svg"
import appstore from "@public/icon/appstore.svg"
import { AnyARecord } from 'dns'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'
import Link from 'next/link'

type Props = {
    data: any
}

function OurApp({ data }: Props) {

    const imageProps = useNextSanityImage(configuredSanityClient, data?.image);


    return (
        <div className='bg-[#F2F2F2] items-center justify-center relative py-12 rounded-3xl max-w-8xl mx-auto w-full px-6'>

            <div className='grid md:grid-cols-2 gap-4 md:h-[60vh] w-full md:px-8'>
                <div className=' p-2 flex flex-col gap-3 justify-center'>
                    <Title className='font-bold text-3xl leading-8 2xl:text-4xl w-full'>
                        {data?.title}
                    </Title>
                    <Text className='font-normal w-full max-w-2xl'>
                        {data?.description[0]?.children[0]?.text}
                    </Text>

                    <div className='flex gap-3 xl:gap-5 mt-6'>
                        <Link href={data?.playStore}
                            className='flex gap-3 items-center bg-[#333333] rounded-full py-1.5 md:py-2 text-white px-5 md:px-6 xl:px-10'>
                            <Image src={playstore} alt="playstore" />
                            Google Play
                        </Link>
                        <Link href={data?.appStore}
                            className='flex gap-3 items-center bg-[#333333] rounded-full py-1.5 md:py-2 text-white px-5 md:px-6 xl:px-10'>
                            <Image src={appstore} alt="appstore" />
                            App Store
                        </Link>
                    </div>

                </div>

                <div className=' relative'>
                    {imageProps &&
                        <Image src={imageProps} alt='bgpattern' fill />}
                </div>
            </div>

        </div>
    )
}

export default OurApp