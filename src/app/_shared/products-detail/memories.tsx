"use client"
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import screen from "@public/product-details/Frame 5885.png"
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'
import ReactPlayer from "react-player/youtube"
import { FaSpinner } from 'react-icons/fa'


type Props = {
    data: any
}

function Memories({ data }: Props) {

    // Video
    // const videoProps = useNextSanityImage(configuredSanityClient, data?.video);


    return (
        <Container className={"mt-24"}>
            <div className='flex flex-col gap-1'>
                <Title className='text-center text-2xl xl:text-3xl w-full font-extrabold'>
                    {data?.title}
                </Title>
                <Text className='font-medium text-center text-sm max-w-4xl mx-auto'>
                    {data?.subtitle}
                </Text>
            </div>
            <div className='w-full h-[40vh] md:h-[550px] relative mt-6 rounded-3xl'>
                <div className="video-parent-div  h-full w-full rounded-3xl">
                    {data?.video ? <>
                        <ReactPlayer
                            width={"100%"}
                            controls={true}
                            className={"rounded-3xl h-full aspect-video"}
                            url={data?.video}
                            light={<Image src={screen} alt="" className="w-full h-full" />}
                        />
                    </> : <>
                        <div className="flex items-center justify-center h-full w-full">
                            <FaSpinner size={20} className={"animate-spin"} />
                        </div>
                    </>}
                </div>
                {/* // Video
                    // <Image src={imageProps} alt="" fill /> */}
            </div>
        </Container>
    )
}

export default Memories