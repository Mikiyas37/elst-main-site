"use client"
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import smartphone from "@public/products/smartphone.png"
import playstore from "@public/icon/palystore.svg"
import appstore from "@public/icon/appstore.svg"
import Link from 'next/link'
import { SanityImage } from '../products/product-tabs/sanity-image'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'

type Props = {
    data: any
}

function ProductDetailsHero({ data }: Props) {

    const imageProps = useNextSanityImage(configuredSanityClient, data?.aboutSection?.image);

    return (
        <Container className='mx-auto w-full'>
            <div className='bg-[#F2F2F2] flex flex-col gap-8 items-center justify-center relative my-12 rounded-3xl max-w-8xl mx-auto w-full p-3 md:px-6'>

                <div className='flex flex-col items-center gap-3 text-center mx-auto max-w-7xl w-full my-6'>
                    <button className='px-5 py-1 rounded-xl bg-[#1A6B4814] text-[#2F654C] w-fit'>
                        {data?.category}
                    </button>
                    <div className='leading-3'>
                        <Title className='text-3xl 2xl:text-4xl font-extrabold my-2 md:my-1'>
                            {data?.title}
                        </Title>
                        <Text className='text-2xl 2xl:text-3xl font-bold'>
                            {data?.subtitle}
                        </Text>
                    </div>

                    <Text className='text-base 2xl:text-xl font-medium opacity-70 max-w-3xl w-full'>
                        {data?.description[0]?.children[0]?.text}
                    </Text>

                    {/* PlayStore and AppStore */}
                    <div className='flex items-center gap-3 xl:gap-5 w-full justify-center mt-6'>
                        <Link href={data?.playStore}
                            className='flex gap-3 items-center bg-[#333333] rounded-full py-2 text-white px-6 xl:px-10'>
                            <Image src={playstore} alt="playstore" />
                            Google Play
                        </Link>
                        <Link href={data?.appStore}
                            className='flex gap-3 items-center bg-[#333333] rounded-full py-2 text-white px-6 xl:px-10'>
                            <Image src={appstore} alt="appstore" />
                            App Store
                        </Link>
                    </div>
                </div>

                <div className='grid md:grid-cols-2 gap-4 md:h-[60vh] w-full md:px-8'>
                    <div className='p-2 flex flex-col gap-3 mt-12'>
                        <Title className='font-bold text-3xl leading-8 2xl:text-4xl w-full max-w-md'>
                            {data?.aboutSection?.title}
                        </Title>
                        <Text className='font-light w-full max-w-md'>
                            {data?.aboutSection?.description[0]?.children[0]?.text}
                        </Text>
                    </div>

                    <div className='relative'>
                        {imageProps &&
                            <Image src={imageProps} alt='smartphone' fill />}
                    </div>
                </div>

            </div>
        </Container>
    )
}

export default ProductDetailsHero