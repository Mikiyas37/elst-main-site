"use client"
import React from 'react'
import Container from '@/components/container'
import Image from 'next/image'
import { Title, Text } from "@/components/ui/text"
import bgPattern from "@public/pattern/BackgroundPattern 1.png"
import images from "@public/products/smartphone.png"
import { productStatData } from '@/constants/constants'
import ProductStat from '../products/product-details/product-stat'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'

type Props = {
    data: any
}

function Embark({ data }: Props) {

    const imageProps = useNextSanityImage(configuredSanityClient, data?.image);

    return (
        <div className='mt-24 bg-[#F2F2F2] w-full items-center justify-center relative py-12'>
            <Image src={bgPattern} alt='bgpattern' fill />

            <Container className='grid md:grid-cols-5 gap-4 h-full md:h-[60vh] w-full'>
                <div className='col-span-3 p-2 flex flex-col justify-around gap-3'>
                    <Title className='font-extrabold text-2xl xl:text-3xl md:leading-8 w-full max-w-xl'>
                        {data?.title}
                    </Title>
                    <Text className='font-medium md:text-base w-full max-w-3xl'>
                        {data?.description[0]?.children[0]?.text}
                    </Text>

                    <div className='mt-8'>
                        <div className='grid grid-cols-2 md:grid-cols-4 md:divide-x-2 divide-black gap-6 justify-between items-center max-w-3xl'>
                            {data?.stats?.length && data?.stats?.map((data: any, index: number) => (
                                <ProductStat data={data} key={index + "Stat Data"} />
                            ))}
                        </div>
                    </div>
                </div>

                <div className='col-span-2 relative'>
                    {imageProps &&
                        <Image src={imageProps!} alt='bgpattern' fill />}
                </div>

            </Container>
        </div>
    )
}

export default Embark