"use client"
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import computer from "@public/product-details/LapMOck@2x 1.png"
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'

type Props = {
    data: any
}

function FinancialManagment({ data }: Props) {


    const imageProps = useNextSanityImage(configuredSanityClient, data?.image);

    return (
        <Container >
            <div className='mt-24 flex flex-col gap-8 justify-between'>
                <div className='flex flex-col gap-2'>
                    <Title className='text-center text-2xl max-w-4xl mx-auto xl:text-3xl w-full font-extrabold'>
                        {data?.title}
                    </Title>
                    <Text className='font-medium text-center text-base'>
                        {data?.subtitle}
                    </Text>
                </div>
                <div className='w-full h-[40vh] md:h-[100vh] relative'>
                    {imageProps &&
                        <Image src={imageProps} alt="" fill className='object-contain'/>}
                </div>
            </div>
        </Container>
    )
}

export default FinancialManagment