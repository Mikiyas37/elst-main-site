"use client"
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import boaColor from "@public/companies/boa-color.svg";
import boaBlack from "@public/companies/boa-black.svg";
import coopColor from "@public/companies/coop-color.svg";
import coopBlack from "@public/companies/coop-black.svg";
import hibretColor from "@public/companies/hibret-color.svg";
import hibretBlack from "@public/companies/hibret-black.svg";
import teleColor from "@public/companies/tele-color.svg";
import teleBlack from "@public/companies/tele-black.svg";
import telebirrColor from "@public/companies/telebirr-color.svg";
import telebirrBlack from "@public/companies/telebirr-black.svg";
import awashColor from "@public/companies/awash-color.svg";
import awashBlack from "@public/companies/awash-black.svg";

import CompanyLogo from '../home/company/company-logo';

type Props = {
    data: any
}

const companiesLogo = [
    {
        name: "BOA",
        image1: boaBlack,
        image2: boaColor
    },
    {
        name: "COOP",
        image1: coopBlack,
        image2: coopColor
    },
    {
        name: "Hibret",
        image1: hibretBlack,
        image2: hibretColor
    },
    {
        name: "Telecome",
        image1: teleBlack,
        image2: teleColor
    },
    {
        name: "Telebirr",
        image1: telebirrBlack,
        image2: telebirrColor
    },
    {
        name: "Awash",
        image1: awashBlack,
        image2: awashColor
    },
]

function PartnerShip({ data }: Props) {

    return (
        <Container className='my-24'>
            <Title className='font-bold text-center text-2xl'>
                In Partnership with
            </Title>

            {/* Company Logo */}
            <div className='flex flex-wrap gap-6 md:gap-12 my-8 max-w-5xl mx-auto w-full items-center justify-center'>
                {data?.length && data?.map((logo: any, index: number) => (
                    <CompanyLogo key={index} logo={logo} />
                ))}
            </div>

        </Container>
    )
}

export default PartnerShip