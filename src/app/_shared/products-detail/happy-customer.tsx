"use client"
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'
import startQuote from "@public/icon/Vector (19).svg"
import endQuote from "@public/icon/Vector (16).svg"
import person from "@public/icon/Ellipse 820.png"
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'


type Props = {
    data: any
}

function HappyCustomer({ data }: Props) {

    const imageProps = useNextSanityImage(configuredSanityClient, data?.happyCustomerImage);

    return (
        <Container className={"mt-24 mx-auto flex flex-col gap-5"}>
            <Title className='text-center text-2xl xl:text-3xl font-extrabold'>
                One of our  Happy Customers
            </Title>
            <div className='max-w-6xl mx-auto mt-3 flex gap-3 md:gap-6'>
                <Image src={startQuote} alt={"start Quote"} />
                <div className='max-w-5xl mx-auto md:px-5'>
                    <Text className='text-center font-medium opacity-70'>
                        {data?.description[0]?.children[0]?.text}
                    </Text>
                </div>
                <Image src={endQuote} alt={"start Quote"} />
            </div>

            <div className='flex flex-col items-center justify-center'>
                {imageProps &&
                    < Image src={imageProps} alt={"person"} className='h-24 w-24 rounded-full' />}
                <Title className='font-bold text-lg'>
                    {data?.customerName}
                </Title>
                <Text className='font-light text-sm'>
                    {data?.jobTitle}
                </Text>
            </div>
        </Container>
    )
}

export default HappyCustomer