import React from 'react'
import Container from '@/components/container'
import { Text, Title } from "@/components/ui/text"
import playstore from "@public/icon/palystore.svg"
import appstore from "@public/icon/appstore.svg"
import Image from 'next/image'


type Props = {}

function ProductDetailsPage({ }: Props) {
    return (
        <Container className={"flex items-center justify-center min-h-[70vh] h-full"}>
            <div className='flex flex-col items-center gap-3 text-center mx-auto max-w-7xl w-full mt-12 mb-16'>
                <button className='px-3 rounded-xl bg-[#1A6B4814] text-[#2F654C] w-fit'>
                    Payment Solution
                </button>
                <div className='leading-3'>
                    <Title className='text-3xl 2xl:text-4xl font-bold my-2 md:my-1'>Dubeale App</Title>
                    <Text className='text-2xl 2xl:text-3xl font-semibold'>
                        Your Financial Companion: Dubeale - Loans Made Simple, Shopping Made Easy
                    </Text>
                </div>

                <Text className='text-base 2xl:text-xl font-light max-w-4xl'>
                    With Dubeale, users can easily apply for loans directly through the app,
                    streamlining the borrowing process and eliminating the hassle of traditional paperwork.
                    Additionally, Dubeale&apos;s integrated marketplace feature allows users to explore
                    and purchase products from a wide range of registered merchants, all within the same
                    intuitive interface.
                </Text>

                {/* PlayStore and AppStore */}
                <div className='flex items-center gap-3 xl:gap-5 w-full justify-center mt-6'>
                    <button className='flex gap-3 items-center bg-[#333333] rounded-full py-2 text-white px-6 xl:px-10'>
                        <Image src={playstore} alt="playstore" />
                        Google Play
                    </button>
                    <button className='flex gap-3 items-center bg-[#333333] rounded-full py-2 text-white px-6 xl:px-10'>
                        <Image src={appstore} alt="appstore" />
                        App Store
                    </button>
                </div>
            </div>
        </Container>
    )
}

export default ProductDetailsPage