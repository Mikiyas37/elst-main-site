"use client"
import Container from '@/components/container'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Image from 'next/image'

import image1 from "@public/product-details/iPhone 14 Pro Max (3).png"
import image2 from "@public/product-details/iPhone 14 Pro Max (4).png"
import image3 from "@public/product-details/iPhone 14 Pro Max (5).png"
import image4 from "@public/product-details/iPhone 14 Pro Max (6).png"
import image5 from "@public/product-details/iPhone 14 Pro Max (7).png"
import image6 from "@public/product-details/iPhone 14 Pro Max (8).png"
import SimpleBar from '@/components/ui/simplebar'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../configuredSanityClient'
import { SanityImage } from '../products/product-tabs/sanity-image'


type Props = {
    data: any
}

const howItWorks = [
    {
        title: "Initiating Payment by ID",
        image: image1
    },
    {
        title: "Enter Merchant Information",
        image: image2
    },
    {
        title: "Add Payment Detail",
        image: image3
    },
    {
        title: "Purchase Summary",
        image: image4
    },
    {
        title: "Confirm PIN",
        image: image5
    },
    {
        title: "Payment Success",
        image: image6
    },

]

function HowItWorks({ data }: Props) {

    return (
        <Container className='mt-24'>
            <div className='flex flex-col gap-1'>
                <Title className='text-3xl font-extrabold'>
                    {data?.title}
                </Title>
                <Text className='text-base font-medium'>
                    {data?.subtitle}
                </Text>
            </div>

            <SimpleBar className='flex flex-col gap-5 py-8'>

                <div className='grid grid-cols-2 md:grid-cols-6 gap-3 md:gap-6 justify-center items-center'>
                    {data?.steps?.length && data?.steps?.map((data: any, index: number) => (

                        <div key={index} className='flex flex-col gap-1 pb-4 items-center justify-center'>
                            <Title className='p-1 px-2 font-bold rounded-full flex items-center justify-center border-[.5px] border-[#1A6B4880]'>
                                0{index + 1}
                            </Title>
                            <Text className='text-center font-bold text-sm md:text-base h-12'>
                                {data?.Title}
                            </Text>
                            <div className='relative w-full flex items-center justify-center h-[150px] md:h-[250px] mt-2 md:mt-6'>
                                {/* {useNextSanityImage(configuredSanityClient, data?.image) &&
                                    <Image src={useNextSanityImage(configuredSanityClient, data?.image)!} alt='phone' fill className='object-contain' />} */}
                                <SanityImage imageUrl={data?.image} className={"w-full h-full object-contain"} />
                            </div>
                        </div>

                    )
                    )}
                </div>
                {/* <div className='hidden md:grid grid-cols-6 justify-between'>
                    <div className='relative w-full flex items-center justify-center h-[250px]'>
                        <Image src={image1} alt='phone' fill className='object-contain' />
                    </div>
                    <div className='relative w-full flex items-center justify-center h-[250px]'>
                        <Image src={image2} alt='phone' fill className='object-contain' />
                    </div>
                    <div className='relative w-full flex items-center justify-center h-[250px]'>
                        <Image src={image3} alt='phone' fill className='scale-125 object-scale-down' />
                    </div>
                    <div className='relative w-full flex items-center justify-center h-[250px]'>
                        <Image src={image4} alt='phone' fill className='object-contain' />
                    </div>
                    <div className='relative w-full flex items-center justify-center h-[250px]'>
                        <Image src={image5} alt='phone' fill className='scale-125 object-scale-down' />
                    </div>
                    <div className='relative w-full flex items-center justify-center h-[250px]'>
                        <Image src={image6} alt='phone' fill className='scale-125 object-scale-down' />
                    </div>

                </div> */}
            </SimpleBar>
        </Container>
    )
}

export default HowItWorks