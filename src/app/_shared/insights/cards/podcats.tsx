"use client"
import cn from '@/utils/class-names'
import Image from 'next/image'
import React from 'react'
import { Title, Text } from "@/components/ui/text"
import podcast from "@public/insights/Group 1171275233.svg"
import calender from "@public/insights/Group.svg"
import { formatISODate } from '@/utils/formate-iso-date'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../../configuredSanityClient'
import Link from 'next/link'
import { routes } from '@/configs/routes'

type Props = {
    className: string
    data: any
}

function PodcastCard({ className, data }: Props) {

    const imageProps = useNextSanityImage(configuredSanityClient, data?.image);

    return (
        <Link href={routes.insights + `/${data?._id}`} className={cn(className, 'rounded-xl hover:bg-[#ffffff]')}>
            {imageProps &&
                <Image src={imageProps} alt={"achievements"} className='object-cover w-full h-[60%] rounded-3xl' />}

            <div className='flex flex-col gap-2 mt-3'>
                <Text className='text-sm font-light'>{data?.episode}</Text>
                <Title className='font-bold text-base line-clamp-1'>
                    {data?.title}
                </Title>
                <Text className='font-light text-xs line-clamp-3'>
                    {data?.description && data?.description[0]?.children[0]?.text}
                </Text>

                <div className='flex justify-between'>
                    <Text className='flex items-center gap-2'>
                        <Image src={calender} alt={"calender"} />
                        {data?._updatedAt && formatISODate(data?._updatedAt)}
                    </Text>
                    <Image src={podcast} alt={"podcast"} />
                </div>
            </div>
        </Link>
    )
}

export default PodcastCard