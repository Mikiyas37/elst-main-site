import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"

type Props = {}

function Hero({ }: Props) {
    return (
        <Container>
            <div className='my-24 flex flex-col gap-2 max-w-5xl mx-auto'>
                <Title className='text-center text-3xl 2xl:text-4xl max-w-xl mx-auto w-full font-extrabold'>
                    Latest News from Us: Your Gateway to Industry Updates.
                </Title>
                <Text className='font-light opacity-90 text-center text-base max-w-4xl mx-auto w-full'>
                    Discover a wealth of knowledge and insights from our diverse collection of blogs,
                    articles, news, and podcasts. Explore the latest trends, best practices,
                    and industry updates across various sectors in the realm of software development.
                </Text>
            </div>

        </Container>
    )
}

export default Hero