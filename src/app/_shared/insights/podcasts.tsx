"use client"
import React from 'react'
import Container from '@/components/container'
import { Title, Text } from "@/components/ui/text"
import { Button } from "@/components/ui/button"
import PodcastCard from './cards/podcats'
import image1 from "@public/insights/Rectangle 1716 (3).png"
import image2 from "@public/insights/Rectangle 1716 (4).png"
import image3 from "@public/insights/Rectangle 1716 (5).png"
import image4 from "@public/insights/Rectangle 1716 (6).png"
import SimpleBar from "@/components/ui/simplebar"

type Props = {
    podcasts: any
}

// const podcasts = [
//     {
//         image: image1,
//         episode: "Episode-01",
//         title: "AI: Shaping the Future of Tech",
//         description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront",
//         date: "Sep 22, 2024"
//     },
//     {
//         image: image2,
//         episode: "Episode-01",
//         title: "AI: Shaping the Future of Tech",
//         description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront",
//         date: "Sep 22, 2024"
//     },
//     {
//         image: image3,
//         episode: "Episode-01",
//         title: "AI: Shaping the Future of Tech",
//         description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront",
//         date: "Sep 22, 2024"
//     },
//     {
//         image: image4,
//         episode: "Episode-01",
//         title: "AI: Shaping the Future of Tech",
//         description: "In this era of rapid technological advancement, artificial intelligence (AI) stands at the forefront",
//         date: "Sep 22, 2024"
//     },
// ]

function Podcasts({ podcasts }: Props) {
    return (
        <Container >
            <div className='my-24 w-full'>

                <Title className='text-xl md:text-2xl font-bold'>
                    Stay Connected, stay Informed, Discover our Prodcasts
                </Title>
                <Text className='text-sm font-light'>
                    Explore our diverse range of podcasts offering thought-provoking discussions
                </Text>


                <SimpleBar className='flex flex-col gap-6 mt-12 w-full'>
                    <div className='flex justify-between gap-6 2xl:gap-8 w-full'>
                        {podcasts.length && podcasts?.map((data: any, index: number) => (
                            <PodcastCard key={index}
                                data={data}
                                className='min-h-[380px] min-w-[300px]' />
                        ))}
                    </div>

                    <div className='flex justify-center items-center mt-8'>
                        <Button className={"bg-transparent text-primary hover:text-white hover:bg-primary font-semibold text-base border-2 border-primary rounded-full flex items-center justify-center min-w-[150px] uppercase"}>
                            See All
                        </Button>
                    </div>
                </SimpleBar>
            </div>
        </Container>
    )
}

export default Podcasts