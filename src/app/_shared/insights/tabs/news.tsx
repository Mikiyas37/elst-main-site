"use client"
import React, { useEffect, useState } from 'react'
import SimpleBar from "@/components/ui/simplebar"
import image1 from "@public/insights/Group 1171275168.png"
import image2 from "@public/insights/Rectangle 1774.png"
import image3 from "@public/insights/Group 1171275169.png"
import Image from 'next/image'
import logo from "@public/insights/Group 1171275164.svg"
import { Title, Text } from "@/components/ui/text"
import cn from '@/utils/class-names'
import Link from 'next/link'
import { routes } from '@/configs/routes'
import { formatISODate } from '@/utils/formate-iso-date'
import { useNextSanityImage } from 'next-sanity-image'
import { configuredSanityClient } from '../../configuredSanityClient'
import { getsubCategoryById } from '@/data/get-category'
import CardLoader from '../../looader/card-loader'
import NewsLoader from '../../looader/news-card-loader'

type Props = {
    blogs?: any,
    loading?: boolean
}

function News({ blogs, loading }: Props) {
    return (
        <div className="grid md:grid-cols-6 gap-3 w-full">
            <div className='col-span-2 overflow-hidden w-full'>
                <Image src={image1} alt="image" className='w-full hidden md:block md:h-[70vh] object-contain' />
            </div>
            <SimpleBar className='col-span-3 w-full h-full md:h-[70vh]'>
                {loading ?
                    <div className=' flex flex-col gap-4 justify-between h-full md:h-full divide-y-2 divide-[#333333]/40'>
                        <NewsLoader />
                        <NewsLoader />
                        {/* <NewsLoader /> */}
                    </div>
                    :
                    <div className=' flex flex-col gap-4 justify-between h-full w-full min-h-[50vh] md:h-full divide-y-2 divide-[#333333]/40'>
                        {blogs.length ? blogs?.map((data: any, index: number) => (
                            <NewsCard key={index} data={data}
                                className={" h-full w-full md:h-[24%] p-2 md:p-4"} />
                        ))
                            :
                            <div className=' flex flex-col gap-4 justify-between h-full md:h-full divide-y-2 divide-[#333333]/40'>
                                <NewsLoader />
                                <NewsLoader />
                                {/* <NewsLoader /> */}
                            </div>
                        }
                    </div>
                }
            </SimpleBar>
            <div className='col-span-1 overflow-hidden'>
                <Image src={image3} alt="image" className='w-full hidden md:block md:h-[70vh] object-contain' />
            </div>
        </div>

    )
}

export default News


const NewsCard = ({ data, className }: { data: any, className: string }) => {

    const [subCategory, setsubCategory] = useState<any>([])


    useEffect(() => {
        setsubCategory([]); // Clear previous blogs (optional)
        getsubCategoryById(data?.subcategory?._ref).then(data => setsubCategory(data));
    }, [data])

    const imageProps = useNextSanityImage(configuredSanityClient, data?.image);

    return (
        <Link href={routes.insights + `/${data?._id}`} className={cn(className, 'grid md:grid-cols-3 w-full')}>
            <div className='w-full h-full flex items-center justify-center overflow-hidden p-1.5'>
                {imageProps && <Image src={imageProps} alt={""} className='md:col-span-1 w-full h-full object-cover' />}
            </div>
            <div className='flex flex-col gap-2 col-span-2 p-3 w-full'>
                {subCategory[0]?.title && <span className='bg-primary rounded-full text-xs p-1 px-3 text-white font-bold w-fit'>
                    {subCategory[0]?.title}
                </span>}
                <Title className='text-xs md:text-base font-extrabold'>
                    {data?.title}
                </Title>

                <div className='flex flex-col md:flex-row gap-1 justify-between md:items-center w-full'>
                    <div className='flex gap-1 items-center'>

                        <Image src={logo} alt="logo" className='w-7 h-7 rounded-full object-cover' />
                        <Text className='font-extrabold text-[9px]'>
                            Eaglelion system technologies
                        </Text>
                    </div>

                    <Text className='font-light text-[9px]'>
                        {data?._updatedAt && formatISODate(data?._updatedAt)}
                    </Text>

                    <Text className='font-light text-[9px]'>
                        5 min read
                    </Text>
                </div>

            </div>
        </Link>
    )
}