"use client"
import { Tab, TabList, TabPanel, TabPanels, Tabs } from 'rizzui'
import React, { useEffect, useState } from 'react'
import cn from '@/utils/class-names'

import 'simplebar-react/dist/simplebar.min.css';
import SimpleBar from "@/components/ui/simplebar"

import { useCallback } from 'react';
import nedaj from "@public/products/nedaj.svg"
import dube from "@public/products/dube.svg"
import guzogo from "@public/products/guzogo.svg"
import getrooms from "@public/products/getrooms.svg"

import smartphone from "@public/products/smartphone.png"
import Image from 'next/image';
import News from './news';
import { Title, Text } from "@/components/ui/text"
import { getBlogsBySubCategory } from '@/data/get-blogs';
import Container from '@/components/container';
import SectionLoader from '../../looader/section-loader';


type Props = {
    className: string,
    getSubCatergory: any
    data: any
}

// const tabs = [
//     {
//         title: "All",
//         icon: nedaj,
//         image: smartphone,
//         component: <News />
//     },
//     {
//         title: "news",
//         icon: dube,
//         image: smartphone,
//         component: <News />
//     },
//     {
//         title: "Technology",
//         icon: guzogo,
//         image: smartphone,
//         component: <News />
//     },
//     {
//         title: "Business",
//         icon: getrooms,
//         image: smartphone,
//         component: <News />
//     },
//     {
//         title: "Entertainment",
//         icon: getrooms,
//         image: smartphone,
//         component: <News />
//     },
//     {
//         title: "Articles",
//         icon: getrooms,
//         image: smartphone,
//         component: <News />
//     },
//     {
//         title: "Insights",
//         icon: getrooms,
//         image: smartphone,
//         component: <News />
//     },
// ]

function CategoriesTabs({ data, getSubCatergory, className }: Props) {

    const [blogs, setBlogs] = useState([])
    const [loading, setLoading] = useState(false)

    function handleTabClick(categoryId: string) {
        setLoading(true)
        setBlogs([]); // Clear previous blogs (optional)
        if (categoryId) {
            getBlogsBySubCategory(categoryId).then(data => {
                setBlogs(data)
            });
        } else {
            setBlogs(data)
        }
        data && setLoading(false)
    }

    const memoizedHandleTabClick = useCallback(handleTabClick, blogs);

    useEffect(() => {
        memoizedHandleTabClick(getSubCatergory[0]?._id)
    }, [getSubCatergory, memoizedHandleTabClick]);

    // useEffect(() => {
    //     setBlogs(data);
    // }, [data]);

    return (
        <Container >
            <div className={cn("w-full h-full flex", className)}>

                <Tabs className="mt-6 flex flex-col gap-8 px-3 md:px-6 w-full" defaultIndex={1}>
                    <TabList
                        className="flex flex-col md:flex-row md:items-center gap-3 justify-between w-full border-b border-[#333333]/70">
                        <div className='flex-1'>
                            <Title className='font-extrabold text-3xl border-b-4 w-fit border-b-primary whitespace-nowrap'>
                                Top Categories
                            </Title>
                        </div>

                        <div className='flex w-full justify-end'>

                            <SimpleBar className='w-full md:w-[400px] py-2'>
                                <div className={cn(`flex gap-3 w-full`)}>
                                    <Tab
                                        onClick={() => handleTabClick("")}
                                        className={({ selected }: { selected: boolean }) =>
                                            cn(
                                                "border-none border-b-[1.5px] flex uppercase text-sm font-light opacity-90 w-full max-w-[8rem]",
                                                selected ? "border-b-[#A03A43] outline-none text-primary font-extrabold" : "border-b-[#8C65331A]"
                                            )
                                        }
                                    >
                                        {"All"}
                                    </Tab>
                                    {getSubCatergory?.length ? getSubCatergory?.map((data: any, index: number) => (
                                        <Tab
                                            key={index}
                                            onClick={() => handleTabClick(data?._id)}
                                            className={({ selected }: { selected: boolean }) =>
                                                cn(
                                                    " border-none border-b-[1.5px] flex uppercase text-sm w-full max-w-[8rem]",
                                                    selected ? "border-b-[#A03A43] outline-none text-primary font-extrabold" : "border-b-[#8C65331A] font-light opacity-60"
                                                )
                                            }
                                        >
                                            {data?.title}
                                        </Tab>
                                    )) : null}
                                </div>
                            </SimpleBar>
                        </div>
                    </TabList>
                    {loading ?
                        <SectionLoader />
                        :
                        <TabPanels className="flex-grow w-full h-full">
                            <TabPanel className="w-full rounded-xl shadow-xl h-full " >
                                <News blogs={blogs} loading={loading} />
                            </TabPanel>
                            {getSubCatergory?.map((_: any, index: number) => (
                                <TabPanel key={index} className="w-full rounded-xl bg-white shadow-xl h-full" >
                                    {/* {component} */}
                                    <News blogs={blogs} loading={loading} />
                                </TabPanel>
                            ))}
                        </TabPanels>
                    }
                </Tabs>
            </div>
        </Container>

    )
}

export default CategoriesTabs