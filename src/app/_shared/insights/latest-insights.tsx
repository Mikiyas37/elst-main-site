import React from 'react'
import { Title, Text } from "@/components/ui/text"
import Container from '@/components/container'
import Image from 'next/image'

import image1 from "@public/insights/Group 1171275166.png"
import image2 from "@public/insights/Group 1171275165.png"
import image3 from "@public/insights/Group 1171275165 (1).png"
import image4 from "@public/insights/Group 1171275160.png"

type Props = {}

function LatestInsights({ }: Props) {
    return (
        <Container >
            <div className='my-24'>
                <Title className='text-2xl font-extrabold'>
                    Latest Insights from all Categories
                </Title>
                <Text className='text-sm font-light opacity-90'>
                    Explore thw snippets from different Categories
                </Text>


                <div className='grid md:grid-cols-2 gap-3 mt-4'>
                    <div className='w-full h-[250px] md:h-[500px] overflow-hidden'>
                        <Image src={image1} alt="" className='w-full h-full object-contain' />
                    </div>
                    <div className='flex flex-col gap-3 h-[350px] md:h-[500px] w-full'>
                        <div className='h-full md:h-[50%] w-full flex gap-3'>
                            <div className='h-full w-full overflow-hidden'>
                                <Image src={image2} alt="" className='w-full h-full object-contain' />
                            </div>
                            <div className='h-full w-full overflow-hidden'>
                                <Image src={image3} alt="" className='w-full h-full object-contain' />
                            </div>
                        </div>


                        <div className='h-full md:h-[50%] w-full overflow-hidden '>
                            <Image src={image4} alt="" className='w-full h-full object-contain' />
                        </div>

                    </div>
                </div>
            </div>
        </Container>
    )
}

export default LatestInsights