"use client"
import Container from '@/components/container'
import React from 'react'
import { Title } from "@/components/ui/text"
import { Button } from "@/components/ui/button"
import { Input } from 'rizzui'

type Props = {}

function Subscription({ }: Props) {
    return (
        <Container>
            <div className='my-24 py-12 border-t-2 boder-[#333333] grid md:grid-cols-2 items-center gap-8'>
                <Title className='font-bold text-2xl md:text-3xl max-w-lg w-full'>
                    Join our resources of Eaglelion and get the latest activity update into your inbox monthly.
                </Title>

                <div className='flex justify-between p-[0.5px] items-center border-2 border-primary w-full h-12 rounded-full'>
                    <Input
                        placeholder='Enter your email address'
                        className='placeholder:black/60 focus:!outline-none !outline-none !border-none focus:border-none bg-transparent flex-1 text-sm'
                    />

                    <Button className='bg-primary text-white text-sm rounded-full max-w-[8rem] md:max-w-[12rem] w-full h-full'>
                        Let’s go!
                    </Button>
                </div>
            </div>
        </Container>
    )
}

export default Subscription 