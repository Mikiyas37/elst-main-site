import React from 'react'
import CategoriesTabs from './tabs/categories-tab'

type Props = {
    getSubCatergory: any,
    data: any
}

function TopCategories({ data, getSubCatergory }: Props) {
    return <CategoriesTabs
        data={data}
        getSubCatergory={getSubCatergory}
        className="my-24" />
}

export default TopCategories