import type { Config } from "tailwindcss"

const config = {
  darkMode: ["class"],
  content: [
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/rizzui/dist/*.{js,ts,jsx,tsx}",
    "./src/app/globals.css"
  ],
  prefix: "",
  theme: {
    // container: {
    //   center: true,
    //   padding: "2rem",
    //   screens: {
    //     "2xl": "1400px",
    //   },
    // },
    extend: {
      fontFamily: {
        sans: ["var(--font-sans)"],
        public: ["var(--font-public)"],
        landsans: ["var(--font-landsans)"],
        gilroy: ["var(--font-gilroy)"],
      },
      colors: {
        border: "hsl(var(--border))",
        input: "hsl(var(--input))",
        ring: "hsl(var(--ring))",
        background: "hsl(var(--background))",
        foreground: "hsl(var(--foreground))",
        primary: {
          lighter: "#a03a43",
          DEFAULT: "#a03a43 ",
          foreground: "hsl(var(--primary-foreground))",
        },
        secondary: {
          lighter: "#8c6533",
          DEFAULT: "#8c6533",
          foreground: "hsl(var(--secondary-foreground))",
        },
        destructive: {
          DEFAULT: "hsl(var(--destructive))",
          foreground: "hsl(var(--destructive-foreground))",
        },
        muted: {
          DEFAULT: "hsl(var(--muted))",
          foreground: "hsl(var(--muted-foreground))",
        },
        accent: {
          DEFAULT: "hsl(var(--accent))",
          foreground: "hsl(var(--accent-foreground))",
        },
        popover: {
          DEFAULT: "hsl(var(--popover))",
          foreground: "hsl(var(--popover-foreground))",
        },
        card: {
          DEFAULT: "hsl(var(--card))",
          foreground: "hsl(var(--card-foreground))",
        },
      },
      borderRadius: {
        lg: "var(--radius)",
        md: "calc(var(--radius) - 2px)",
        sm: "calc(var(--radius) - 4px)",
      },
      keyframes: {
        'spin': {
          '0%': { transform: 'rotate(0deg)' },
          '100%': { transform: 'rotate(360deg)' }
        },
        'spin-reverse': {
          '0%': { transform: 'rotate(0deg)' },
          '100%': { transform: 'rotate(-360deg)' }
        },
        "accordion-down": {
          from: { height: "0" },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: "0" },
        },
        orbit: {
          "0%": {
            transform: "rotate(0deg) translateY(calc(var(--radius) * 1px)) rotate(0deg)",
          },
          "100%": {
            transform: "rotate(360deg) translateY(calc(var(--radius) * 1px)) rotate(-360deg)",
          },
        },
        "rotate-full": {
          "0%": { transform: "rotate(0deg)" },
          "100%": { transform: "rotate(360deg)" },
        },

      },
      animation: {
        'spin-slow': 'spin 60s linear infinite',
        'spin-slow-reverse': 'spin-reverse 60s linear infinite',
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
        orbit: "orbit calc(var(--duration)*1s) linear infinite",
      },
    },
  },
  // plugins: [require("tailwindcss-animate"), require("@tailwindcss/forms")],
  plugins: [require("tailwindcss-animate")],
} satisfies Config

export default config

// import type { Config } from "tailwindcss";
// import colors from "tailwindcss/colors";

// const config: Config = {
//   content: [
//     "./src/**/*.{js,ts,jsx,tsx}",
//     "./node_modules/rizzui/dist/*.{js,ts,jsx,tsx}",
//     "./src/app/globals.css"
//   ],
//   theme: {
//     extend: {
//       colors: {
//         /*
//          * body, modal, drawer background & ring-offset-color
//          */
//         background: colors.white,

//         /*
//          * body text color
//          */
//         foreground: colors.gray[600],

//         /*
//          * border, default flat bg color for input components, tab & dropdown hover color
//          */
//         muted: colors.gray[200],

//         /*
//          * primary colors
//          */
//         primary: {
//           lighter: "#a03a43",
//           DEFAULT: "#a03a43 ",
//           dark: colors.gray[950],
//           foreground: colors.white,
//         },

//         /*
//          * secondary colors
//          */
//         secondary: {
//           lighter: "#8c6533",
//           DEFAULT: "#8c6533",
//           dark: colors.indigo[700],
//           foreground: colors.white,
//         },

//         /*
//          * danger colors
//          */
//         red: {
//           lighter: colors.rose[200],
//           DEFAULT: colors.rose[500],
//           dark: colors.rose[700],
//         },

//         black: {
//           DEFAULT: "#333333"
//         },
//         white: {
//           DEFAULT: "#F8F8F8"
//         },
//         /*
//          * warning colors
//          */
//         orange: {
//           lighter: colors.amber[200],
//           DEFAULT: colors.amber[500],
//           dark: colors.amber[700],
//         },

//         /*
//          * info colors
//          */
//         blue: {
//           lighter: colors.sky[200],
//           DEFAULT: colors.sky[500],
//           dark: colors.sky[700],
//         },

//         /*
//          * success colors
//          */
//         green: {
//           lighter: colors.emerald[200],
//           DEFAULT: colors.emerald[500],
//           dark: colors.emerald[700],
//         },
//       },
//       fontFamily: {
//         sans: ["var(--font-sans)"],
//         public: ["var(--font-public)"],
//         landsans: ["var(--font-landsans)"],
//         gilroy: ["var(--font-gilroy)"],
//       },
//       boxShadow: {
//         'glass-inset': 'inset 0 17px 5px -9px rgba(254,254,91, 0.05)',
//         'glass-sm': '5px 5px 20px 0px rgba(254,254,91, 0.3)',
//       },

//       animation: {
//         'spin-slow': 'spin 60s linear infinite',
//         'spin-slow-reverse': 'spin-reverse 60s linear infinite',
//         orbit: "orbit calc(var(--duration)*1s) linear infinite",
//       },

//       keyframes: {
//         'spin-reverse': {
//           '0%': { transform: 'rotate(0deg)' },
//           '100%': { transform: 'rotate(-360deg)' }
//         },
//         orbit: {
//           "0%": {
//             transform: "rotate(0deg) translateY(calc(var(--radius) * 1px)) rotate(0deg)",
//           },
//           "100%": {
//             transform: "rotate(360deg) translateY(calc(var(--radius) * 1px)) rotate(-360deg)",
//           },
//         },
//       },
//     },
//   },
//   plugins: [require("@tailwindcss/forms")],
// };
// export default config;
