import { type SchemaTypeDefinition } from 'sanity'
import { blogType } from './schema/blogType'
import { featuredProductType } from './schema/featuredProductType'
import { testimonialsType } from './schema/testimonials'
import { productDetailsType } from './schema/productDetailsType'
import { productType } from './schema/productsType'
import {  subCategoryType } from './schema/subCategoryType'
import { categoryType } from './schema/category'
import { jobsType } from './schema/jobsType'


export const schema: { types: SchemaTypeDefinition[] } = {
  types: [
    blogType,
    productType,
    featuredProductType,
    productDetailsType,
    testimonialsType,
    subCategoryType,
    categoryType,
    jobsType
  ],
}
