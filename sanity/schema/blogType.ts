import { defineArrayMember, defineField, defineType } from 'sanity'

export const blogType = defineType({
    name: 'blog',
    title: 'Blog',
    type: 'document',
    fields: [
        defineField({
            name: 'category', // Reference field
            title: 'Main Category',
            type: 'reference',
            to: [{ type: 'category' }], // Reference to the product schema
            validation: (Rule) => Rule.required(),
        }),
        defineField({
            name: 'subcategory', // Reference field
            title: 'Sub Category',
            type: 'reference',
            to: [{ type: 'subcategory' }], // Reference to the product schema
            validation: (Rule) => Rule.required(),
        }),

        defineField({
            name: 'title',
            type: 'string',
        }),
        defineField({
            name: 'image',
            type: 'image',
        }),
        defineField({
            name: 'description',
            type: 'array',
            title: 'Product Details',
            of: [{ type: 'block' }],
        }),

        defineField({
            name: 'details',
            title: "Details Information",
            type: "array",
            of: [
                defineArrayMember({
                    name: "information",
                    type: "object",
                    fields: [
                        defineField({
                            name: 'subtitle',
                            type: 'string',
                            title: "Sub Title"
                        }),
                        defineField({
                            name: 'description',
                            type: 'array',
                            title: 'Product Details',
                            of: [{ type: 'block' }],
                        }),
                    ]
                })
            ]
        }),

    ],
})

