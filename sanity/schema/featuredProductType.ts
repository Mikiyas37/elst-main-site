import { defineArrayMember, defineField, defineType } from 'sanity'

export const featuredProductType = defineType({
    name: 'featuredProduct',
    title: 'Featured Products',
    type: 'document',
    fields: [
        defineField({
            name: 'product', // Reference field
            title: 'Product',
            type: 'reference',
            to: [{ type: 'product' }], // Reference to the product schema
            validation: (Rule) => Rule.required(),
        }),

        defineField({
            name: 'category',
            type: 'string',
            title: 'Prodcut Category'
        }),
        defineField({
            name: 'title',
            type: 'string',
            title: 'Product Name'
        }),
        defineField({
            name: 'logo',
            type: 'image',
            title: 'Product Logo',
        }),
        defineField({
            name: 'image',
            type: 'image',
            title: 'Product Image',
        }),
        defineField({
            name: 'description',
            type: 'array',
            title: 'Product Details',
            of: [{ type: 'block' }],
        }),
        defineField({
            name: 'link',
            type: 'url',
            title: 'Product Link',
        }),

        defineField({
            name: "achievments",
            title: "Achievments",
            type: "object",
            fields: [
                // stats
                defineField({
                    name: "stats",
                    title: "Statistics",
                    type: "array",
                    of: [
                        defineArrayMember({
                            name: "",
                            type: "object",
                            title: "Stats",
                            fields: [
                                defineField({
                                    name: "name",
                                    title: "Name",
                                    type: "string",
                                }),
                                defineField({
                                    name: 'value',
                                    type: 'number',
                                    title: 'Value',
                                }),
                                defineField({
                                    name: 'description',
                                    type: 'string',
                                    title: 'Description (Like Billion+, Million+, +, %)',
                                }),
                            ]
                        })
                    ]
                })
            ]
        }),

        defineField({
            name: 'happyCustomer',
            type: 'text',
            title: 'One of Happy Customer says:'
        }),
        defineField({
            name: 'happyCustomerImage',
            type: 'image',
            title: 'One of Happy Customer Avatar'
        }),

    ],
})
