import { defineField, defineType } from "sanity";


export const testimonialsType = defineType({
    name: 'testimonials',
    type: 'document',
    title: 'Testimonails',
    fields: [
        defineField({
            name: 'name',
            type: "string",
            title: "Full Name"
        }),
        defineField({
            name: 'title',
            type: "string",
            title: 'Job Position'
        }),
        defineField({
            name: 'description',
            type: "text",
            title: 'Description'
        }),
        defineField({
            name: 'image',
            type: 'image',
            title: "Avatar"
        })
    ]
})