import { defineArrayMember, defineField, defineType } from "sanity";

export const productDetailsType = defineType({
    name: 'productDetails',
    title: 'Product Detail Information',
    type: 'document',

    fields: [
        defineField({
            name: 'product', // Reference field
            title: 'Product',
            type: 'reference',
            to: [{ type: 'product' }], // Reference to the product schema
            validation: (Rule) => Rule.required(),
        }),

        // Introdcution Section
        defineField({
            name: "introdcution",
            title: "Product Introdcution",
            type: 'object',
            fields: [
                defineField({
                    name: 'category',
                    type: 'string',
                    title: 'Prodcut Category'
                }),
                defineField({
                    name: 'title',
                    type: 'string',
                    title: 'Product Name'
                }),
                defineField({
                    name: 'subtitle',
                    type: 'string',
                    title: 'Product Sub Title'
                }),

                defineField({
                    name: 'description',
                    type: 'array',
                    title: 'Product Description',
                    of: [{ type: 'block' }],
                }),
                defineField({
                    name: 'playStore',
                    type: 'string',
                    title: 'PlayStore Link'
                }),
                defineField({
                    name: 'appStore',
                    type: 'string',
                    title: 'AppStore Link'
                }),

                // About Section
                defineField({
                    name: 'aboutSection',
                    title: 'About Section',
                    type: "object",
                    fields: [
                        defineField({
                            name: 'title',
                            title: "About Title",
                            type: "string"
                        }),

                        defineField({
                            name: 'description',
                            type: 'array',
                            title: 'About Description',
                            of: [{ type: 'block' }],
                        }),
                        defineField({
                            name: 'image',
                            title: "About Section Image",
                            type: "image"
                        }),
                    ]
                })
            ]

        }),

        // Our App Top Features
        defineField({
            name: 'topFeatures',
            title: 'Our App Top Features',
            type: 'object',
            fields: [
                defineField({
                    name: 'title',
                    title: 'Title',
                    type: "string"
                }),
                defineField({
                    name: 'subtitle',
                    title: 'Sub Title',
                    type: "string"
                }),
                defineField({
                    name: 'features',
                    title: "App Features",
                    type: "array",
                    of: [
                        defineArrayMember({
                            name: "",
                            type: "object",
                            fields: [
                                defineField({
                                    name: 'featureName',
                                    title: 'Feature Name',
                                    type: "string"
                                }),
                                defineField({
                                    name: 'featureDescription',
                                    type: 'array',
                                    title: 'Feature Description',
                                    of: [{ type: 'block' }],
                                }),
                                defineField({
                                    name: 'image',
                                    title: 'Feature Image',
                                    type: "image"
                                }),
                            ]
                        })

                    ]
                })
            ]

        }),

        // Achievements
        defineField({
            name: "achievments",
            title: "Achievments",
            type: "object",
            fields: [
                defineField({
                    name: "title",
                    title: "Title",
                    type: "string",
                }),

                defineField({
                    name: 'description',
                    type: 'array',
                    title: ' Description',
                    of: [{ type: 'block' }],
                }),
                defineField({
                    name: "image",
                    title: "Image",
                    type: "image",
                }),

                // stats
                defineField({
                    name: "stats",
                    title: "Statistics",
                    type: "array",
                    of: [
                        defineArrayMember({
                            name: "",
                            type: "object",
                            title: "Stats",
                            fields: [
                                defineField({
                                    name: "name",
                                    title: "Name",
                                    type: "string",
                                }),
                                defineField({
                                    name: 'value',
                                    type: 'number',
                                    title: 'Value',
                                }),
                                defineField({
                                    name: 'description',
                                    type: 'string',
                                    title: 'Description (Like Billion+, Million+, +, %)',
                                }),
                            ]
                        })
                    ]
                })
            ]
        }),

        // How it works
        defineField({
            name: 'howItWorks',
            title: 'How It Works',
            type: 'object',
            fields: [
                defineField({
                    name: 'title',
                    title: 'Title',
                    type: "string"
                }),
                defineField({
                    name: 'subtitle',
                    title: 'Sub Title',
                    type: "string"
                }),
                defineField({
                    name: 'steps',
                    title: "Steps",
                    type: "array",
                    of: [
                        defineArrayMember({
                            name: "",
                            type: "object",
                            title: "Steps",
                            fields: [
                                defineField({
                                    name: 'Title',
                                    title: 'Step Name',
                                    type: "string"
                                }),
                                defineField({
                                    name: 'image',
                                    title: 'Step Image',
                                    type: "image"
                                }),
                            ]
                        })

                    ]
                })
            ]

        }),


        // Web Experince
        defineField({
            name: "web",
            title: "Web Experince",
            type: "object",
            fields: [
                defineField({
                    name: 'title',
                    type: 'string',
                    title: 'Title'
                }),
                defineField({
                    name: 'subtitle',
                    type: 'string',
                    title: 'Sub Title'
                }),
                defineField({
                    name: 'image',
                    type: 'image',
                    title: 'Web Image'
                }),
            ]
        }),

        // Memories
        defineField({
            name: "memories",
            title: "Experince",
            type: "object",
            fields: [
                // Video
                defineField({
                    name: "videoMemory",
                    title: "Video Memory",
                    type: "object",
                    fields: [
                        defineField({
                            name: 'title',
                            type: 'string',
                            title: 'Title'
                        }),
                        defineField({
                            name: 'subtitle',
                            type: 'string',
                            title: 'Sub Title'
                        }),
                        defineField({
                            name: 'video',
                            type: 'array',
                            title: 'Videos',
                            of: [
                                defineField({
                                    name: "video",
                                    title: "Video URL",
                                    type: "url"
                                })
                            ]
                        }),
                    ]
                }),

                // Photo
                defineField({
                    name: "photoMemory",
                    title: "Photo Memory",
                    type: "object",
                    fields: [
                        defineField({
                            name: 'title',
                            type: 'string',
                            title: 'Title'
                        }),

                        defineField({
                            name: 'images',
                            type: 'array',
                            title: 'Images',
                            of: [
                                defineField({
                                    name: "images",
                                    title: "Images",
                                    type: "object",
                                    fields: [
                                        defineField({
                                            name: "image1",
                                            title: "Image 1",
                                            type: "image",
                                            validation: (Rule) => Rule.required()
                                        }),
                                        defineField({
                                            name: "image2",
                                            title: "Image 2",
                                            type: "image",
                                            validation: (Rule) => Rule.required()
                                        }),
                                        defineField({
                                            name: "image3",
                                            title: "Image 3",
                                            type: "image",
                                            validation: (Rule) => Rule.required()
                                        }),
                                        defineField({
                                            name: "image4",
                                            title: "Image 4",
                                            type: "image",
                                            validation: (Rule) => Rule.required()
                                        }),
                                    ]
                                })

                            ]
                        }),
                    ]
                }),

            ]
        }),


        // Testimonials
        defineField({
            name: "testimonail",
            title: "Testimonail",
            type: "object",
            fields: [
                defineField({
                    name: 'customerName',
                    type: 'string',
                    title: 'Customer Name'
                }),
                defineField({
                    name: 'jobTitle',
                    type: 'string',
                    title: 'Customer Job Title'
                }),
                defineField({
                    name: 'description',
                    type: 'array',
                    title: ' One of Happy Customer says:',
                    of: [{ type: 'block' }],
                }),
                defineField({
                    name: 'happyCustomerImage',
                    type: 'image',
                    title: 'customer Avatar'
                }),

            ]
        }),


        // Partnership
        defineField({
            name: "partner",
            title: "Partnership",
            type: "array",
            of: [
                defineArrayMember({
                    name: "",
                    title: "partnership with",
                    type: "object",
                    fields: [
                        defineField({
                            name: "image",
                            type: "image",
                            title: "LOGO"
                        })
                    ]
                })
            ]
        }),


        // Download the App
        defineField({
            name: 'download',
            title: "Download Recommendation",
            type: 'object',
            fields: [
                defineField({
                    name: 'title',
                    type: 'string',
                    title: 'Title'
                }),

                defineField({
                    name: 'description',
                    type: 'array',
                    title: 'Download Description',
                    of: [{ type: 'block' }],
                }),
                defineField({
                    name: "image",
                    type: "image",
                    title: "Image"
                }),
                defineField({
                    name: 'playStore',
                    type: 'string',
                    title: 'PlayStore Link'
                }),
                defineField({
                    name: 'appStore',
                    type: 'string',
                    title: 'AppStore Link'
                }),
            ]
        })

    ]
})
