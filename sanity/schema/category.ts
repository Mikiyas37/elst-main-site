import { defineField, defineType } from 'sanity'

export const categoryType = defineType({
    name: 'category',
    title: 'Main Category',
    type: 'document',
    fields: [
        defineField({
            name: 'title',
            type: 'string',
            title: "Main Category Name"
        }),
    ],
})

