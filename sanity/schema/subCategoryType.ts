import { defineField, defineType } from 'sanity'

export const subCategoryType = defineType({
    name: 'subcategory',
    title: 'Sub Category',
    type: 'document',
    fields: [
        defineField({
            name: 'title',
            type: 'string',
            title: "Sub Category Name"
        }),
    ],
})

