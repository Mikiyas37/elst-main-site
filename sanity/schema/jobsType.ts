import { defineArrayMember, defineField, defineType } from 'sanity'

export const jobsType = defineType({
    name: 'jobs',
    title: 'Jobs',
    type: 'document',
    fields: [

        defineField({
            name: 'title',
            type: 'string',
            title: 'Job Name'
        }),
        defineField({
            name: 'type',
            type: 'string',
            title: 'Job Type (Full Time, remote...}'
        }),
        defineField({
            name: 'description',
            type: 'array',
            title: 'short Description',
            of: [{ type: 'block' }],
        }),
        defineField({
            name: 'details',
            type: 'array',
            title: 'Job Details Description',
            of: [{ type: 'block' }],
        }),

        defineField({
            name: "tasks",
            title: "List of Tasks",
            type: "array",
            of: [
                defineField({
                    name: 'description',
                    type: 'string',
                    title: 'Task',
                }),
            ]
        }),
        defineField({
            name: "requirements",
            title: "List of Requirements",
            type: "array",
            of: [
                defineField({
                    name: 'description',
                    type: 'string',
                    title: 'Requirement',
                }),
            ]
        }),

        defineField({
            name: 'contact',
            type: 'array',
            title: 'Contact Info',
            of: [{ type: 'block' }],
        }),

    ],
})
