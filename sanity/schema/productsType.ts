import { defineField, defineType } from "sanity";

export const productType = defineType({
    name: "product",
    title: "Product",
    type: "document",
    fields: [
        defineField({
            name: 'name',
            title: 'Product Name',
            type: 'string',
            validation: (Rule) => Rule.required(),
        }),
        defineField({
            name: 'category',
            title: 'Product Category',
            type: 'string',
            validation: (Rule) => Rule.required(),
        }),
        // defineField({
        //     name: 'category', // Reference field
        //     title: 'Product Category',
        //     type: 'reference',
        //     to: [{ type: 'category' }], // Reference to the product schema
        //     validation: (Rule) => Rule.required(),
        // }),
        defineField({
            name: 'logo',
            type: 'image',
            title: 'Product Logo',
        }),
        defineField({
            name: 'image',
            type: 'image',
            title: 'Product Image',
        }),
        defineField({
            name: 'description',
            type: 'array',
            title: 'Product Details',
            of: [{ type: 'block' }],
        }),
        defineField({
            name: 'link',
            type: 'url',
            title: 'Product Redirect Link',
        }),
    ]
})